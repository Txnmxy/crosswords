include:
  # Used for the flatkpak "extends" in the jobs below
  - project: 'gnome/citemplates'
    file: 'flatpak/flatpak-ci-initiative-sdk-extensions.yml'
    # ref: ''

  # Freedesktop CI Templates, used to generate a container image for general work
  - remote: "https://gitlab.freedesktop.org/freedesktop/ci-templates/-/raw/b61a03cabbf308e81289f7aaaf0b5a80a34ffb99/templates/opensuse.yml"

stages:
  - container-build
  - test
  - analysis
  - docs
  - deploy

variables:
  # Tag for CI container image.
  # When branching change the suffix to avoid conflicts with images
  # from the master branch.
  BASE_TAG: "2024-11-21.0-master"
  RUST_STABLE: "1.81.0"
  RUSTUP_VERSION: "1.26.0"
  GIT_DEPTH: 1
  BUNDLE: "crosswords-nightly.flatpak"

# Enable merge request pipelines and avoid duplicate pipelines
# https://docs.gitlab.com/ee/ci/yaml/index.html#switch-between-branch-pipelines-and-merge-request-pipelines
workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"
      when: never
    - if: '$CI_COMMIT_TAG'
    - if: '$CI_COMMIT_BRANCH'

# Builds a container image based on openSUSE Tumbleweed, with a bunch
# of tools for CI jobs and local development.
#
# If you want to use this image locally, run the
# ci/pull-container-image.sh script on your machine and follow its
# instructions.
opensuse-container@x86_64.stable:
  extends:
    - .fdo.container-build@opensuse@x86_64
  stage: "container-build"
  before_script:
    - source ./ci/env.sh
  variables:
    FDO_DISTRIBUTION_VERSION: "tumbleweed"
    FDO_UPSTREAM_REPO: "jrb/crosswords"
    FDO_DISTRIBUTION_TAG: "x86_64-${BASE_TAG}"

    FDO_DISTRIBUTION_PACKAGES: >-
      appstream-glib
      clang
      clang-tools
      desktop-file-utils
      diffutils
      gcc
      gettext
      git
      glib2-devel
      gobject-introspection-devel
      gstreamer-plugins-bad-devel
      gtk4-devel
      iso-codes-devel
      itstool
      json-glib-devel
      less
      libadwaita-devel
      libasan8
      librsvg-devel
      libpanel-devel
      llvm
      meson
      ninja
      python3-pip
      util-linux-systemd
      wget
      which

    FDO_DISTRIBUTION_EXEC: >-
      bash ci/install-rust.sh --rustup-version ${RUSTUP_VERSION} \
                              --stable ${RUST_STABLE} \
                              --arch x86_64-unknown-linux-gnu &&
      bash ci/install-rust-tools.sh &&
      bash ci/install-grcov.sh &&
      bash ci/checkout-flatpak-builder-tools.sh &&
      python3 -m venv /usr/local/python &&
      source /usr/local/python/bin/activate &&
      pip3 install --upgrade pip &&
      pip3 install -r ci/requirements.txt &&
      rm -rf /root/.cargo /root/.cache    # cleanup compilation dirs; binaries are installed now

# Build a Crosswords development flatpak
flatpak:
    image: 'quay.io/gnome_infrastructure/gnome-runtime-images:gnome-47'
    variables:
        MANIFEST_PATH: "org.gnome.Crosswords.Devel.json"
        FLATPAK_MODULE: "crosswords"
        APP_ID: "org.gnome.Crosswords.Devel"
        RUNTIME_REPO: "https://nightly.gnome.org/gnome-nightly.flatpakrepo"
    extends: '.flatpak'

# Publish the nightly flatpak
nightly:
  extends: '.publish_nightly'
  dependencies: ['flatpak']
  needs: ['flatpak']

# Builds in release mode to catch compiler warnings
build-and-test:
  stage: test
  extends:
    - 'opensuse-container@x86_64.stable'
    - '.fdo.distribution-image@opensuse'
  needs:
    - job: opensuse-container@x86_64.stable
      artifacts: false
  variables:
    MESON_EXTRA_FLAGS: "--buildtype=release -Dwerror=true"
  script:
    - source ci/env.sh
    - bash -x ./ci/build-and-test.sh
  artifacts:
    name: "crosswords-${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}"
    when: always
    paths:
      - "_build_and_test/meson-logs/*"

# Run static analysis on the code.
#
# The logs are part of the compilation stderr.
static-scan:
  stage: analysis
  extends:
    - 'opensuse-container@x86_64.stable'
    - '.fdo.distribution-image@opensuse'
  needs:
    - job: opensuse-container@x86_64.stable
      artifacts: false
  variables:
    MESON_EXTRA_FLAGS: "--buildtype=debug"
  script:
    - meson subprojects download libipuz
    - meson setup  _libipuz_build subprojects/libipuz --prefix /usr -Dintrospection=disabled -Ddocumentation=disabled
    - meson compile -C _libipuz_build
    - meson install -C _libipuz_build
    - meson setup ${MESON_EXTRA_FLAGS} --prefix /usr _scan_build .
    - SCANBUILD=$(pwd)/ci/scan-build.sh ninja -C _scan_build scan-build
  artifacts:
    name: "crosswords-${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}"
    when: always
    paths:
      - "_scan_build/meson-logs/scanbuild"

# Render the development guide
devel-docs:
  extends:
    - 'opensuse-container@x86_64.stable'
    - '.fdo.distribution-image@opensuse'
  stage: docs
  needs:
    - job: opensuse-container@x86_64.stable
      artifacts: false
  script:
    - source ci/env.sh
    - bash -x ./ci/gen-devel-docs.sh
  artifacts:
    paths:
      - public

# Publish the rendered development guide with GitLab Pages
pages:
  stage: deploy
  needs:
    - job: devel-docs
  script:
    - cp ci/pages-index.html public/index.html
  artifacts:
    paths:
      - public
  rules:
    # Restrict to the main branch so not every branch tries to deploy the web site
    - if: ($CI_DEFAULT_BRANCH == $CI_COMMIT_BRANCH)
    # Alternatively, restrict it to the jrb namespace to avoid every fork pushing a set of pages by default
    # - if: ($CI_DEFAULT_BRANCH == $CI_COMMIT_BRANCH && $CI_PROJECT_NAMESPACE == "jrb")
