#!/bin/sh

set -eu -o pipefail

cd /usr/local
git clone --depth=1 https://github.com/flatpak/flatpak-builder-tools.git
