#!/bin/bash
#
# Utility script so you can pull the container image from CI for local development.
# Run this script and follow the instructions; the script will tell you how
# to run "podman run" to launch a container that has the same environment as the
# one used during CI pipelines.  You can debug things at leisure there.

set -eu
set -o pipefail

CONTAINER_BUILDS=.gitlab-ci.yml

if [ ! -f $CONTAINER_BUILDS ]
then
    echo "Please run this from the toplevel source directory in crosswords"
    exit 1
fi

tag=$(grep -e '^  BASE_TAG:' $CONTAINER_BUILDS | head -n 1 | sed -E 's/.*BASE_TAG: "(.+)"/\1/')
full_tag=x86_64-$tag
echo full_tag=\"$full_tag\"

image_name=registry.gitlab.gnome.org/jrb/crosswords/opensuse/tumbleweed:$full_tag

echo pulling image $image_name
podman pull $image_name

echo ""
echo "Now run this:"
echo ""
echo "  distrobox create --image $image_name --name crosswords"
echo "  distrobox enter crosswords"
echo ""
echo "Once inside the container, you can build and install with this:"
echo ""
echo "  source ci/env.sh"
echo "  sudo --preserve-env=PATH,RUSTUP_HOME sh ci/build-and-install.sh"
echo ""
echo "You can just run crosswords on the shell afterwards.  It is installed to"
echo "the overlay /usr filesystem, and will not interfere with your system:"
echo ""
echo "  crosswords"
