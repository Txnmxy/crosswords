# Desktop File
desktop_file_in_config = configuration_data()
desktop_file_in_config.set('app_id', app_id)
desktop_file_in_config.set('app_name', 'org.gnome.Crosswords')
desktop_file_in_config.set('app_version', meson.project_version())
desktop_file_in = configure_file(
          input: 'org.gnome.Crosswords.desktop.in.in',
         output: '@0@.desktop.in'.format(app_id),
  configuration: desktop_file_in_config,
)

desktop_file = i18n.merge_file(
  input: desktop_file_in,
  output: '@0@.desktop'.format(app_id),
  type: 'desktop',
  po_dir: '../po',
  install: true,
  install_dir: join_paths(get_option('datadir'), 'applications')
)

# Edit Desktop File
edit_desktop_file_in_config = configuration_data()
edit_desktop_file_in_config.set('edit_app_id', edit_app_id)
edit_desktop_file_in_config.set('edit_app_name', 'org.gnome.Crosswords.Editor')
edit_desktop_file_in_config.set('edit_app_version', meson.project_version())
edit_desktop_file_in = configure_file(
          input: 'org.gnome.Crosswords.Editor.desktop.in.in',
         output: '@0@.desktop.in'.format(edit_app_id),
  configuration: edit_desktop_file_in_config,
)

edit_desktop_file = i18n.merge_file(
  input: edit_desktop_file_in,
  output: '@0@.desktop'.format(edit_app_id),
  type: 'desktop',
  po_dir: '../po',
  install: true,
  install_dir: join_paths(get_option('datadir'), 'applications')
)

desktop_utils = find_program('desktop-file-validate', required: false)
if desktop_utils.found()
  test('Validate desktop file', desktop_utils,
    args: [desktop_file, edit_desktop_file]
  )
endif

# Appstream
appconf = configuration_data()
appconf.set('app_id', app_id)
appstream_file_in = configure_file(
  input: 'org.gnome.Crosswords.metainfo.xml.in.in',
         output: '@0@.metainfo.xml.in'.format(app_id),
  configuration: appconf,
)
appstream_file = i18n.merge_file(
  input: appstream_file_in,
  output: '@0@.metainfo.xml'.format(app_id),
  po_dir: '../po',
  install: true,
  install_dir: join_paths(get_option('datadir'), 'metainfo')
)

edit_appconf = configuration_data()
edit_appconf.set('edit_app_id', edit_app_id)
edit_appstream_file_in = configure_file(
  input: 'org.gnome.Crosswords.Editor.metainfo.xml.in.in',
         output: '@0@.metainfo.xml.in'.format(edit_app_id),
  configuration: edit_appconf,
)
edit_appstream_file = i18n.merge_file(
  input: edit_appstream_file_in,
  output: '@0@.metainfo.xml'.format(edit_app_id),
  po_dir: '../po',
  install: true,
  install_dir: join_paths(get_option('datadir'), 'metainfo')
)

appstream_util = find_program('appstream-util', required: false)
if appstream_util.found()
  test('Validate appstream file', appstream_util,
    args: ['validate', '--nonet', appstream_file]
  )
  test('Validate appstream file', appstream_util,
    args: ['validate', '--nonet', edit_appstream_file]
  )
endif

# Schemas
gnome.compile_schemas(build_by_default: true)
install_data('org.gnome.Crosswords.gschema.xml',
             install_dir: join_paths(get_option('datadir'), 'glib-2.0/schemas'))
install_data('org.gnome.Crosswords.Editor.gschema.xml',
             install_dir: join_paths(get_option('datadir'), 'glib-2.0/schemas'))

# D-Bus service file.
dbusconf = configuration_data()
dbusconf.set('bindir', join_paths(get_option('prefix'), get_option('bindir')))
dbusconf.set('app_id', app_id)
configure_file(
          input: 'org.gnome.Crosswords.service.in',
         output: '@0@.service'.format(app_id),
  configuration: dbusconf,
    install_dir: join_paths(get_option('datadir'), 'dbus-1', 'services'),
)

# Editor D-Bus service file.
edit_dbusconf = configuration_data()
edit_dbusconf.set('bindir', join_paths(get_option('prefix'), get_option('bindir')))
edit_dbusconf.set('edit_app_id', edit_app_id)
configure_file(
          input: 'org.gnome.Crosswords.Editor.service.in',
         output: '@0@.service'.format(edit_app_id),
  configuration: edit_dbusconf,
    install_dir: join_paths(get_option('datadir'), 'dbus-1', 'services'),
)

# MIME package.
install_data('org.gnome.Crosswords.mime.xml',
             rename: '@0@.xml'.format(app_id),
             install_dir: join_paths(get_option('datadir'), 'mime/packages'))

subdir('icons')
