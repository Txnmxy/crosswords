<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
  <id>@app_id@</id>
  <name>Crosswords</name>
  <!-- Translators: No need to translate the name-->
  <developer id="org.gnome.crosswords">
    <name>Jonathan Blandford</name>
  </developer>
  <summary>Solve crossword puzzles</summary>
  <metadata_license>CC0-1.0</metadata_license>
  <recommends>
    <control>keyboard</control>
    <control>pointing</control>
    <control>touch</control>
  </recommends>
  <branding>
    <color type="primary" scheme_preference="light">#f8e45c</color>
    <color type="primary" scheme_preference="dark">#f5c211</color>
  </branding>
  <requires>
    <display_length compare="ge">360</display_length>
  </requires>
  <project_license>GPL-3.0-or-later</project_license>
  <description>
    <p>
      A simple and fun game of crosswords. Load your crossword files,
      or play one of the included games. Features include:
    </p>
    <ul>
      <li>Support for shaped and colored crosswords</li>
      <li>Loading .ipuz, .jpuz, .xd, and .puz files</li>
      <li>Hint support, such as showing mistakes and suggesting words</li>
      <li>Dark mode</li>
      <li>Locally installed crosswords as well as support for 3rd party downloaders</li>
    </ul>
  </description>
  <url type="homepage">https://gitlab.gnome.org/jrb/crosswords</url>
  <url type="bugtracker">https://gitlab.gnome.org/jrb/crosswords/-/issues</url>
  <url type="donation">https://www.gnome.org/donate/</url>
  <url type="translate">https://gitlab.gnome.org/jrb/crosswords/-/tree/master/po</url>
  <releases>
    <release version="0.3.14" date="2025-01-07" type="stable">
      <description>
        <p>
          This release just updates to the latest dependencies, along
          with some minor UI fixes. Significant work was done behind
          the scenes to the word-list, which primarily benefited the
          editor.
        </p>
      </description>
    </release>
    <release version="0.3.13.3" date="2024-05-21" type="stable">
      <description>
        <p>
          This release adds the following improvements:
        </p>
        <ul>
          <li>Fix rendering bug when reveal error is disabled</li>
          <li>Warn when trying to load an invalid file</li>
          <li>Many behavioral fixes for keyboard nav</li>
          <li>Change undo to undo/redo differently depending on the direction</li>
          <li>Support loading .xd crosswords</li>
        </ul>
      </description>
    </release>
  </releases>
  <project_group>GNOME</project_group>

  <content_rating type="oars-1.1">
    <content_attribute id="language-profanity">mild</content_attribute>
  </content_rating>

  <launchable type="desktop-id">@app_id@.desktop</launchable>
  <screenshots>
    <screenshot type="default">
      <image type="source">https://gitlab.gnome.org/jrb/crosswords/-/raw/master/data/images/a-dogs-day.png</image>
      <caption>A crossword in the shape of a dog's face</caption>
    </screenshot>
    <screenshot>
      <image type="source">https://gitlab.gnome.org/jrb/crosswords/-/raw/master/data/images/cats-and-dogs.png</image>
      <caption>Crosswords showing a screen for selecting puzzles</caption>
    </screenshot>
    <screenshot>
      <image type="source">https://gitlab.gnome.org/jrb/crosswords/-/raw/master/data/images/reveal.png</image>
      <caption>A crossword with wrong answers highlighted</caption>
    </screenshot>
    <screenshot>
      <image type="source">https://gitlab.gnome.org/jrb/crosswords/-/raw/master/data/images/dark.png</image>
      <caption>The first crossword shown in dark mode </caption>
    </screenshot>
  </screenshots>
</component>
