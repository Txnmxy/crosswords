# Notes on packaging GNOME Crosswords

This doc aims to provide hints to those packaging Crosswords for other
platforms.

## Version 0.3.14

* The editor has an optional dependency on libadwaita-1.7beta. It will
  compile with and without that version. If the newer library is
  present, a substantially fuller clue details section will
  appear. Future versions will just require 1.7 once it's shipped in
  GNOME.

* We now install three word list resources:
    * player.gresource — 20M
    * broda.gresource — 66M
    * wordnik.gresource — 42M

  The `player` dict should be included with the player, while `broda`
  and `wordnik` should be shipped with the editor. The difference in
  size is because the editors have definitions included now which
  expands them considerably. It's possible to shrink the player
  substantially down the line if the size is a problem.

* It is no longer practical for packagers to generate the word list
  resources from scratch. The definition files take a couple hours to
  build, and require downloading a 20Gb intermediate dump of
  wiktionary's database. That intermediate dump takes over a day to
  generate from scratch, so we use a pre-generated version. I build
  the word lists on my computer and check them in to git.

    * The scripts to reproduce the word lists are in
      `scripts/build-wordlist-defs.sh` and
      `scripts/build-wordlist-index.sh`. Instructions on how to use
      the scripts are in `docs/word-entry.md`. In practice, if someone
      wants to double check them with the pregenerated definitions,
      running those scripts on another computer (of the same
      endianness) will produce byte-for-byte identical word lists.

* A consequence of the intermediate files being generated and shipped
  in advance is that we **no longer run on big endian
  machines**. There's open work needed to do to byteswap when parsing
  the word list. I don't know of any practical shippments on those
  machines, so haven't prioritized it.
