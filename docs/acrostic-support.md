# Adding Acrostic Support to Crosswords
**Status:** Final | Implemented | December 2023
**Author:** Tanmay Patil

Acrostic puzzles are two-pane puzzles. Typically, they consist of two
sections. There's a main grid that spells out a sentence or phrase,
with each cell containing a number. Then, a set of clues, whose answers
are made up from the letters of the grids. The first letter of each
answer traditionally spells out the final answer to the clue.

For example, if the first section is a quote, then the first letter of
the clues may spell out the author of the quote. There's more about
this type of puzzle on the [wikipedia
page](https://en.wikipedia.org/wiki/Acrostic_(puzzle)).

![Example Acrostic](acrostic-puzzle-wikipedia.jpg)<br>
_Example Acrostic puzzle from wikipedia_

## Overall Plan

### Prep work
- [X] Decide between whether acrostics should inherit from
      IpuzCrossword. Answer: It should
- [X] Create a couple sample acrostic .ipuz files

### libipuz
- [ ] Add acrostic support to libipuz
    - [X] Add new puzzle type to libipuz
        - [ ] Add documentation of any quirks or expectations
    - [X] Add some Acrostic test harness.
    - [X] Support loading acrostics
        - [X] Fix libipuz #22
        - [X] Add getter for answer clue
            - [X] Remove clue from clueset when appropriate
        - [ ] Add linters
        - [X] (Optional) Add fixup functions

### Crosswords
- [X] Extend PlayXword
    - [X] Write a PlayAnswer widget
        - [X] Update XwordState to be able to handle AnswerGrid widgets
    - [X] Write PlayAcrosticList that can be used as a clue list for
          acrostics
    - [X] Adjust style for focus changes
- [X] Add a test suite for acrostics in XwordState
