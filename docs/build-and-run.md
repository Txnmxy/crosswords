# Build instructions

*Crosswords* and *Crossword Editor* build on most modern Linux
distributions and MacOS ([build hints](docs/macos-build.md)). It was
developed primarily on Fedora and OpenSUSE and should build easily on
recent releases of those platforms. It has dependencies on:

* meson 1.1
* glib 2.82
* GTK 4.16
* libadwaita-1.6

It also follow libipuz development extremely closely. These will
be built as submodules if not installed.

There are multiple approaches supported to to build Crosswords from
source:

* *Flatpak* — Best for testing the latest version
* *Provided container image* — Best for doing development
* *Directly on the host* — Best for targeting a sepcific platform

> **Note:** The git history is fairly substantial due to the inclusion
> of the word list data. To download crosswords for building/testing
> only, use:
>
> ```shell
> $ git clone --depth=1 https://gitlab.gnome.org/jrb/crosswords.git
> ```
>
> This will cut the size/bandwidth requirement down by a third.

## Flatpak

The easiest way to build and test Crosswords from source is by
building a flatpak. This command will build and install it:

```shell
$ git clone --depth=1 https://gitlab.gnome.org/jrb/crosswords.git
$ cd crosswords
$
$ # For the game
$ flatpak-builder  --force-clean _flatpak/ org.gnome.Crosswords.Devel.json  --user --install
$
$ # For the editor
$ flatpak-builder  --force-clean _flatpak/ org.gnome.Crosswords.Editor.Devel.json  --user --install
$ 
$ # To run the flatpak:
$ flatpak run org.gnome.Crosswords.Devel
```

This approach is much less convenient for development.

## Local build from a container image

An easy way to set up a development environment is to use a container
image. We provide an image for Crosswords that has all the necessary
dependencise for it already installed. It is used for the continuous
integration pipeline (CI), so you can have exactly the same setup on
your own machine. We recommend using `distrobox` (a useful wrapper
around podman) to work with the container.

To do this, first install `podman` and `distrobox` on your distro, and
grab crosswords from git.

> **NOTE:** Not every distro has distrobox within its package
> system. The upstream github page has [detailed
> alternative](https://github.com/89luca89/distrobox?tab=readme-ov-file#alternative-methods)
> instructions on how to install it. YMMV.

```shell
$ # If you forked the repo for development, obviously use your URL intsead
$ git clone https://gitlab.gnome.org/jrb/crosswords.git
```

Next, run the `ci/pull-container-image.sh` script:

```shell
$ cd crosswords   # wherever you did your "git clone"
$ bash ci/pull-container-image.sh
```

This script will invoke `podman pull` to download the container image
we use for development. When complete, the script will give detailed
instructions on how to use `distrobox` to build and install Crosswords
inside the container:

```
$ bash ci/pull-container-image.sh
Now run this:

  distrobox create --image $IMAGE_NAME --name crosswords
  distrobox enter crosswords

Once inside the container, you can build and install with this:

  source ci/env.sh
  sudo --preserve-env=PATH,RUSTUP_HOME sh ci/build-and-install.sh

You can just run crosswords on the shell afterwards.  It is installed to
the overlay /usr filesystem, and will not interfere with your system:

  crosswords
$
```

Run those commands from the script with the correct $IMAGE_NAME copied
from the instructions.

Now you can build and install Crosswords while inside the container.
With the instructions above, the `crosswords` program will get
installed to the container's overlay file system for `/usr` without
affecting your main system. Simply call `distrobox enter crosswords`
whenever you want to develop against the code base.

> **NOTE:** When done with this approach, you can remove the image by
> calling `distrobox rm crosswords`.

## Local build

You can use standard meson tools to build crosswords locally on
Linux. You'll need recent versions of most libraries to get this to
build. In general, we test crosswords on the latest released Fedora,
openSUSE, and Ubuntu, but it's not guaranteed to build on older
versions of linux.

```shell
$ meson setup _build -Dlocaledir=/usr/share/locale
$ ninja -C _build
```

> **NOTE:** We set localedir just so that we can find translations of
> language names. It's not necessary, but there will be a runtime
> warning about missing translations without it.

> **NOTE:** We require a relatively new version of meson to build
> Crosswords. You can install a local version of meson that's
> sufficiently new by running `pip3 install --user meson`.

### Load .puz files (Optional)

In order to use the convertor to load other crossword types, you need
to install some python dependencies. The easiest way to do this is to
use pip:

```shell
$ # Set up the virtualenv first (see note)
$ pip install -r requirements.txt
$ pip install --no-deps -r requirements.no-deps.txt
```

> **NOTE:** If you're not running this in a VM, we _strongly_
> recommended that you use `virtualenv` to setup a python environment
> before using
> pip. [Here's](https://developer.fedoraproject.org/tech/languages/python/python-installation.html#using-virtual-environments)
> an example of how to do so on fedora.

### Running Crosswords without installation

Running it locally out of the `builddir` is a little more involved as
it requires some environment variables set to work. To make this
simple and avoid a full system installation, a `run` script is
included.

Use it as follows:

```shell
$ cd _build/

$ # To run the game
$ ./run ./src/crosswords

$ # To run the crossword editor
$ ./run ./src/crossword-editor

$ # To use the convertor
$ ./run ./tools/ipuz-convertor -i puzzle.puz -o /path/to/puzzle.ipuz

$ # To debug the game
$ ./run gdb ./src/crosswords

$ # To run crossword tests
$ ./run ninja test
```

### Loading Puzzle Sets

If you want to test the game with another puzzle-set without installation, you can use the `PUZZLE_SET_PATH` and `PATH` environment variables. As an example:

```shell
$ PATH=~/Projects/puzzle-sets-xword-dl/ PUZZLE_SET_PATH=~/Projects/puzzle-sets-xword-dl/_build/puzzle-sets/ ./run src/crosswords
```

# Common problems

## gschemas.compiled

Meson doesn't rebuild the compiled gschemas file when the source gschema file changes. If you see an error that looks similar to this after a rebuild:

```shell
$ ./run src/crosswords
(crosswords:100131): GLib-GIO-ERROR **: 09:46:51.527: Settings schema 'org.gnome.Crosswords' does not contain a key named 'hidden-puzzle-sets'
Trace/breakpoint trap (core dumped)
```

You'll have to recreate the compiled file. Assuming you're still in _build, this should fix it:

```shell
$ rm data/gschemas.compiled
$ ninja -C .
```
