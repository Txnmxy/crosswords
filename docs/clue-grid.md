# ClueGrid widget
**Status**: Final | Implemented | July 2024

## Problem Statement

An acrostic puzzle consist of two parts. The first part is a main grid
that represents a quote or text. The second part is a set of clues whose
answers are made up from the letter of the main grids. In order to solve
the puzzle, we require a means to input the answers for individual clues
as well.

To facilitate this, `ClueGrid` widget is implemented for allowing
users to input their answer for respective clue in the answer cell
as well as for editing/selecting clues. However, we need to ensure that
changes made in the `ClueGrid` widget are reflected in the main grid.

## Navigation

In addition to synchronization changes between the grid and the Answer widget,
Answer widget will also play it's role in navigation within the acrostic puzzle.
When the focus is on the main grid, the navigation will follow orderly progression,
moving from one cell to the next in a linear manner. However, when the focus is
on the ClueGrid widget, the navigation will be based on the individual clue.
It will reflect the next cell in the specific clue rather than predetermined
linear sequence.
