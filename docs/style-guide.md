# Style Guide

We use the GTK style by default.

## Random Style hints
* Declare variables at the top of the scope, in order they're
  used. Mid-function declarations are discouraged.
* Always use `g_autofree` and `g_autoptr` where possible. Do one per
  line, and make sure they're initialized to NULL.
* Include the minimum number of header files, and list them in
  alphabetical order.
* Use C-style comments `/* . . . */` instead of C++-style `//`
   * Exception: If you have a debugging print-statement that's often
     used, feel free to use `//`. Ex:
```C
  //puzzle_set_model_print (self);
```

## FIXME messages

If you're going to leave a FIXME in the code, please put the domain in
parens. That make it easy to grep for all common areas that may need
fixing. As an example:

```C
/* FIXME(css): This color is hardcoded instead of css based. See bug #xxx */
```

We don't use TODO or have a TODO.md file. Please file bugs for all
other issues.

Common FIXME domain reminders:
* *error*: an unhandled error, that probably needs a better message to
  the user than a g_warning.
* *refactor*: clean up an egregiously messy area of code
* *optimization*: a chunk of code is unnecessarily slow/big
* *css*: Something that should be moved to be a CSS setting
* *gtk/adwaita/libfoo*: Work around for an issue in a
  dependency. Often includes the version number it was fixed or
  introduced.
* *magichars*: A hardcoded constant in the code, that should be
  centralized or made configurable
* *mystery*: Something we don't understand in the code and couldn't
  figure out.

## File order

Within a file, we use the following section order. Keep two newlines
between each section, and one newline within each section:

* License
* Includes
* Local #defines and enums. Global tables
* Object properties and signals
* Object structs
* Local headers
* G_DEFINE_TYPE()
* Object methods, with small helper functions immediately above the caller
* Local methods, in order they're called
* Public methods
* Global helper functions

Note, this is mostly aspirational, and has settled over time. Very few
files follow this to the letter

## Glossary

* **puzzle_id**: Identifier of puzzles in resources. This starts at 1
  instead of 0
