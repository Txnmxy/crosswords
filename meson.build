project('crosswords', 'c',
        version: '0.3.15',
    meson_version: '>= 1.1.0',
  default_options: [ 'warning_level=2',
                     'c_std=gnu11',
                   ],
)

fs = import('fs')
gnome = import('gnome')
i18n = import('i18n')

python = find_program('python3')

if get_option('development')
  app_id = 'org.gnome.Crosswords.Devel'
  edit_app_id = 'org.gnome.Crosswords.Editor.Devel'
else
  app_id = 'org.gnome.Crosswords'
  edit_app_id = 'org.gnome.Crosswords.Editor'
endif

extensionsdir = join_paths(get_option('prefix'), 'extensions')
libexecdir = join_paths(get_option('prefix'), get_option('libexecdir'))
liblocaledir = join_paths(get_option('prefix'), get_option('libdir'), 'locale')
localedir = join_paths(get_option('prefix'), get_option('localedir'))
pkgdatadir = join_paths(get_option('prefix'), get_option('datadir'), 'crosswords')
datadir = join_paths(get_option('prefix'), get_option('datadir'))

iso_codes = dependency('iso-codes')
iso_codes_prefix = iso_codes.get_variable(pkgconfig: 'prefix')

config_h = configuration_data()
config_h.set('DEVELOPMENT_BUILD', get_option('development'))
config_h.set_quoted('APP_ID', app_id)
config_h.set_quoted('EDIT_APP_ID', edit_app_id)
config_h.set_quoted('GETTEXT_PACKAGE', 'crosswords')
config_h.set_quoted('LOCALEDIR', localedir)
config_h.set_quoted('LIBEXECDIR', libexecdir)
config_h.set_quoted('EXTENSIONSDIR', extensionsdir)
config_h.set_quoted('PYTHON_VERSION', 'python' + python.version())
config_h.set_quoted('ISO_CODES_PREFIX', iso_codes_prefix)
config_h.set_quoted('LIBLOCALEDIR', liblocaledir)

config_h.set_quoted('PACKAGE_ICON_NAME', app_id)
config_h.set_quoted('PACKAGE_VERSION', meson.project_version())

add_project_arguments(
  '-I' + meson.project_build_root(),
  '-Wno-unused-parameter',
  #temporarily disable this warning while librsvg triggers it
  '-Wno-expansion-to-defined',
  language: 'c')

gio_dep = [
  dependency('gio-2.0', version: '>= 2.82'),
  dependency('gio-unix-2.0', version: '>= 2.82'),
]

json_glib_dep = dependency('json-glib-1.0')

gtk4_req_version = '4.14'
gtk4_req = '>= @0@'.format(gtk4_req_version)

gtk4_dep = dependency('gtk4', version: gtk4_req,
                      default_options: [
                        'x11-backend=false',
                        'documentation=false',
                        'build-tests=false',
                      ])

librsvg_dep = dependency('librsvg-2.0', version: '>= 2.58.0')

# temporary optional depend. Change required to be true to force it to
# build with the wrapbox
libadwaita_wrap_box_version = '1.7.alpha'
libadwaita_req = '>= @0@'.format(libadwaita_wrap_box_version)
libadwaita_dep = dependency('libadwaita-1', required: false,
                            version: libadwaita_req,
                            default_options: [
                              'examples=false',
                              'introspection=disabled',
                              'tests=false',
                              'vapi=false',
                            ])
if libadwaita_dep.found()
  message ('Using AdwWrapBox')
  config_h.set('HAVE_ADW_WRAP_BOX', true)
  have_wrap_box = true
else
  libadwaita_dep = dependency('libadwaita-1', required: true,
                              version: '>= 1.5.0',
                              default_options: [
                                'examples=false',
                                'introspection=disabled',
                                'tests=false',
                                'vapi=false',
                              ])
  have_wrap_box = false
endif

libipuz_dep = dependency('libipuz-0.5', version: '>= 0.5.1')

word_list_inc = include_directories('src')

# Variables used to translate puzzle_sets
po_dir = meson.current_source_dir() / 'po'
all_po = []
foreach lang: fs.read(po_dir / 'LINGUAS').split('\n')
  if lang == ''
    continue
  endif
  all_po += files('po/' + lang + '.po')
endforeach

# Write out crosswords-config.h
configure_file(
  output: 'crosswords-config.h',
  configuration: config_h,
)


subdir('data')
subdir('src')
subdir('puzzle-sets')
subdir('word-lists')
subdir('tools')
subdir('po')
subdir('thumbnailer')

run_data = configuration_data()
run_data.set('CROSSWORD_BUILDDIR', meson.current_build_dir())
run_data.set('CROSSWORD_SRCDIR', meson.current_source_dir())
configure_file(
          input: 'run.in',
         output: 'run',
  configuration: run_data)

# This is for rebuilding the index file
run_target('build-wordlist-index',
  command : 'scripts/build-wordlist-index.sh')

# For rebuilding definitions
run_target('build-wordlist-defs',
  command : 'scripts/build-wordlist-defs.sh')

gnome.post_install(
  gtk_update_icon_cache: true,
  glib_compile_schemas: true,
  update_mime_database: true,
  update_desktop_database: true,
)
