#!/bin/sh

BRODA_INPUT="word-lists/peter-broda-wordlist__full-text__scored.txt"
WORDNIK_INPUT="word-lists/wordlist-20210729.txt"
TEST_INPUT="word-lists/test-words.txt"


cd "${MESON_SOURCE_ROOT}"

echo "Creating Player list"
"${MESON_BUILD_ROOT}/src/gen-word-list-resource" \
    --identifier player \
    --directory "${MESON_BUILD_ROOT}/word-lists/" \
    --display-name "Crossword Player" \
    --import-format BRODA-FULL \
    --visibility "player" \
    $BRODA_INPUT

echo "Creating broda word list"
"${MESON_BUILD_ROOT}/src/gen-word-list-resource" \
    --identifier broda \
    --directory "${MESON_BUILD_ROOT}/word-lists/" \
    --display-name "Peter Broda's Wordlist" \
    --import-format BRODA-FULL \
    --visibility "editor" \
    $BRODA_INPUT

echo "Creating wordnik word list"
"${MESON_BUILD_ROOT}/src/gen-word-list-resource" \
    --identifier wordnik \
    --directory "${MESON_BUILD_ROOT}/word-lists/" \
    --display-name "Wordnik" \
    --import-format BRODA-FULL \
    --visibility "editor" \
    $WORDNIK_INPUT

echo "Creating test word list"
"${MESON_BUILD_ROOT}/src/gen-word-list-resource" \
    --identifier test \
    --directory "${MESON_BUILD_ROOT}/word-lists/" \
    --display-name "Test Words" \
    --import-format BRODA-FULL \
    --visibility "test" \
    $TEST_INPUT


echo "Wrote updates in ${MESON_BUILD_ROOT}/word-lists/"
echo "Check for correctness, then update the copy in ${MESON_SOURCE_ROOT}/word-lists/ and update git."
echo "You'll also have to rebuild the .gresource files by calling `ninja -C ${MESON_BUILD_ROOT}`"
