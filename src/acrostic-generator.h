/* acrostic-generator.h
 *
 * Copyright 2023 Tanmay Patil <tanmaynpatil105@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once


#include <libipuz/libipuz.h>
#include <glib-object.h>

G_BEGIN_DECLS


typedef enum
{
  ACROSTIC_GENERATOR_ERROR_SOURCE_TOO_SHORT,
  ACROSTIC_GENERATOR_ERROR_SOURCE_NOT_IN_QUOTE,
} AcrosticGeneratorError;

#define ACROSTIC_GENERATOR_ERROR acrostic_generator_error_quark ()

GQuark acrostic_generator_error_quark (void);


#define ACROSTIC_TYPE_GENERATOR (acrostic_generator_get_type())
G_DECLARE_FINAL_TYPE (AcrosticGenerator, acrostic_generator, ACROSTIC, GENERATOR, GObject);


AcrosticGenerator      *acrostic_generator_new               (void);
void                    acrostic_generator_set_min_word_size (AcrosticGenerator *self,
                                                              guint              new_size);
void                    acrostic_generator_set_max_word_size (AcrosticGenerator *self,
                                                              guint              new_size);
gboolean                acrostic_generator_set_text          (AcrosticGenerator *self,
                                                              const gchar       *quote_str,
                                                              const gchar       *source_str,
                                                              GError           **error);
void                    acrostic_generator_set_seed          (AcrosticGenerator *self,
                                                              guint32            seed);
void                    acrostic_generator_run               (AcrosticGenerator *self);



G_END_DECLS
