/* test-utils.c
 *
 * Copyright 2021 Federico Mena
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include <glib.h>
#include <locale.h>
#include "grid-layout.h"
#include "play-grid.h"

G_GNUC_WARN_UNUSED_RESULT static GridState *
load_state (const char *filename)
{
  char *path = g_test_build_filename (G_TEST_DIST, filename, NULL);
  GError *error = NULL;
  IpuzPuzzle *puzzle = ipuz_puzzle_new_from_file (path, &error);
  GridState *state;

  g_assert_no_error (error);
  g_assert (IPUZ_IS_CROSSWORD (puzzle));

  state = grid_state_new (IPUZ_CROSSWORD (puzzle), NULL, GRID_STATE_SOLVE);
  g_assert (state != NULL);

  g_object_unref (puzzle);
  g_free (path);

  return state;
}

#define ITERATIONS 100

static void
grid_bench (void)
{
  GridState *state = load_state ("tests/missing-pets.ipuz");
  gdouble total_elapsed = 0.0;
  guint i;

  GtkWidget *window = gtk_window_new ();
  gtk_widget_set_visible (window, TRUE);

  for (i = 0; i < ITERATIONS; i++) {
    GTimer *timer = g_timer_new ();
    g_timer_start (timer);

    GtkWidget *grid = play_grid_new (PLAY_GRID_FIXED);
    gtk_widget_set_visible (grid, TRUE);
    gtk_window_set_child (GTK_WINDOW (window), grid);
    play_grid_update_state (PLAY_GRID (grid), state, layout_config_default (IPUZ_PUZZLE_CROSSWORD));

    g_timer_stop (timer);
    total_elapsed += g_timer_elapsed (timer, NULL);
    g_timer_reset (timer);
      
    gtk_window_set_child (GTK_WINDOW (window), NULL);
  }

  gdouble average = total_elapsed / ITERATIONS;

  printf ("%u iterations; average per iteration: %f sec\n", ITERATIONS, average);
}

int
main (int argc, char **argv)
{
  setlocale(LC_ALL, "en_US.UTF-8");

  g_test_init (&argc, &argv, NULL);
  gtk_init ();

  grid_bench ();
  return 0;
}
