/* cell-array.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "cell-array.h"


static gint
cell_array_cmp (gconstpointer a,
                gconstpointer b)
{
  IpuzCellCoord *coord_a = (IpuzCellCoord *) a;
  IpuzCellCoord *coord_b = (IpuzCellCoord *) b;

  if (coord_a->column == coord_b->column)
    return coord_a->row - coord_b->row;
  return coord_a->column - coord_b->column;
}

CellArray *
cell_array_new (void)
{
  return (CellArray *) g_array_new (FALSE, FALSE, sizeof (IpuzCellCoord));
}

CellArray *
cell_array_copy (CellArray *src)
{
  CellArray *dest;

  g_return_val_if_fail (src != NULL, NULL);

  dest = cell_array_new ();

  g_array_set_size (dest, cell_array_len (src));
  memcpy (((GArray*)dest)->data, ((GArray*)src)->data, cell_array_len (src) * sizeof (IpuzCellCoord));

  return dest;
}

/* Not a macro so we can pass it into other functions */
void
cell_array_unref (CellArray *cell_array)
{
  g_array_unref ((GArray *) cell_array);
}

void
cell_array_add (CellArray     *cell_array,
                IpuzCellCoord  coord)
{
  guint out;

  g_return_if_fail (cell_array != NULL);

  if (g_array_binary_search ((GArray *) cell_array,
                             &coord,
                             cell_array_cmp,
                             &out))
    return;

  g_array_append_val ((GArray *) cell_array, coord);
  g_array_sort ((GArray *) cell_array, cell_array_cmp);
}

void
cell_array_remove (CellArray     *cell_array,
                   IpuzCellCoord  coord)
{
  guint out;

  g_return_if_fail (cell_array != NULL);

  if (g_array_binary_search ((GArray *) cell_array,
                             &coord,
                             cell_array_cmp,
                             &out))
    g_array_remove_index ((GArray *) cell_array, out);
}

void
cell_array_toggle (CellArray     *cell_array,
                   IpuzCellCoord  coord)
{
  guint out;

  g_return_if_fail (cell_array != NULL);

  if (g_array_binary_search ((GArray *) cell_array,
                             &coord,
                             cell_array_cmp,
                             &out))
    g_array_remove_index ((GArray *) cell_array, out);
  else
    {
      g_array_append_val ((GArray *) cell_array,
                          coord);
      g_array_sort ((GArray *) cell_array, cell_array_cmp);
    }
}

gboolean
cell_array_find (CellArray     *cell_array,
                 IpuzCellCoord  coord,
                 guint         *out)
{
  g_return_val_if_fail (cell_array != NULL, FALSE);

  return g_array_binary_search ((GArray *) cell_array,
                                &coord,
                                cell_array_cmp,
                                out);
}

void
cell_array_print (CellArray *cell_array)
{
  if (cell_array == NULL)
    {
      g_print ("<NULL>\n");
      return;
    }

  g_print ("[");
  for (guint i = 0; i < cell_array->len; i++)
    {
      IpuzCellCoord coord;

      coord = cell_array_index (cell_array, i);
      if (i > 0)
        g_print (" ");
      g_print ("[%u, %u]", coord.row, coord.column);
      if (i != cell_array->len - 1)
        g_print ("\n");
    }
  g_print ("]\n");
}
