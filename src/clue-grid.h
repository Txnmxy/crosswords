/* clue-grid.h
 *
 * Copyright 2023 Tanmay Patil <tanmaynpatil105@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>
#include "play-grid.h"


G_BEGIN_DECLS


#define CLUE_TYPE_GRID (clue_grid_get_type())
G_DECLARE_FINAL_TYPE (ClueGrid, clue_grid, CLUE, GRID, PlayGrid);

GtkWidget *clue_grid_new              (void);
void       clue_grid_set_clue_id      (ClueGrid     *self,
                                       IpuzClueId    clue_id);
void       clue_grid_update_state     (ClueGrid     *self,
                                       GridState    *state,
                                       IpuzClue     *clue,
                                       LayoutConfig  config);
void       clue_grid_focus_cell       (ClueGrid     *self);
void       clue_grid_selection_invert (ClueGrid     *self);
void       clue_grid_select_all       (ClueGrid     *self);


G_END_DECLS
