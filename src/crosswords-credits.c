/* crosswords-credits.c
 *
 * Copyright 2022 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include "crosswords-credits.h"


/* Add additional contributors here: */

const char *crosswords_authors[] = {
  "Jonathan Blandford <jrb@gnome.org>",
  "Federico Mena-Quintero",
  "Philip Goto",
  "Davide Cavalca",
  "Tanmay Patil",
  NULL
};

const char *crosswords_designers[] = {
  "Sam Hewitt",
  NULL
};

const char *crosswords_puzzles[] = {
  "Jonathan Blandford",
  "Rosanna Yuen",
  NULL
};

const char *crosswords_copyright = "© 2021-2024 Jonathan Blandford";
