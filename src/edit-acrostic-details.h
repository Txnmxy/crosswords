/* edit-acrostic-details.h
 *
 * Copyright 2024 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>


G_BEGIN_DECLS


#define EDIT_TYPE_ACROSTIC_DETAILS (edit_acrostic_details_get_type())
G_DECLARE_FINAL_TYPE (EditAcrosticDetails, edit_acrostic_details, EDIT, ACROSTIC_DETAILS, GtkWidget);


void edit_acrostic_details_update           (EditAcrosticDetails *self,
                                             GridState           *grid_state);
void edit_acrostic_details_commit_changes   (EditAcrosticDetails *self);


G_END_DECLS
