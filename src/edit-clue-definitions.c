/* edit-clue-definitions.c
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include "crosswords-enums.h"
#include <libipuz/libipuz.h>
#include <glib/gi18n-lib.h>
#include <adwaita.h>
#include "edit-clue-definitions.h"
#include "word-list-def.h"


struct _EditClueDefinitions
{
  GtkWidget parent_instance;

  gchar *current_filter;
  WordListResource *resource;

  GtkWidget *def_link_label;
  GtkWidget *no_results_label;
  GtkWidget *text_view;
  GtkTextBuffer *text_buffer;
  GtkTextTag *word_tag;
  GtkTextTag *sense_tag;
  GtkTextTag *pos_tag;
  GtkTextTag *gloss_count_tag;
  GtkTextTag *gloss_tag;
};


G_DEFINE_FINAL_TYPE (EditClueDefinitions, edit_clue_definitions, GTK_TYPE_WIDGET);


static void edit_clue_definitions_init       (EditClueDefinitions      *self);
static void edit_clue_definitions_class_init (EditClueDefinitionsClass *klass);
static void edit_clue_definitions_dispose    (GObject                  *object);


static void
edit_clue_definitions_init (EditClueDefinitions *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  /* FIXME(gtk): This tag copies and pastes the ".title-1" CSS value
   * from libadwaita.  At some point (gtk5?) we should set all this
   * from CSS */
  self->word_tag = gtk_text_buffer_create_tag (self->text_buffer, NULL,
                                               "weight", PANGO_WEIGHT_ULTRABOLD,
                                               "scale", 1.81,
                                               NULL);
  self->sense_tag =  gtk_text_buffer_create_tag (self->text_buffer, NULL,
                                                 "weight", PANGO_WEIGHT_MEDIUM,
                                                 NULL);
  self->pos_tag =  gtk_text_buffer_create_tag (self->text_buffer, NULL,
                                               "weight", PANGO_WEIGHT_SEMILIGHT,
                                               NULL);
  self->gloss_count_tag =  gtk_text_buffer_create_tag (self->text_buffer, NULL,
                                                       "weight", PANGO_WEIGHT_SEMILIGHT,
                                                       "family", "monospace",
                                                       NULL);
  self->gloss_tag =  gtk_text_buffer_create_tag (self->text_buffer, NULL,
                                                 "style", PANGO_STYLE_OBLIQUE,
                                                 "weight", PANGO_WEIGHT_MEDIUM,
                                                 NULL);
}

static void
edit_clue_definitions_class_init (EditClueDefinitionsClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = edit_clue_definitions_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-clue-definitions.ui");

  gtk_widget_class_bind_template_child (widget_class, EditClueDefinitions, def_link_label);
  gtk_widget_class_bind_template_child (widget_class, EditClueDefinitions, no_results_label);
  gtk_widget_class_bind_template_child (widget_class, EditClueDefinitions, text_view);
  gtk_widget_class_bind_template_child (widget_class, EditClueDefinitions, text_buffer);

  gtk_widget_class_set_css_name (widget_class, "cluedefinitions");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BOX_LAYOUT);
}

static void
edit_clue_definitions_dispose (GObject *object)
{
  GtkWidget *child;
  EditClueDefinitions *self;

  self = EDIT_CLUE_DEFINITIONS (object);

  g_clear_pointer (&self->current_filter, g_free);
  g_clear_object (&self->resource);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);


  G_OBJECT_CLASS (edit_clue_definitions_parent_class)->dispose (object);
}

static void
update_labels (EditClueDefinitions *self,
               guint                count)
{
  gboolean valid_filter = TRUE;

  /* Handle the central label */
  if (count == 0)
    {
      g_autofree gchar *str = NULL;

      str = g_strdup_printf ("No definition for '%s'",
                             self->current_filter);
      gtk_label_set_label (GTK_LABEL (self->no_results_label), str);
    }
  gtk_widget_set_visible (self->no_results_label, (count == 0));


  /* Web link */
  for (const gchar *ptr = self->current_filter;
       ptr && ptr[0];
       ptr = g_utf8_next_char (ptr))
    {
      if (ptr[0] == '?')
        {
          valid_filter = FALSE;
          break;
        }
    }

  if (valid_filter)
    {
      g_autofree gchar *link_markup = NULL;
      g_autofree gchar *lower = NULL;

      lower = g_utf8_strdown (self->current_filter, -1);
      /* Put a warning label up when we have a valid filter */

      /* Let the user look up the word */
      link_markup = g_strdup_printf
        ("%s <a href=\"https://en.wiktionary.org/wiki/%s\">%s</a> |"
         " <a href=\"https://en.wikipedia.org/wiki/%s\">%s</a>",
         _("Lookup in"), lower, _("Wiktionary"),
         lower, _("Wikipedia"));
         
      gtk_label_set_markup (GTK_LABEL (self->def_link_label), link_markup);
    }
      
  gtk_widget_set_visible (self->def_link_label, valid_filter);
}

static void
update_count (EditClueDefinitions *self,
              guint                count)
{
  GtkWidget *stack;
  AdwViewStackPage *page;

  stack = gtk_widget_get_ancestor (GTK_WIDGET (self), ADW_TYPE_VIEW_STACK);
  page = adw_view_stack_get_page (ADW_VIEW_STACK (stack), GTK_WIDGET (self));
  adw_view_stack_page_set_badge_number (ADW_VIEW_STACK_PAGE (page), count);
}

static void
update_buffer (EditClueDefinitions *self)
{
  g_autoptr (GArray) defs = NULL;
  GtkTextIter iter;
  const gchar *text;
  guint count = 0;

  /* Clear the buffer to start */
  gtk_text_buffer_set_text (self->text_buffer, "", 0);
  defs = word_list_def_get_definitions (self->resource,
                                        self->current_filter);

  if (defs == NULL)
    goto out;

  count = defs->len;

  gtk_text_buffer_get_iter_at_offset (self->text_buffer, &iter, 0);
  gtk_text_buffer_begin_irreversible_action (self->text_buffer);

  /* Loops of loops! */
  for (guint def_index = 0; def_index < defs->len; def_index++)
    {
      WordListDef *def;

      def = g_array_index (defs, WordListDef *, def_index);
      text = word_list_def_get_word_str (def);

      gtk_text_buffer_insert_with_tags (self->text_buffer, &iter, text, -1, self->word_tag, NULL);
      gtk_text_buffer_insert (self->text_buffer, &iter, "\n", -1);

      for (guint i = 0; i < word_list_def_get_n_entries (def); i++)
        {
          g_autofree gchar *pos_text = NULL;
          guint gloss_count = 1;

          gtk_text_buffer_insert_with_tags (self->text_buffer, &iter,
                                            word_list_def_entry_get_header (def, i), -1,
                                            self->sense_tag, NULL);
          gtk_text_buffer_insert (self->text_buffer, &iter, " ", -1); //U+2001 → em quad
          pos_text = g_strdup_printf ("(%s)\n",word_list_def_entry_get_pos (def, i));
          gtk_text_buffer_insert_with_tags (self->text_buffer, &iter,
                                            pos_text, -1, self->pos_tag, NULL);

          for (guint j = 0; j < word_list_def_entry_get_n_senses (def, i); j++)
            {
              for (guint k = 0; k < word_list_def_entry_sense_get_n_glosses (def, i, j); k++)
                {
                  g_autofree gchar *gloss_count_str = NULL;
                  g_autofree gchar *gloss = NULL;

                  gloss_count_str = g_strdup_printf ("  %d.", gloss_count++);
                  gtk_text_buffer_insert_with_tags (self->text_buffer, &iter, gloss_count_str, -1, self->gloss_count_tag, NULL);

                  gloss = g_strdup_printf (" %s\n",
                                           word_list_def_entry_sense_get_gloss (def, i, j, k));
                  gtk_text_buffer_insert_with_tags (self->text_buffer, &iter, gloss, -1, self->gloss_tag, NULL);
                }
            }
          gtk_text_buffer_insert (self->text_buffer, &iter, "\n", -1);
        }
    }
  gtk_text_buffer_end_irreversible_action (self->text_buffer);

 out:
  update_labels (self, count);
  update_count (self, count);
}

/* Public methods */

void
edit_clue_definitions_update (EditClueDefinitions *self,
                              WordListResource    *resource,
                              const gchar         *new_filter)
{
  g_assert (new_filter);

  if ((g_strcmp0 (self->current_filter, new_filter) == 0) &&
      (self->resource == resource))
    return;

  g_clear_pointer (&self->current_filter, g_free);
  self->current_filter = g_strdup (new_filter);

  g_object_ref (resource);
  g_clear_object (&self->resource);
  self->resource = resource;

  update_buffer (self);
}
