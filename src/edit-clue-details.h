/* edit-clue-details.h
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>
#include "grid-layout.h"
#include "grid-state.h"


G_BEGIN_DECLS


#define EDIT_TYPE_CLUE_DETAILS (edit_clue_details_get_type())
G_DECLARE_FINAL_TYPE (EditClueDetails, edit_clue_details, EDIT, CLUE_DETAILS, GtkWidget);


void edit_clue_details_update           (EditClueDetails *self,
                                         GridState       *clues_state,
                                         LayoutConfig     layout_config);
void edit_clue_details_commit_changes   (EditClueDetails *self);
void edit_clue_details_selection_invert (EditClueDetails *self);
void edit_clue_details_select_all       (EditClueDetails *self);


G_END_DECLS
