/* edit-clue-info-section.c
 *
 * Copyright 2024 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include "crosswords-enums.h"
#include <glib/gi18n-lib.h>
#include <adwaita.h>
#include "edit-clue-info-section.h"
#include "word-list-def.h"


enum
{
  PROP_0,
  PROP_TITLE,
  PROP_SUBTITLE,
  N_PROPS,
};

enum
{
  WORD_CLICKED,
  N_SIGNALS,
};

static GParamSpec *obj_props[N_PROPS] =  {NULL, };
static guint obj_signals[N_SIGNALS] = { 0 };


struct _EditClueInfoSection
{
  GtkWidget parent_instance;

  gchar *title;
  gchar *subtitle;

  GtkWidget *title_label;
  GtkWidget *subtitle_label;
  GtkWidget *wrap_box;
  WordListResource *resource;

  /* Context menu */
  GtkWidget *context_popover;
  gchar *context_word;
};


G_DEFINE_FINAL_TYPE (EditClueInfoSection, edit_clue_info_section, GTK_TYPE_WIDGET);


static void edit_clue_info_section_init         (EditClueInfoSection      *self);
static void edit_clue_info_section_class_init   (EditClueInfoSectionClass *klass);
static void edit_clue_info_section_set_property (GObject                  *object,
                                                 guint                     prop_id,
                                                 const GValue             *value,
                                                 GParamSpec               *pspec);
static void edit_clue_info_section_get_property (GObject                  *object,
                                                 guint                     prop_id,
                                                 GValue                   *value,
                                                 GParamSpec               *pspec);
static void edit_clue_info_section_dispose      (GObject                  *object);
static void activate_clipboard_copy_cb          (GtkWidget                *widget,
                                                 const char               *name,
                                                 GVariant                 *parameter);
static void activate_link_open_at_cb            (GtkWidget                *widget,
                                                 const char               *name,
                                                 GVariant                 *parameter);
static void click_gesture_pressed_cb            (EditClueInfoSection      *self,
                                                 int                       n_press,
                                                 double                    widget_x,
                                                 double                    widget_y,
                                                 GtkGesture               *gesture);


static void
edit_clue_info_section_init (EditClueInfoSection *self)
{
  GtkBoxLayout *box_layout;
  GtkGesture *gesture = NULL;

  gtk_widget_init_template (GTK_WIDGET (self));

  box_layout = GTK_BOX_LAYOUT (gtk_widget_get_layout_manager (GTK_WIDGET (self)));
  gtk_box_layout_set_spacing (box_layout, 12);
  gtk_orientable_set_orientation (GTK_ORIENTABLE (box_layout), GTK_ORIENTATION_VERTICAL);

  /* Create the gesture */
  /* Micro-optimization, but by putting the gesture on the parent we
   * can avoid thousands of additional unused gestures from being
   * created on each label*/
  gesture = gtk_gesture_click_new ();
  g_signal_connect_swapped (gesture, "pressed",
                            G_CALLBACK (click_gesture_pressed_cb), self);
  gtk_gesture_single_set_button (GTK_GESTURE_SINGLE (gesture), 0);
  gtk_event_controller_set_propagation_phase (GTK_EVENT_CONTROLLER (gesture),
                                              GTK_PHASE_CAPTURE);
  gtk_widget_set_parent (self->context_popover, GTK_WIDGET (self));
  gtk_widget_add_controller (GTK_WIDGET (self), GTK_EVENT_CONTROLLER (gesture));
}

static void
edit_clue_info_section_class_init (EditClueInfoSectionClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->set_property = edit_clue_info_section_set_property;
  object_class->get_property = edit_clue_info_section_get_property;
  object_class->dispose = edit_clue_info_section_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-clue-info-section.ui");

  gtk_widget_class_bind_template_child (widget_class, EditClueInfoSection, title_label);
  gtk_widget_class_bind_template_child (widget_class, EditClueInfoSection, subtitle_label);
  gtk_widget_class_bind_template_child (widget_class, EditClueInfoSection, wrap_box);
  gtk_widget_class_bind_template_child (widget_class, EditClueInfoSection, context_popover);

  obj_signals [WORD_CLICKED] =
    g_signal_new ("word-clicked",
                  EDIT_TYPE_CLUE_INFO_SECTION,
		  G_SIGNAL_RUN_FIRST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_STRING);

  obj_props [PROP_TITLE] =
    g_param_spec_string ("title",
                         NULL, NULL,
                         NULL,
                         G_PARAM_READWRITE);
  obj_props [PROP_SUBTITLE] =
    g_param_spec_string ("subtitle",
                         NULL, NULL,
                         NULL,
                         G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  gtk_widget_class_install_action (widget_class, "clipboard.copy", NULL,
                                   activate_clipboard_copy_cb);
  gtk_widget_class_install_action (widget_class, "link.open-at", "s",
                                   activate_link_open_at_cb);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BOX_LAYOUT);
}

static void
edit_clue_info_section_set_property (GObject      *object,
                                     guint         prop_id,
                                     const GValue *value,
                                     GParamSpec   *pspec)
{
  EditClueInfoSection *self;

  self = EDIT_CLUE_INFO_SECTION (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      g_clear_pointer (&self->title, g_free);
      self->title = g_value_dup_string (value);
      break;
    case PROP_SUBTITLE:
      g_clear_pointer (&self->subtitle, g_free);
      self->subtitle = g_value_dup_string (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
edit_clue_info_section_get_property (GObject    *object,
                                     guint       prop_id,
                                     GValue     *value,
                                     GParamSpec *pspec)
{
  EditClueInfoSection *self;

  self = EDIT_CLUE_INFO_SECTION (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      g_value_set_string (value, self->title);
      break;
    case PROP_SUBTITLE:
      g_value_set_string (value, self->subtitle);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
edit_clue_info_section_dispose (GObject *object)
{
  EditClueInfoSection *self;
  GtkWidget *child;

  self = EDIT_CLUE_INFO_SECTION (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_pointer (&self->title, g_free);
  g_clear_pointer (&self->subtitle, g_free);
  g_clear_pointer (&self->context_word, g_free);
  g_clear_object (&self->resource);

  G_OBJECT_CLASS (edit_clue_info_section_parent_class)->dispose (object);
}

/* Public methods */

/* This is the number of definitions we show in a tooltip */
#define N_GLOSSES 3

static gboolean
label_query_tooltip_cb (EditClueInfoSection *self,
                        gint                 x,
                        gint                 y,
                        gboolean             keyboard_mode,
                        GtkTooltip          *tooltip,
                        GtkWidget           *label)
{
  g_autoptr (GArray) defs = NULL;
  g_autoptr (GString) tooltip_markup = NULL;
  const gchar *word;
  guint count = 0;

  word = gtk_label_get_label (GTK_LABEL (label));
  defs = word_list_def_get_definitions (self->resource, word);

  if (defs == NULL)
    return FALSE;

  tooltip_markup = g_string_new (NULL);

  /* Loop 'o death! */
  for (guint i = 0; i < defs->len; i++)
    {
      WordListDef *def;

      def = g_array_index (defs, WordListDef *, i);
      for (guint j = 0; j < word_list_def_get_n_entries (def); j++)
        {
          for (guint k = 0; k < word_list_def_entry_get_n_senses (def, j); k++)
            {
              for (guint l = 0; l < word_list_def_entry_sense_get_n_glosses (def, j, k); l++)
                {
                  const gchar *gloss;

                  gloss = word_list_def_entry_sense_get_gloss (def, j, k, l);
                  if (count < N_GLOSSES && gloss)
                    {
                      g_autofree gchar *line = NULL;

                      line = g_strdup_printf ("<tt>%u.</tt> %s", count + 1, gloss);

                      if (count > 0)
                        g_string_append_c (tooltip_markup, '\n');
                      g_string_append (tooltip_markup, line);
                    }
                  count++;
                }
            }
        }
    }

  if (count == 0)
    return FALSE;

  if (count >= N_GLOSSES)
    {
      g_autofree gchar *the_rest = NULL;

      if (count == N_GLOSSES)
        the_rest = g_strdup_printf ("\n\n<i>%s</i>", _("1 other definition"));
      else
        the_rest = g_strdup_printf ("\n\n<i>%u%s</i>",
                                    (count + 1) - N_GLOSSES,
                                    _(" other definitions"));
      g_string_append (tooltip_markup, the_rest);
    }

  gtk_tooltip_set_markup (tooltip, tooltip_markup->str);

  return TRUE;

}

static void
popup_context_menu (EditClueInfoSection *self,
                    GtkWidget           *context_label,
                    double               x,
                    double               y)
{
  if (x != -1 && y != -1)
    {
      GdkRectangle rect = { x, y, 1, 1 };
      gtk_popover_set_pointing_to (GTK_POPOVER (self->context_popover), &rect);
    }
  else
    gtk_popover_set_pointing_to (GTK_POPOVER (self->context_popover), NULL);

  g_clear_pointer (&self->context_word, g_free);
  self->context_word = g_strdup (gtk_label_get_label (GTK_LABEL (context_label)));

  gtk_accessible_update_property (GTK_ACCESSIBLE (self->context_popover),
                                  GTK_ACCESSIBLE_PROPERTY_LABEL, _("Context menu"),
                                  -1);
  gtk_popover_popup (GTK_POPOVER (self->context_popover));
}

static void
activate_clipboard_copy_cb (GtkWidget  *widget,
                            const char *name,
                            GVariant   *parameter)
{
  EditClueInfoSection *self;
  GdkClipboard *clipboard;

  self = EDIT_CLUE_INFO_SECTION (widget);

  clipboard = gtk_widget_get_clipboard (GTK_WIDGET (self));
  gdk_clipboard_set_text (clipboard, self->context_word);
}

static void
activate_link_open_at_cb (GtkWidget  *widget,
                          const char *name,
                          GVariant   *parameter)
{
  EditClueInfoSection *self;
  GtkWidget *toplevel;
  const gchar *format_str;
  g_autofree gchar *uri = NULL;
  g_autoptr (GtkUriLauncher) launcher = NULL;
  g_autofree gchar *lower = NULL;

  self = EDIT_CLUE_INFO_SECTION (widget);
  toplevel = GTK_WIDGET (gtk_widget_get_root (widget));

  format_str = g_variant_get_string (parameter, NULL);
  lower = g_utf8_strdown (self->context_word, -1);
  uri = g_strdup_printf (format_str, lower);

  /* Fire and forget and hope it works */
  launcher = gtk_uri_launcher_new (uri);
  gtk_uri_launcher_launch (launcher, GTK_WINDOW (toplevel), NULL, NULL, NULL);
}

static void
click_gesture_pressed_cb (EditClueInfoSection   *self,
                          int                    n_press,
                          double                 widget_x,
                          double                 widget_y,
                          GtkGesture            *gesture)
{
  GtkWidget *label;
  GdkEventSequence *sequence;
  GdkEvent *event;
  guint button;
  const gchar *word;

  /* get all the info we need to process this gesture */
  label = gtk_widget_pick (GTK_WIDGET (self),
                           widget_x, widget_y,
                           GTK_PICK_DEFAULT);
  sequence = gtk_gesture_single_get_current_sequence (GTK_GESTURE_SINGLE (gesture));
  event = gtk_gesture_get_last_event (GTK_GESTURE (gesture), sequence);
  button = gtk_gesture_single_get_current_button (GTK_GESTURE_SINGLE (gesture));

  if (label == NULL || !GTK_IS_LABEL (label))
    return;

  /* There are other labels in the section! Don't treat clicks on them
   * like the tages */
  if (label == self->title_label || label == self->subtitle_label)
    return;

  gtk_gesture_set_state (GTK_GESTURE (gesture), GTK_EVENT_SEQUENCE_CLAIMED);

  if (gdk_event_triggers_context_menu (event))
    {
      popup_context_menu (self, label, widget_x, widget_y);
      return;
    }
  else if (button == GDK_BUTTON_PRIMARY)
    {
      word = gtk_label_get_label (GTK_LABEL (label));

      g_signal_emit (self, obj_signals [WORD_CLICKED], 0, word);
      return;
    }
  gtk_gesture_set_state (GTK_GESTURE (gesture), GTK_EVENT_SEQUENCE_DENIED);
}


static GtkWidget *
create_label (EditClueInfoSection *self)
{
  GtkWidget *label;

  /* create the label */
  label = (GtkWidget *) g_object_new (GTK_TYPE_LABEL,
                                      "has-tooltip", TRUE,
                                      NULL);
  g_signal_connect_swapped (label, "query-tooltip",
                            G_CALLBACK (label_query_tooltip_cb),
                            self);
  gtk_widget_add_css_class (label, "word-result");
  gtk_widget_add_css_class (label, "tag");

  return label;
}

void
edit_clue_info_section_update (EditClueInfoSection *self,
                               WordListResource    *resource,
                               GArray              *word_arr,
                               GArray              *attr_arr)
{
  guint count;
  GtkWidget *child;

  g_return_if_fail (EDIT_IS_CLUE_INFO_SECTION (self));
  g_return_if_fail (word_arr != NULL);

  /* Cache the word-list in use */
  g_object_ref (resource);
  g_clear_object (&self->resource);
  self->resource = resource;

  /* update the title label */
  gtk_widget_set_visible (self->title_label, (self->title != NULL));
  if (self->title)
    gtk_label_set_markup (GTK_LABEL (self->title_label), self->title);

  gtk_widget_set_visible (self->subtitle_label, (self->subtitle != NULL));
  if (self->subtitle)
    gtk_label_set_markup (GTK_LABEL (self->subtitle_label), self->subtitle);

  /* Add the tags */
  count = word_arr->len;
  child = gtk_widget_get_first_child (self->wrap_box);

  for (guint i = 0; i < count; i++)
    {
      const gchar *word;
      guint len;
      PangoAttrList *attr_list = NULL;

      if (child == NULL)
        {
          child = create_label (self);
          adw_wrap_box_append (ADW_WRAP_BOX (self->wrap_box), child);
        }

      /* get the word and attr_list for the label, attr_arr can be
       * NULL, and shorter than the label we're looking at. */
      word = g_array_index (word_arr, const gchar *, i);
      len = g_utf8_strlen (word, -1);
      if (attr_arr && len < attr_arr->len)
        attr_list = g_array_index (attr_arr, PangoAttrList *, len);

      gtk_label_set_text (GTK_LABEL (child), word);
      gtk_label_set_attributes (GTK_LABEL (child), attr_list);
      child = gtk_widget_get_next_sibling (child);
    }

  while (child)
    {
      GtkWidget *next = gtk_widget_get_next_sibling (child);
      gtk_widget_unparent (child);
      child = next;
    }

  gtk_widget_set_visible (self->wrap_box, (count > 0));
}
