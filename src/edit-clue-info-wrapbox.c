/* edit-clue-info.c
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include "crosswords-enums.h"
#include <libipuz/libipuz.h>
#include <glib/gi18n-lib.h>
#include <adwaita.h>
#include "edit-clue-info-wrapbox.h"
#include "edit-clue-info-section.h"
#include "word-list.h"


enum
  {
    PROP_0,
    PROP_MODE,
    N_PROPS,
  };

enum
{
  WORD_CLICKED,
  N_SIGNALS,
};

static GParamSpec *obj_props[N_PROPS] =  {NULL, };
static guint obj_signals[N_SIGNALS] = { 0 };


struct _EditClueInfo
{
  GtkWidget parent_instance;

  EditClueInfoMode mode;
  gchar *selection_text;
  WordList *word_list;

  GtkWidget *swindow;
  GtkWidget *section1;
  GtkWidget *section2;
  GtkWidget *section3;
};


G_DEFINE_FINAL_TYPE (EditClueInfo, edit_clue_info, GTK_TYPE_WIDGET);


static void edit_clue_info_init         (EditClueInfo      *self);
static void edit_clue_info_class_init   (EditClueInfoClass *klass);
static void edit_clue_info_set_property (GObject           *object,
                                         guint              prop_id,
                                         const GValue      *value,
                                         GParamSpec        *pspec);
static void edit_clue_info_get_property (GObject           *object,
                                         guint              prop_id,
                                         GValue            *value,
                                         GParamSpec        *pspec);
static void edit_clue_info_dispose      (GObject           *object);
static void word_clicked_cb             (EditClueInfo      *self,
                                         const gchar       *word);


static void
edit_clue_info_init (EditClueInfo *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->word_list = word_list_new ();
}

static void
edit_clue_info_class_init (EditClueInfoClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->set_property = edit_clue_info_set_property;
  object_class->get_property = edit_clue_info_get_property;
  object_class->dispose = edit_clue_info_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-clue-info-wrapbox.ui");

  gtk_widget_class_bind_template_child (widget_class, EditClueInfo, swindow);
  gtk_widget_class_bind_template_child (widget_class, EditClueInfo, section1);
  gtk_widget_class_bind_template_child (widget_class, EditClueInfo, section2);
  gtk_widget_class_bind_template_child (widget_class, EditClueInfo, section3);
  gtk_widget_class_bind_template_callback (widget_class, word_clicked_cb);

  obj_props[PROP_MODE] = g_param_spec_enum ("mode", NULL, NULL,
                                            EDIT_TYPE_CLUE_INFO_MODE,
                                            EDIT_CLUE_INFO_MODE_ANAGRAM,
                                            G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  gtk_widget_class_set_css_name (widget_class, "clueinfo");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BOX_LAYOUT);

  obj_signals [WORD_CLICKED] =
    g_signal_new ("word-clicked",
                  EDIT_TYPE_CLUE_INFO,
		  G_SIGNAL_RUN_FIRST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_STRING);
}

static void
edit_clue_info_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  EditClueInfo *self;

  self = EDIT_CLUE_INFO (object);

  switch (prop_id)
    {
    case PROP_MODE:
      self->mode = g_value_get_enum (value);
      switch (self->mode)
        {
        case EDIT_CLUE_INFO_MODE_ANAGRAM:
          g_object_set (self->section1,
                        "title", _("Anagrams"),
                        "subtitle", _("<i>Example: <tt>\"Confused <u>admirer</u>\"</tt> → <tt>MARRIED</tt></i>"),
                        NULL);
          g_object_set (self->section2,
                        "title", _("Cycles"),
                        "subtitle", _("<i>Example: <tt>\"Cycle <u>seat</u>\"</tt> → <tt>EATS</tt></i>"),
                        NULL);
          break;
        case EDIT_CLUE_INFO_MODE_ALTERNATIONS:
          g_object_set (self->section1,
                        "title", _("Odds"),
                        "subtitle", _("<i>Example: <tt>\"Oddly <u>c</u>h<u>a</u>s<u>t</u>e\"</tt> → <tt>CAT</tt></i>"),
                        NULL);
          g_object_set (self->section2,
                        "title", _("Evens"),
                        "subtitle", _("<i>Example: <tt>\"Even f<u>l</u>o<u>o</u>r<u>s</u>\"</tt> → <tt>LOS</tt></i>"),
                        NULL);
          break;
        case EDIT_CLUE_INFO_MODE_DELETIONS:
          g_object_set (self->section1,
                        "title", _("Headless"),
                        "subtitle", _("<i>Example: <tt>\"Decapitated regent\"</tt> → <tt>(k)ING</tt></i>"),
                        NULL);
          g_object_set (self->section2,
                        "title", _("Tailless"),
                        "subtitle", _("<i>Example: <tt>\"Cropped collar\"</tt> → <tt>RUF(f)</tt></i>"),
                        NULL);
          g_object_set (self->section3,
                        "title", _("Trimmed"),
                        "subtitle", _("<i>Example: <tt>\"M<u>ean</u>s without ends\"</tt> → <tt>EAN</tt></i>"),
                        NULL);
          break;
        case EDIT_CLUE_INFO_MODE_EXTREMES:
          g_object_set (self->section1,
                        "title", _("Extremes"),
                        "subtitle", _("<i>Example: <tt>\"Extremely <u>b</u>us<u>y</u>\"</tt> → <tt>BY</tt></i>"),
                        NULL);
          break;
        default:
          g_assert_not_reached ();
        }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
edit_clue_info_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  EditClueInfo *self;

  self = EDIT_CLUE_INFO (object);

  switch (prop_id)
    {
    case PROP_MODE:
      g_value_set_enum (value, self->mode);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
edit_clue_info_dispose (GObject *object)
{
  EditClueInfo *self;
  GtkWidget *child;

  self = EDIT_CLUE_INFO (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_object (&self->word_list);
  g_clear_pointer (&self->selection_text, g_free);

  G_OBJECT_CLASS (edit_clue_info_parent_class)->dispose (object);
}

static void
word_clicked_cb (EditClueInfo *self,
                 const gchar  *word)
{
  g_signal_emit (self, obj_signals [WORD_CLICKED], 0, word);
}


/* Public methods */

static void
clear_attr_array (PangoAttrList **ptr)
{
  pango_attr_list_unref (*ptr);
  *ptr = NULL;
}


static GArray *
create_attr_array (void)
{
  GArray *arr;

  arr = g_array_new (FALSE, TRUE, sizeof (PangoAttrList *));
  g_array_set_clear_func (arr, (GDestroyNotify) clear_attr_array);


  return arr;
}

static void
attr_arr_set_attr (GArray        *attr_arr,
                   guint          index,
                   PangoAttrList *attr_list)
{
  PangoAttrList **ptr;

  /* guard against going off the end. This happens with eg, and
     endless word that's already max_len */
  if (index >= attr_arr->len)
    g_array_set_size (attr_arr, index + 1);

  pango_attr_list_ref (attr_list);

  ptr = &(g_array_index (attr_arr, PangoAttrList *, index));
  *ptr = attr_list;
}

static void
update_count (EditClueInfo *self,
              guint         count)
{
  GtkWidget *stack;
  AdwViewStackPage *page;

  stack = gtk_widget_get_ancestor (GTK_WIDGET (self), ADW_TYPE_VIEW_STACK);
  page = adw_view_stack_get_page (ADW_VIEW_STACK (stack), GTK_WIDGET (self));
  adw_view_stack_page_set_badge_number (ADW_VIEW_STACK_PAGE (page), count);
  adw_view_stack_page_set_visible (ADW_VIEW_STACK_PAGE (page), count > 0);
}

static void
add_word_list_to_array (GArray   *word_arr,
                        WordList *word_list)
{
  guint len = word_arr->len;

  g_array_set_size (word_arr, word_list_get_n_items (word_list) + len);

  for (guint i = 0; i < word_list_get_n_items (word_list); i++)
    {
      const gchar **ptr;

      ptr = &(g_array_index (word_arr, const gchar *, i + len));
      *ptr = word_list_get_word (word_list, i);
    }
}

static gint
word_sort_func (const gchar **a,
                const gchar **b)
{
  return g_strcmp0 (*a, *b);
}

static void
update_section (EditClueInfo        *self,
                EditClueInfoSection *section,
                GArray              *word_arr,
                GArray              *attr_arr)
{
  gtk_widget_set_visible (GTK_WIDGET (section), word_arr->len > 0);
  g_array_sort (word_arr, (GCompareFunc) word_sort_func);
  edit_clue_info_section_update (section,
                                 word_list_get_resource (self->word_list),
                                 word_arr,
                                 attr_arr);
}

static GArray *
make_extremes_attrs (EditClueInfo *self)
{
  WordListResource *resource = word_list_get_resource (self->word_list);
  WordListIndex *index = word_list_resource_get_index (resource);
  GArray *attr_arr;

  attr_arr = create_attr_array ();

  for (guint i = 2; i < (guint)index->max_length; i++)
    {
      PangoAttrList *attr_list;
      PangoAttribute *attr;

      attr_list = pango_attr_list_new ();
      attr = pango_attr_underline_new (PANGO_UNDERLINE_SINGLE_LINE);
      attr->start_index = 0;
      attr->end_index = 1;
      pango_attr_list_insert (attr_list, attr);

      attr = pango_attr_underline_new (PANGO_UNDERLINE_SINGLE_LINE);
      attr->start_index = i;
      attr->end_index = i+1;
      pango_attr_list_insert (attr_list, attr);
      attr_arr_set_attr (attr_arr, i + 1, attr_list);
      pango_attr_list_unref (attr_list);
    }

  return attr_arr;
}

static guint
update_extremes (EditClueInfo *self)
{
  g_autoptr (GString) str = NULL;
  g_autoptr (GArray) word_arr = NULL;
  g_autoptr (GArray) attr_arr = NULL;
  WordListIndex *index;

  /* NOTE: We could use the wordlist's help with this one in the
     future. It also only works with two letters. */
  if (g_utf8_strlen (self->selection_text, -1) != 2)
    return 0;

  /* Empty: W.*D */
  word_arr = g_array_new (FALSE, FALSE, sizeof (const gchar *));
  attr_arr = make_extremes_attrs (self);

  index = word_list_resource_get_index (word_list_get_resource (self->word_list));
  str = g_string_new (self->selection_text);

  for (guint i = 2; i < (guint)index->max_length; i ++)
    {
      g_string_insert_c (str, 1, '?');
      word_list_set_filter (self->word_list, str->str,
                            WORD_LIST_MATCH);
      add_word_list_to_array (word_arr, self->word_list);
    }

  update_section (self, EDIT_CLUE_INFO_SECTION (self->section1), word_arr, attr_arr);

  return word_arr->len;
}


static GArray *
make_tailless_attrs (guint len)
{
  GArray *attr_arr;
  PangoAttrList *attr_list;
  PangoAttribute *attr;

  attr_arr = create_attr_array ();

  attr_list = pango_attr_list_new ();
  attr = pango_attr_underline_new (PANGO_UNDERLINE_SINGLE_LINE);
  attr->start_index = 0;
  attr->end_index = len;
  pango_attr_list_insert (attr_list, attr);

  attr_arr_set_attr (attr_arr, len + 1, attr_list);
  pango_attr_list_unref (attr_list);

  return attr_arr;
}

/* also headless and endless share the same attr list */
static GArray *
make_headless_endless_attrs (guint len)
{
  GArray *attr_arr;
  PangoAttrList *attr_list;
  PangoAttribute *attr;

  attr_arr = create_attr_array ();

  attr_list = pango_attr_list_new ();
  attr = pango_attr_underline_new (PANGO_UNDERLINE_SINGLE_LINE);
  attr->start_index = 1;
  attr->end_index = len + 1;
  pango_attr_list_insert (attr_list, attr);

  attr_arr_set_attr (attr_arr, len + 1, attr_list);
  attr_arr_set_attr (attr_arr, len + 2, attr_list);
  pango_attr_list_unref (attr_list);

  return attr_arr;
}

static guint
update_headless_endless_deletions (EditClueInfo *self)
{
  g_autoptr (GString) str = NULL;
  g_autoptr (GArray) word_arr = NULL;
  g_autoptr (GArray) attr_arr = NULL;
  guint count;

  word_arr = g_array_new (FALSE, FALSE, sizeof (const gchar *));
  attr_arr = make_headless_endless_attrs (g_utf8_strlen (self->selection_text, -1));

  /* Endless: "?WORD?" */
  str = g_string_new (self->selection_text);
  g_string_append (str, "?");
  g_string_prepend (str, "?");
  word_list_set_filter (self->word_list, str->str,
                        WORD_LIST_MATCH);
  add_word_list_to_array (word_arr, self->word_list);

  count = word_arr->len;
  update_section (self, EDIT_CLUE_INFO_SECTION (self->section3), word_arr, attr_arr);

  /* Headless: "?WORD" */
  g_array_set_size (word_arr, 0);
  g_string_truncate (str, strlen (str->str) - 1);
  word_list_set_filter (self->word_list, str->str,
                        WORD_LIST_MATCH);
  add_word_list_to_array (word_arr, self->word_list);

  count += word_arr->len;
  update_section (self, EDIT_CLUE_INFO_SECTION (self->section1), word_arr, attr_arr);

  return count;
}

static guint
update_tailless_deletions (EditClueInfo *self)
{
  g_autoptr (GString) str = NULL;
  g_autoptr (GArray) word_arr = NULL;
  g_autoptr (GArray) attr_arr = NULL;

  word_arr = g_array_new (FALSE, FALSE, sizeof (const gchar *));
  attr_arr = make_tailless_attrs (g_utf8_strlen (self->selection_text, -1));

  /* Tailless: "WORD?" */
  str = g_string_new (self->selection_text);
  g_string_append (str, "?");
  word_list_set_filter (self->word_list, str->str,
                        WORD_LIST_MATCH);
  add_word_list_to_array (word_arr, self->word_list);

  update_section (self, EDIT_CLUE_INFO_SECTION (self->section2), word_arr, attr_arr);

  return word_arr->len;
}
static guint
update_deletions (EditClueInfo *self)
{
  guint count = 0;

  count += update_headless_endless_deletions (self);
  count += update_tailless_deletions (self);

  return count;
}

static GArray *
make_odd_attrs (guint len)
{
  GArray *attr_arr;
  PangoAttrList *attr_list;

  attr_arr = create_attr_array ();

  attr_list = pango_attr_list_new ();
  for (guint i = 0; i < len; i++)
    {
      PangoAttribute *attr;

      attr = pango_attr_underline_new (PANGO_UNDERLINE_SINGLE);
      attr->start_index = i * 2;
      attr->end_index = attr->start_index + 1;
      pango_attr_list_insert (attr_list, attr);
    }

  attr_arr_set_attr (attr_arr, 2 * len - 1, attr_list);
  attr_arr_set_attr (attr_arr, 2 * len, attr_list);
  pango_attr_list_unref (attr_list);

  return attr_arr;
}

static GArray *
make_even_attrs (guint len)
{
  GArray *attr_arr;
  PangoAttrList *attr_list;

  attr_arr = create_attr_array ();

  attr_list = pango_attr_list_new ();
  for (guint i = 0; i < len; i++)
    {
      PangoAttribute *attr;

      attr = pango_attr_underline_new (PANGO_UNDERLINE_SINGLE);
      attr->start_index = i * 2 + 1;
      attr->end_index = attr->start_index + 1;

      pango_attr_list_insert (attr_list, attr);
    }

  attr_arr_set_attr (attr_arr, 2 * len, attr_list);
  attr_arr_set_attr (attr_arr, 2 * len + 1, attr_list);
  pango_attr_list_unref (attr_list);

  return attr_arr;
}

static guint
update_alternations (EditClueInfo *self)
{
  g_autoptr (GString) str = NULL;
  g_autoptr (GArray) word_arr = NULL;
  g_autoptr (GArray) odd_attr_arr = NULL;
  g_autoptr (GArray) even_attr_arr = NULL;
  guint count;

  word_arr = g_array_new (FALSE, FALSE, sizeof (const gchar *));
  odd_attr_arr = make_odd_attrs (g_utf8_strlen (self->selection_text, -1));
  even_attr_arr = make_even_attrs (g_utf8_strlen (self->selection_text, -1));

  /* Odds: "W?O?R?D?" */
  str = g_string_new (NULL);
  for (const gchar *p = self->selection_text; p[0]; p = g_utf8_next_char (p))
    {
      g_string_append_len (str, p, (g_utf8_next_char (p)-p));
      g_string_append (str, "?");
    }
  word_list_set_filter (self->word_list, str->str,
                        WORD_LIST_MATCH);
  add_word_list_to_array (word_arr, self->word_list);

  /* Odds: "W?O?R?D" */
  g_string_truncate (str, strlen (str->str) - 1);
  word_list_set_filter (self->word_list, str->str,
                        WORD_LIST_MATCH);
  add_word_list_to_array (word_arr, self->word_list);

  count = word_arr->len;
  update_section (self, EDIT_CLUE_INFO_SECTION (self->section1), word_arr, odd_attr_arr);

  /* Evens: "?W?O?R?D" */
  g_array_set_size (word_arr, 0);
  g_string_prepend_c (str, '?');
  word_list_set_filter (self->word_list, str->str,
                        WORD_LIST_MATCH);
  add_word_list_to_array (word_arr, self->word_list);

  /* Evens: "?W?O?R?D?" */
  g_string_append_c (str, '?');
  word_list_set_filter (self->word_list, str->str,
                        WORD_LIST_MATCH);
  add_word_list_to_array (word_arr, self->word_list);

  count += word_arr->len;
  update_section (self, EDIT_CLUE_INFO_SECTION (self->section2), word_arr, even_attr_arr);

  return count;
}

static guint
update_anagram (EditClueInfo *self)
{
  guint count = 0;
  g_autoptr (GArray) word_arr = NULL;
  g_autofree gchar *double_word = NULL;

  word_arr = g_array_new (FALSE, FALSE, sizeof (const gchar *));

  /* Find all the anagrams */
  word_list_set_filter (self->word_list,
                        self->selection_text,
                        WORD_LIST_ANAGRAM);

  add_word_list_to_array (word_arr, self->word_list);
  update_section (self, EDIT_CLUE_INFO_SECTION (self->section1), word_arr, NULL);
  count += word_arr->len;

  /* Cycles are special cases of anagrams.
   * To see a word is a cycle, we can just do strstr on the word twice.
   * eg. EATS is in SEATSEAT */
  g_array_set_size (word_arr, 0);
  double_word = g_strdup_printf ("%s%s", self->selection_text, self->selection_text);

  for (guint i = 0; i < word_list_get_n_items (self->word_list); i++)
    {
      const gchar *word = word_list_get_word (self->word_list, i);
      if (g_strcmp0 (self->selection_text, word) == 0)
        continue;

      if (g_strstr_len (double_word, -1, word))
        g_array_append_val (word_arr, word);
    }

  update_section (self, EDIT_CLUE_INFO_SECTION (self->section2), word_arr, NULL);
  count += word_arr->len;

  return count;
}

void
edit_clue_info_update (EditClueInfo     *self,
                       WordListResource *resource,
                       const gchar      *selection_text)
{
  GtkAdjustment *vadj;
  WordListResource *old_resource;
  guint count = 0;
  g_assert (selection_text);

  old_resource = word_list_get_resource (self->word_list);

  /* Short circuit same selection_text and resource */
  if (resource == old_resource &&
      g_strcmp0 (self->selection_text, selection_text) == 0)
    return;

  /* reset the swindow */
  vadj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (self->swindow));
  gtk_adjustment_set_value (GTK_ADJUSTMENT (vadj), 0.0);
  
  word_list_set_resource (self->word_list, resource);
  g_clear_pointer (&self->selection_text, g_free);
  self->selection_text = g_strdup (selection_text);

  gtk_widget_set_visible (self->section1, FALSE);
  gtk_widget_set_visible (self->section2, FALSE);
  gtk_widget_set_visible (self->section3, FALSE);

  if (g_utf8_strlen (selection_text, strlen (selection_text)) < 2)
    goto out;

  /* filter out partial clues. We don't support those yet */
  for (const gchar *p = selection_text;
       p[0];
       p = g_utf8_next_char (p))
    {
      if (p[0] == '?')
        goto out;
    }

  switch (self->mode)
    {
    case EDIT_CLUE_INFO_MODE_ANAGRAM:
      count = update_anagram (self);
      break;
    case EDIT_CLUE_INFO_MODE_ALTERNATIONS:
      count = update_alternations (self);
      break;
    case EDIT_CLUE_INFO_MODE_DELETIONS:
      count = update_deletions (self);
      break;
    case EDIT_CLUE_INFO_MODE_EXTREMES:
      count = update_extremes (self);
      break;
    default:
      g_assert_not_reached ();
    }

 out:
  update_count (self, count);
}
