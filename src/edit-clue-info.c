/* edit-clue-info.c
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include "crosswords-enums.h"
#include <libipuz/libipuz.h>
#include <glib/gi18n-lib.h>
#include <adwaita.h>
#include "edit-clue-info.h"
#include "word-list-model.h"


enum
{
  PROP_0,
  PROP_MODE,
  N_PROPS,
};

static GParamSpec *obj_props[N_PROPS] =  {NULL, };


struct _EditClueInfo
{
  GtkWidget parent_instance;
  GtkWidget *flow_box;
  WordListModel *word_list_model;

  GtkWidget *flow_box2;
  WordListModel *word_list_model2;
  EditClueInfoMode mode;
};


G_DEFINE_FINAL_TYPE (EditClueInfo, edit_clue_info, GTK_TYPE_WIDGET);


static void edit_clue_info_init         (EditClueInfo      *self);
static void edit_clue_info_class_init   (EditClueInfoClass *klass);
static void edit_clue_info_set_property (GObject           *object,
                                         guint              prop_id,
                                         const GValue      *value,
                                         GParamSpec        *pspec);
static void edit_clue_info_get_property (GObject           *object,
                                         guint              prop_id,
                                         GValue            *value,
                                         GParamSpec        *pspec);
static void  edit_clue_info_dispose     (GObject           *object);



static void
row_changed_cb (GtkWidget        *label,
                WordListModelRow *row)
{
  gtk_label_set_text (GTK_LABEL (label),
                      word_list_model_row_get_word (row));
}

GtkWidget *
create_widget_func (GObject  *item,
                    gpointer  user_data)
{
  GtkWidget *label;
  const gchar *word;

  word = word_list_model_row_get_word (WORD_LIST_MODEL_ROW (item));
  label = gtk_label_new (word);
  gtk_widget_add_css_class (label, "word-result");
  gtk_widget_add_css_class (label, "tag");
  g_signal_connect_swapped (item, "changed", G_CALLBACK (row_changed_cb), label);

  return label;
}

static void
edit_clue_info_init (EditClueInfo *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
  gtk_flow_box_bind_model (GTK_FLOW_BOX (self->flow_box),
                           G_LIST_MODEL (self->word_list_model),
                           (GtkFlowBoxCreateWidgetFunc) create_widget_func,
                           NULL, NULL);
  gtk_flow_box_bind_model (GTK_FLOW_BOX (self->flow_box2),
                           G_LIST_MODEL (self->word_list_model2),
                           (GtkFlowBoxCreateWidgetFunc) create_widget_func,
                           NULL, NULL);
}

static void
edit_clue_info_class_init (EditClueInfoClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->set_property = edit_clue_info_set_property;
  object_class->get_property = edit_clue_info_get_property;
  object_class->dispose = edit_clue_info_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-clue-info.ui");

  gtk_widget_class_bind_template_child (widget_class, EditClueInfo, flow_box);
  gtk_widget_class_bind_template_child (widget_class, EditClueInfo, word_list_model);
  gtk_widget_class_bind_template_child (widget_class, EditClueInfo, flow_box2);
  gtk_widget_class_bind_template_child (widget_class, EditClueInfo, word_list_model2);

  obj_props[PROP_MODE] = g_param_spec_enum ("mode", NULL, NULL,
                                            EDIT_TYPE_CLUE_INFO_MODE,
                                            EDIT_CLUE_INFO_MODE_ANAGRAM,
                                            G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  gtk_widget_class_set_css_name (widget_class, "clueinfo");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BOX_LAYOUT);
}

static void
edit_clue_info_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  EditClueInfo *self;

  self = EDIT_CLUE_INFO (object);

  switch (prop_id)
    {
    case PROP_MODE:
      self->mode = g_value_get_enum (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
edit_clue_info_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  EditClueInfo *self;

  self = EDIT_CLUE_INFO (object);

  switch (prop_id)
    {
    case PROP_MODE:
      g_value_set_enum (value, self->mode);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
edit_clue_info_dispose (GObject *object)
{
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  G_OBJECT_CLASS (edit_clue_info_parent_class)->dispose (object);
}

/* Public methods */


static gchar *
make_filter_odd (const gchar *selection_text,
                 guint        variant)
{
  GString *str;

  str = g_string_new (NULL);
  for (const gchar *p = selection_text; p[0]; p = g_utf8_next_char (p))
    {
      g_string_append_len (str, p, (g_utf8_next_char (p)-p));
      g_string_append (str, "?");
    }

  if (variant == 1)
    g_string_truncate (str, strlen (str->str) - 1);

  return g_string_free_and_steal (str);
}

static gchar *
make_filter_even (const gchar *selection_text,
                  guint        variant)
{
  GString *str;

  str = g_string_new (NULL);
  for (const gchar *p = selection_text; p[0]; p = g_utf8_next_char (p))
    {
      g_string_append (str, "?");
      g_string_append_len (str, p, (g_utf8_next_char (p)-p));
    }

  if (variant == 1)
    g_string_append (str, "?");

  return g_string_free_and_steal (str);
}

static gchar *
make_filter (const gchar      *selection_text,
             EditClueInfoMode  mode,
             guint             variant)
{
  if (mode == EDIT_CLUE_INFO_MODE_ODD)
    return make_filter_odd (selection_text, variant);
  else if (mode == EDIT_CLUE_INFO_MODE_EVEN)
    return make_filter_even (selection_text, variant);

  return NULL;
}

static void
update_count (EditClueInfo *self)
{
  GtkWidget *stack;
  AdwViewStackPage *page;
  guint count;

  count = g_list_model_get_n_items (G_LIST_MODEL (self->word_list_model));
  count += g_list_model_get_n_items (G_LIST_MODEL (self->word_list_model2));

  stack = gtk_widget_get_ancestor (GTK_WIDGET (self), ADW_TYPE_VIEW_STACK);
  page = adw_view_stack_get_page (ADW_VIEW_STACK (stack), GTK_WIDGET (self));
  adw_view_stack_page_set_badge_number (ADW_VIEW_STACK_PAGE (page), count);
}

void
edit_clue_info_update (EditClueInfo     *self,
                       WordListResource *resource,
                       const gchar      *selection_text)
{
  g_assert (selection_text);

  word_list_model_set_resource (WORD_LIST_MODEL (self->word_list_model), resource);
  word_list_model_set_resource (WORD_LIST_MODEL (self->word_list_model2), resource);

  if (g_utf8_strlen (selection_text, strlen (selection_text)) < 2)
    {
      word_list_model_set_filter (WORD_LIST_MODEL (self->word_list_model), "", WORD_LIST_MATCH);
      word_list_model_set_filter (WORD_LIST_MODEL (self->word_list_model2), "", WORD_LIST_MATCH);
      goto out;
    }

  /* filter out partial clues. We don't support those yet */
  for (const gchar *p = selection_text;
       p[0];
       p = g_utf8_next_char (p))
    {
      if (p[0] == '?')
        {
          word_list_model_set_filter (WORD_LIST_MODEL (self->word_list_model),
                                      "", WORD_LIST_MATCH);
          word_list_model_set_filter (WORD_LIST_MODEL (self->word_list_model2),
                                      "", WORD_LIST_MATCH);
          goto out;
        }
    }

  /* Update the filter */
  if (self->mode == EDIT_CLUE_INFO_MODE_ANAGRAM)
    {
      word_list_model_set_filter (WORD_LIST_MODEL (self->word_list_model),
                                  selection_text,
                                  WORD_LIST_ANAGRAM);
      word_list_model_set_filter (WORD_LIST_MODEL (self->word_list_model2),
                                  "", WORD_LIST_MATCH);

    }
  else
    {
      g_autofree gchar *filter = NULL;
      g_autofree gchar *filter2 = NULL;

      filter = make_filter (selection_text, self->mode, 0);
      filter2 = make_filter (selection_text, self->mode, 1);

      word_list_model_set_filter (WORD_LIST_MODEL (self->word_list_model),
                                  filter, WORD_LIST_MATCH);
      word_list_model_set_filter (WORD_LIST_MODEL (self->word_list_model2),
                                  filter2, WORD_LIST_MATCH);
    }

 out:
  update_count (self);
}
