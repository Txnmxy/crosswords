/* edit-clue-info.h
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>
#include "word-list-resource.h"

G_BEGIN_DECLS


typedef enum
{
  EDIT_CLUE_INFO_MODE_ANAGRAM,
  EDIT_CLUE_INFO_MODE_ODD,
  EDIT_CLUE_INFO_MODE_EVEN,
  EDIT_CLUE_INFO_MODE_TAILLESS,
  EDIT_CLUE_INFO_MODE_HEADLESS,
} EditClueInfoMode;


#define EDIT_TYPE_CLUE_INFO (edit_clue_info_get_type())
G_DECLARE_FINAL_TYPE (EditClueInfo, edit_clue_info, EDIT, CLUE_INFO, GtkWidget);


void edit_clue_info_update (EditClueInfo     *self,
                            WordListResource *resource,
                            const gchar      *selection_text);


G_END_DECLS
