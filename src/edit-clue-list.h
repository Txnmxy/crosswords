/* edit-clue-list.h
 *
 * Copyright 2023 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>
#include "grid-state.h"


G_BEGIN_DECLS


#define EDIT_TYPE_CLUE_LIST (edit_clue_list_get_type())
G_DECLARE_FINAL_TYPE (EditClueList, edit_clue_list, EDIT, CLUE_LIST, GtkWidget);


void edit_clue_list_update (EditClueList *self,
                            GridState    *clues_state);


G_END_DECLS
