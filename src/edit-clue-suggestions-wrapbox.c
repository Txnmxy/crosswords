/* edit-clue-suggestions.c
 *
 * Copyright 2024 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
/* Filter editing widgetry is adapted from nautilus-file-chooser.{c,ui}
 */
#include "crosswords-config.h"
#include "crosswords-enums.h"
#include "crosswords-misc.h"
#include <libipuz/libipuz.h>
#include <glib/gi18n-lib.h>
#include <adwaita.h>
#include "edit-clue-definitions.h"
#include "edit-clue-info-wrapbox.h"
#include "edit-clue-suggestions.h"
#include "word-list.h"


enum
{
  FILTER_CHANGED,
  N_SIGNALS
};

static guint obj_signals[N_SIGNALS] = { 0 };


struct _EditClueSuggestions
{
  GtkWidget parent_instance;

  GtkWidget *filter_stack;
  GtkWidget *filter_button_container;
  GtkWidget *filter_button_start;
  GtkWidget *filter_label;
  GtkWidget *filter_button_end;
  GtkWidget *filter_entry;

  GtkWidget *info_clues_stack;
  GtkWidget *clue_definitions;
  GtkWidget *clue_info_anagram;
  GtkWidget *clue_info_alternations;
  GtkWidget *clue_info_deletions;
  GtkWidget *clue_info_extremes;

  WordListResource *resource;
  gchar *current_filter;
  IpuzCharset *filter_charset;
  IpuzCharset *source_charset;
};


G_DEFINE_FINAL_TYPE (EditClueSuggestions, edit_clue_suggestions, GTK_TYPE_WIDGET);


static void edit_clue_suggestions_init       (EditClueSuggestions      *self);
static void edit_clue_suggestions_class_init (EditClueSuggestionsClass *klass);
static void edit_clue_suggestions_dispose    (GObject                  *object);
static void filter_button_clicked_cb         (EditClueSuggestions      *self);
static void filter_flip_icon_pressed_cb      (EditClueSuggestions      *self);
static void filter_entry_focus_leave_cb      (EditClueSuggestions      *self);
static void filter_entry_insert_text_cb      (GtkEditable              *editable,
                                              const char               *text,
                                              int                       length,
                                              int                      *position,
                                              EditClueSuggestions      *self);
static void filter_entry_activate_cb         (EditClueSuggestions      *self);
static void word_clicked_cb                  (EditClueSuggestions      *self,
                                              const gchar              *word);

static void
edit_clue_suggestions_init (EditClueSuggestions *self)
{
  GtkEditable *delegate;

  gtk_widget_init_template (GTK_WIDGET (self));

  delegate = gtk_editable_get_delegate (GTK_EDITABLE (self->filter_entry));
  g_signal_connect (delegate,
                    "insert-text",
                    G_CALLBACK (filter_entry_insert_text_cb),
                    self);

  gtk_stack_set_visible_child (GTK_STACK (self->filter_stack),
                               self->filter_button_container);
}

static void
edit_clue_suggestions_class_init (EditClueSuggestionsClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = edit_clue_suggestions_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-clue-suggestions-wrapbox.ui");

  gtk_widget_class_bind_template_child (widget_class, EditClueSuggestions, filter_stack);
  gtk_widget_class_bind_template_child (widget_class, EditClueSuggestions, filter_button_container);
  gtk_widget_class_bind_template_child (widget_class, EditClueSuggestions, filter_button_start);
  gtk_widget_class_bind_template_child (widget_class, EditClueSuggestions, filter_label);
  gtk_widget_class_bind_template_child (widget_class, EditClueSuggestions, filter_button_end);
  gtk_widget_class_bind_template_child (widget_class, EditClueSuggestions, filter_entry);
  gtk_widget_class_bind_template_child (widget_class, EditClueSuggestions, info_clues_stack);
  gtk_widget_class_bind_template_child (widget_class, EditClueSuggestions, clue_definitions);
  gtk_widget_class_bind_template_child (widget_class, EditClueSuggestions, clue_info_anagram);
  gtk_widget_class_bind_template_child (widget_class, EditClueSuggestions, clue_info_alternations);
  gtk_widget_class_bind_template_child (widget_class, EditClueSuggestions, clue_info_deletions);
  gtk_widget_class_bind_template_child (widget_class, EditClueSuggestions, clue_info_extremes);
  gtk_widget_class_bind_template_callback (widget_class, filter_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, filter_flip_icon_pressed_cb);
  gtk_widget_class_bind_template_callback (widget_class, filter_entry_focus_leave_cb);
  gtk_widget_class_bind_template_callback (widget_class, filter_entry_activate_cb);
  gtk_widget_class_bind_template_callback (widget_class, word_clicked_cb);

  obj_signals [FILTER_CHANGED] =
    g_signal_new ("filter-changed",
                  EDIT_TYPE_CLUE_SUGGESTIONS,
                  G_SIGNAL_RUN_FIRST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_STRING);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
}

static void
edit_clue_suggestions_dispose (GObject *object)
{
  EditClueSuggestions *self = EDIT_CLUE_SUGGESTIONS (object);
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_pointer (&self->current_filter, g_free);
  g_clear_pointer (&self->source_charset, ipuz_charset_unref);
  g_clear_pointer (&self->filter_charset, ipuz_charset_unref);
  g_clear_object (&self->resource);

  G_OBJECT_CLASS (edit_clue_suggestions_parent_class)->dispose (object);
}

static void
filter_button_clicked_cb (EditClueSuggestions *self)
{
  gtk_stack_set_visible_child (GTK_STACK (self->filter_stack),
                               self->filter_entry);
  gtk_entry_grab_focus_without_selecting (GTK_ENTRY (self->filter_entry));
}

static void
filter_flip_icon_pressed_cb (EditClueSuggestions *self)
{
  const gchar *filter;
  g_autofree gchar *retlif = NULL;

  filter = gtk_editable_get_text (GTK_EDITABLE (self->filter_entry));
  retlif = g_utf8_strreverse (filter, -1);

  /* We have different behavriors for C-f depending on whether we're
     mid-edit or not. If we are, we just flip the text in the entry
     and don't update. If we're not, we commit the flipped text.*/
  if (gtk_stack_get_visible_child (GTK_STACK (self->filter_stack)) ==
      self->filter_button_container)
    {
      g_signal_emit (self, obj_signals [FILTER_CHANGED], 0, retlif);
    }
  else
    {
      gtk_editable_set_text (GTK_EDITABLE (self->filter_entry), retlif);
    }
}

static void
filter_entry_focus_leave_cb (EditClueSuggestions *self)
{
  /* If we lose focus because the window itself lost focus, then the
   * filter entry should persist, because this may happen due to the
   * user switching keyboard layout/input method; or they may want to
   * copy/drop an text from another window/app. We detect this case by
   * looking at the focus widget of the window (GtkRoot).
   */

  GtkWidget *focus_widget;

  focus_widget = gtk_root_get_focus (gtk_widget_get_root (GTK_WIDGET (self)));

  if (focus_widget != NULL &&
      gtk_widget_is_ancestor (focus_widget, GTK_WIDGET (self->filter_entry)))
    return;

  /* force an activate */
  filter_entry_activate_cb (self);
  gtk_stack_set_visible_child (GTK_STACK (self->filter_stack),
                               self->filter_button_container);
}

static void
filter_entry_insert_text_cb (GtkEditable         *editable,
                             const char          *text,
                             int                  length,
                             int                 *position,
                             EditClueSuggestions *self)
{
  g_autofree char *upper = g_utf8_strup (text, length);

  g_signal_handlers_block_by_func (editable,
                                   (gpointer) filter_entry_insert_text_cb, self);
  if (ipuz_charset_check_text (self->filter_charset, upper))
    gtk_editable_insert_text (editable,
                              upper, length, position);

  g_signal_handlers_unblock_by_func (editable,
                                     (gpointer) filter_entry_insert_text_cb, self);

  g_signal_stop_emission_by_name (editable, "insert_text");
}

static void
filter_entry_activate_cb (EditClueSuggestions *self)
{
  const gchar *text;

  text = gtk_editable_get_text (GTK_EDITABLE (self->filter_entry));

  if (g_strcmp0 (text, self->current_filter) != 0)
    g_signal_emit (self, obj_signals [FILTER_CHANGED], 0, text);
}

static void
word_clicked_cb (EditClueSuggestions *self,
                 const gchar         *word)
{
  /* we switch to the definitions page as a convenience */
  adw_view_stack_set_visible_child (ADW_VIEW_STACK (self->info_clues_stack),
                                    self->clue_definitions);
  if (g_strcmp0 (word, self->current_filter) != 0)
    g_signal_emit (self, obj_signals [FILTER_CHANGED], 0, word);
}


/* Public methods */


static IpuzCharset *
create_filter_charset (IpuzCharset *source_charset)
{
  g_autofree gchar *alphabet = NULL;
  IpuzCharsetBuilder *builder;

  alphabet = ipuz_charset_serialize (source_charset);
  builder = ipuz_charset_builder_new_from_text (alphabet);
  ipuz_charset_builder_add_text (builder, "?");

  return ipuz_charset_builder_build (builder);
}

void
edit_clue_suggestions_update (EditClueSuggestions *self,
                              WordListResource    *resource,
                              IpuzCharset         *charset,
                              const gchar         *new_filter)
{
  /* values are only meaningful with a filter */
  if (new_filter == NULL)
    return;

  /* short circuit any non-changes */
  if ((g_strcmp0 (self->current_filter, new_filter) == 0) &&
      (self->resource == resource) &&
      (self->source_charset == charset))
    return;

  /* update our local cache of values. First the resource */
  g_object_ref (resource);
  g_clear_object (&self->resource);
  self->resource = resource;

  /* filter charset */
  if (self->source_charset != charset)
    {
      g_clear_pointer (&self->source_charset, ipuz_charset_unref);
      g_clear_pointer (&self->filter_charset, ipuz_charset_unref);

      self->source_charset = ipuz_charset_ref (charset);
      self->filter_charset = create_filter_charset (charset);
    }

  /* current filter */
  g_clear_pointer (&self->current_filter, g_free);
  self->current_filter = g_strdup (new_filter);

  /* Set up the filter widgetry */
  gtk_label_set_text (GTK_LABEL (self->filter_label), new_filter);
  gtk_editable_set_text (GTK_EDITABLE (self->filter_entry), new_filter);

  /* Push the filter to our children */
  edit_clue_definitions_update (EDIT_CLUE_DEFINITIONS (self->clue_definitions),
                                resource,
                                self->current_filter);
  edit_clue_info_update (EDIT_CLUE_INFO (self->clue_info_anagram),
                         resource,
                         self->current_filter);
  edit_clue_info_update (EDIT_CLUE_INFO (self->clue_info_alternations),
                         resource,
                         self->current_filter);
  edit_clue_info_update (EDIT_CLUE_INFO (self->clue_info_deletions),
                         resource,
                         self->current_filter);
  edit_clue_info_update (EDIT_CLUE_INFO (self->clue_info_extremes),
                         resource,
                         self->current_filter);
}

void
edit_clue_suggestions_flip (EditClueSuggestions *self)
{
  g_return_if_fail (EDIT_IS_CLUE_SUGGESTIONS (self));

  filter_flip_icon_pressed_cb (self);
}
