/* edit-grid.c
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <libipuz/libipuz.h>
#include <glib/gi18n-lib.h>
#include "crosswords-enums.h"
#include "edit-grid.h"
#include "edit-symmetry.h"
#include "play-grid.h"


enum
{
  PROP_0,
  PROP_STAGE,
  N_PROPS
};

enum
{
  GUESS,
  GUESS_AT_CELL,
  DO_COMMAND,
  CELL_SELECTED,
  SELECT_DRAG_START,
  SELECT_DRAG_UPDATE,
  SELECT_DRAG_END,
  COPY,
  PASTE,
  N_SIGNALS
};

static GParamSpec *obj_props[N_PROPS] = {NULL, };
static guint obj_signals[N_SIGNALS] = { 0 };


struct _EditGrid
{
  GtkWidget parent_instance;

  PuzzleStackStage stage;
  gchar *data_hint;

  /* Template widgets */
  GtkWidget *grid;
};


static void edit_grid_init                     (EditGrid          *self);
static void edit_grid_class_init               (EditGridClass     *klass);
static void edit_grid_set_property             (GObject           *object,
                                                guint              prop_id,
                                                const GValue      *value,
                                                GParamSpec        *pspec);
static void edit_grid_get_property             (GObject           *object,
                                                guint              prop_id,
                                                GValue            *value,
                                                GParamSpec        *pspec);
static void edit_grid_dispose                  (GObject           *object);
static void edit_grid_activate_clipboard_copy  (EditGrid          *self,
                                                const char        *name,
                                                GVariant          *parameter);
static void edit_grid_activate_clipboard_paste (EditGrid          *self,
                                                const char        *name,
                                                GVariant          *parameter);
static void grid_guess_cb                      (PlayGrid          *grid,
                                                gchar             *guess,
                                                EditGrid          *self);
static void grid_guess_at_cell_cb              (PlayGrid          *grid,
                                                gchar             *guess,
                                                guint              row,
                                                guint              column,
                                                EditGrid          *self);
static void grid_do_command_cb                 (PlayGrid          *grid,
                                                GridCmdKind        kind,
                                                GridSelectionMode  mode,
                                                EditGrid          *xword);
static void grid_cell_selected_cb              (PlayGrid          *grid,
                                                guint              row,
                                                guint              column,
                                                EditGrid          *self);
static void grid_select_drag_start_cb          (EditGrid          *self,
                                                IpuzCellCoord     *anchor_coord,
                                                IpuzCellCoord     *new_coord,
                                                GridSelectionMode  mode);
static void grid_select_drag_update_cb         (EditGrid          *self,
                                                IpuzCellCoord     *anchor_coord,
                                                IpuzCellCoord     *new_coord,
                                                GridSelectionMode  mode);
static void grid_select_drag_end_cb            (EditGrid          *self,
                                                IpuzCellCoord     *anchor_coord);


G_DEFINE_TYPE (EditGrid, edit_grid, GTK_TYPE_WIDGET);


/* keep a unique id for every instance of EditGrid so it can find its
 * own puzzle-stack data. */
static guint data_hint_id = 0;

static void
edit_grid_init (EditGrid *self)
{
  self->stage = EDIT_STAGE_GRID;
  gtk_widget_init_template (GTK_WIDGET (self));
  self->data_hint = g_strdup_printf ("edit-grid-%u", data_hint_id++);
}

static void
edit_grid_class_init (EditGridClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->set_property = edit_grid_set_property;
  object_class->get_property = edit_grid_get_property;
  object_class->dispose = edit_grid_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-grid.ui");

  gtk_widget_class_bind_template_callback (widget_class, grid_guess_cb);
  gtk_widget_class_bind_template_callback (widget_class, grid_guess_at_cell_cb);
  gtk_widget_class_bind_template_callback (widget_class, grid_do_command_cb);
  gtk_widget_class_bind_template_callback (widget_class, grid_cell_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, grid_select_drag_start_cb);
  gtk_widget_class_bind_template_callback (widget_class, grid_select_drag_update_cb);
  gtk_widget_class_bind_template_callback (widget_class, grid_select_drag_end_cb);

  gtk_widget_class_bind_template_child (widget_class, EditGrid, grid);

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_c, GDK_CONTROL_MASK, "clipboard.copy", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_v, GDK_CONTROL_MASK, "clipboard.paste", NULL);

  gtk_widget_class_install_action (widget_class, "clipboard.copy", NULL,
                                   (GtkWidgetActionActivateFunc) edit_grid_activate_clipboard_copy);
  gtk_widget_class_install_action (widget_class, "clipboard.paste", NULL,
                                   (GtkWidgetActionActivateFunc) edit_grid_activate_clipboard_paste);

  gtk_widget_class_set_css_name (widget_class, "edit-grid");

  /* Proxy signals from PlayGrid */
  obj_signals [GUESS] =
    g_signal_new ("guess",
                  EDIT_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_STRING);

  obj_signals [GUESS_AT_CELL] =
    g_signal_new ("guess-at-cell",
                  EDIT_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 2,
                  G_TYPE_STRING,
                  IPUZ_TYPE_CELL_COORD);

  obj_signals [DO_COMMAND] =
    g_signal_new ("do-command",
                  EDIT_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 2,
                  GRID_TYPE_CMD_KIND,
                  GRID_TYPE_SELECTION_MODE);

  obj_signals [CELL_SELECTED] =
    g_signal_new ("cell-selected",
		  EDIT_TYPE_GRID,
		  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
		  0,
		  NULL, NULL,
		  NULL,
		  G_TYPE_NONE, 1,
		  IPUZ_TYPE_CELL_COORD);

  obj_signals [SELECT_DRAG_START] =
    g_signal_new ("select-drag-start",
                  EDIT_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 3,
                  IPUZ_TYPE_CELL_COORD,
                  IPUZ_TYPE_CELL_COORD,
                  GRID_TYPE_SELECTION_MODE);

  obj_signals [SELECT_DRAG_UPDATE] =
    g_signal_new ("select-drag-update",
                  EDIT_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 3,
                  IPUZ_TYPE_CELL_COORD,
                  IPUZ_TYPE_CELL_COORD,
                  GRID_TYPE_SELECTION_MODE);

  obj_signals [SELECT_DRAG_END] =
    g_signal_new ("select-drag-end",
                  EDIT_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  IPUZ_TYPE_CELL_COORD);

  obj_signals [COPY] =
    g_signal_new ("copy",
                  EDIT_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  obj_signals [PASTE] =
    g_signal_new ("paste",
                  EDIT_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_STRING);

  obj_props[PROP_STAGE] = g_param_spec_enum ("stage", NULL, NULL,
                                             PUZZLE_TYPE_STACK_STAGE,
                                             EDIT_STAGE_GRID,
                                             G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
}

static void
edit_grid_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  EditGrid *self;

  self = EDIT_GRID (object);

  switch (prop_id)
    {
    case PROP_STAGE:
      edit_grid_set_stage (self, g_value_get_enum (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
edit_grid_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  EditGrid *self;

  self = EDIT_GRID (object);

  switch (prop_id)
    {
    case PROP_STAGE:
      g_value_set_enum (value, self->stage);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
edit_grid_dispose (GObject *object)
{
  EditGrid *self;
  GtkWidget *child;

  self = EDIT_GRID (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_pointer (&self->data_hint, g_free);

  G_OBJECT_CLASS (edit_grid_parent_class)->dispose (object);
}

static void
edit_grid_activate_clipboard_copy (EditGrid   *self,
                                   const char *name,
                                   GVariant   *parameter)
{
  g_signal_emit (self, obj_signals [COPY], 0);
}

static void
paste_received (GObject      *clipboard,
                GAsyncResult *result,
                gpointer      data)
{
  /* There's an outstanding ref on self, just in case the window is
   * closed while waiting for the paste to come in.*/
  g_autoptr (EditGrid) self = EDIT_GRID (data);
  g_autofree gchar *text = NULL;
  g_autofree gchar *upper = NULL;

  text = gdk_clipboard_read_text_finish (GDK_CLIPBOARD (clipboard), result, NULL);
  if (text == NULL)
    {
      /* Strange text to get. */
      gtk_widget_error_bell (GTK_WIDGET (self));
      return;
    }

  upper = g_utf8_strup (text, -1);
  g_signal_emit (self, obj_signals [PASTE], 0, upper);
}

static void
edit_grid_activate_clipboard_paste (EditGrid   *self,
                                    const char *name,
                                    GVariant   *parameter)
{
  GdkClipboard *clipboard;

  clipboard = gtk_widget_get_clipboard (GTK_WIDGET (self));
  gdk_clipboard_read_text_async (clipboard, NULL, paste_received, g_object_ref (self));
}

static void
grid_guess_cb (PlayGrid *grid,
               gchar    *guess,
               EditGrid *self)
{
  g_signal_emit (self, obj_signals [GUESS], 0, guess);
}

static void
grid_guess_at_cell_cb (PlayGrid *grid,
                       gchar    *guess,
                       guint     row,
                       guint     column,
                       EditGrid *self)
{
  IpuzCellCoord coord = {
    .row = row,
    .column = column
  };

  g_signal_emit (self, obj_signals [GUESS_AT_CELL], 0, guess, &coord);
}


static void
grid_do_command_cb (PlayGrid          *grid,
                    GridCmdKind        kind,
                    GridSelectionMode  mode,
                    EditGrid          *self)
{
  g_signal_emit (self, obj_signals [DO_COMMAND], 0, kind, mode);
}

static void
grid_cell_selected_cb (PlayGrid *grid,
                       guint     row,
                       guint     column,
                       EditGrid *self)
{
  IpuzCellCoord coord = {
    .row = row,
    .column = column,
  };

  g_signal_emit (self, obj_signals [CELL_SELECTED], 0, &coord);
}

static void
grid_select_drag_start_cb (EditGrid          *self,
                           IpuzCellCoord     *anchor_coord,
                           IpuzCellCoord     *new_coord,
                           GridSelectionMode  mode)
{
  g_signal_emit (self, obj_signals [SELECT_DRAG_START], 0, anchor_coord, new_coord, mode);
}

static void
grid_select_drag_update_cb (EditGrid          *self,
                            IpuzCellCoord     *anchor_coord,
                            IpuzCellCoord     *new_coord,
                            GridSelectionMode  mode)
{
  g_signal_emit (self, obj_signals [SELECT_DRAG_UPDATE], 0, anchor_coord, new_coord, mode);
}

static void
grid_select_drag_end_cb (EditGrid      *self,
                         IpuzCellCoord *anchor_coord)
{
  g_signal_emit (self, obj_signals [SELECT_DRAG_END], 0, anchor_coord);
}

/* Public methods */

void
edit_grid_update (EditGrid  *self,
                  GridState *state)
{
  play_grid_update_state (PLAY_GRID (self->grid),
                          state,
                          layout_config_default (IPUZ_PUZZLE_CROSSWORD));

  /* FIXME: Move the grab to the toplevel, where it can get it right */
  /* We get updated when the stack changes even when we're not being
   * shown. Grabbing focus can confuse gtk. Only set the focus when
   * we're mapped. */
  if (gtk_widget_get_mapped (GTK_WIDGET (self)) &&
      GRID_STATE_CURSOR_SET (state))
    play_grid_focus_cell (PLAY_GRID (self->grid), state->cursor);
}

void
edit_grid_unset_anchor (EditGrid *self)
{
  g_return_if_fail (EDIT_IS_GRID (self));

  play_grid_unset_anchor (PLAY_GRID (self->grid));
}

void
edit_grid_set_stage (EditGrid         *self,
                     PuzzleStackStage  stage)
{
  g_return_if_fail (EDIT_IS_GRID (self));

  if (self->stage == stage)
    return;

  self->stage = stage;

  if (self->stage == EDIT_STAGE_GRID)
    {
        
      gtk_widget_add_css_class (GTK_WIDGET (self), "stage-grid");
      gtk_widget_remove_css_class (GTK_WIDGET (self), "stage-clues");
      gtk_widget_remove_css_class (GTK_WIDGET (self), "stage-style");
    }
  else if (self->stage == EDIT_STAGE_CLUES)
    {
      gtk_widget_remove_css_class (GTK_WIDGET (self), "stage-grid");
      gtk_widget_add_css_class (GTK_WIDGET (self), "stage-clues");
      gtk_widget_remove_css_class (GTK_WIDGET (self), "stage-style");
    }
  else if (self->stage == EDIT_STAGE_STYLE)
    {
      gtk_widget_remove_css_class (GTK_WIDGET (self), "stage-grid");
      gtk_widget_remove_css_class (GTK_WIDGET (self), "stage-clues");
      gtk_widget_add_css_class (GTK_WIDGET (self), "stage-style");
    }

  
}
