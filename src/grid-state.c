/* grid-state.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "grid-state.h"
#include "crosswords-enums.h"
#include "crosswords-misc.h"
#include "crosswords-quirks.h"


static GridState    *grid_state_clone                   (GridState         *state);
static gboolean      find_first_focusable_cell          (IpuzCrossword     *xword,
                                                         IpuzCellCoord     *out_coord);
static void          set_initial_cursor                 (GridState         *state);
static void          grid_state_clue_selected_mut       (GridState         *state,
                                                         IpuzClue          *clue);
static void          grid_state_cell_selected_mut       (GridState         *state,
                                                         IpuzCellCoord      coord);
static IpuzClue     *get_next_clue                      (GridState         *state,
                                                         IpuzClueId         prev_clue,
                                                         gint               incr);
static IpuzCellCoord acrostic_get_next_clue_cell        (GridState         *state,
                                                         gint               incr);
static IpuzCellCoord acrostic_get_next_cell             (GridState         *state,
                                                         gint               incr);
static IpuzCellCoord coord_for_forward_back             (GridState         *state,
                                                         IpuzClueDirection  direction,
                                                         gint               incr);
static void          grid_state_focus_forward_back      (GridState         *state,
                                                         gint               incr);
static void          grid_state_focus_forward_back_clue (GridState         *state,
                                                         gint               incr);
static void          grid_state_focus_up_down           (GridState         *state,
                                                         gint               incr);
static void          grid_state_focus_left_right        (GridState         *state,
                                                         gint               incr);
static void          grid_state_focus_toggle            (GridState         *state);
static void          grid_state_delete                  (GridState         *state);
static void          grid_state_backspace               (GridState         *state);
static void          grid_state_solve_guess             (GridState         *state,
                                                         const gchar       *guess,
                                                         IpuzCellCoord      coord);
static void          grid_state_grid_guess              (GridState         *state,
                                                         const gchar       *guess,
                                                         IpuzCellCoord      coord);
static void          guess_advance_adjacent             (GridState         *state);
static gboolean      advance_through_clue_until_open    (GridState         *state,
                                                         IpuzClue          *clue,
                                                         gboolean           start_at_cursor,
                                                         gboolean           end_at_cursor);



static GridState *
grid_state_clone (GridState *state)
{
  GridState *c;

  if (state == NULL)
    return NULL;

  c = g_new0 (GridState, 1);
  *c = *state;
  if (state->xword)
    c->xword = g_object_ref (state->xword);
  if (state->selected_cells)
    c->selected_cells = cell_array_ref (state->selected_cells);
  if (state->quirks)
    c->quirks = g_object_ref (state->quirks);

  return c;
}

static gboolean
find_first_focusable_cell (IpuzCrossword *xword,
                           IpuzCellCoord *out_coord)
{
  out_coord->row = 0;
  out_coord->column = 0;

  guint rows = ipuz_grid_get_height (IPUZ_GRID (xword));
  guint columns = ipuz_grid_get_width (IPUZ_GRID (xword));
  guint row, column;

  for (row = 0; row < rows; row++)
    {
      for (column = 0; column < columns; column++)
        {
          IpuzCellCoord coord = {
            .row = row,
            .column = column,
          };
          IpuzCell *cell = ipuz_grid_get_cell (IPUZ_GRID (xword), &coord);

          if (IPUZ_CELL_IS_NORMAL (cell))
            {
              *out_coord = coord;
              return TRUE;
            }
        }
    }

  return FALSE;
}

static void
set_initial_cursor (GridState *state)
{
  IpuzClueId clue_id;
  clue_id.direction = IPUZ_CLUE_DIRECTION_NONE;
  clue_id.index = 0;

  if (! GRID_STATE_HAS_USE_CURSOR (state))
    return;

  for (guint i = 0; i < ipuz_clues_get_n_clue_sets (IPUZ_CLUES (state->xword)); i ++)
    {
      IpuzClueDirection dir;

      dir = ipuz_clues_clue_set_get_dir (IPUZ_CLUES (state->xword), i);
      if (ipuz_clues_get_n_clues (IPUZ_CLUES (state->xword), dir) > 0)
        {
          clue_id.direction = dir;
          break;
        }
    }

  if (clue_id.direction != IPUZ_CLUE_DIRECTION_NONE)
    {
      IpuzClue *clue = ipuz_clues_get_clue_by_id (IPUZ_CLUES (state->xword), &clue_id);
      g_assert (clue != NULL);

      /* Select the first cell in that clue */
      if (!ipuz_clue_get_first_coord (clue, &state->cursor))
        {
          clue_id.direction = IPUZ_CLUE_DIRECTION_NONE;
        }
    }
  else
    {
      find_first_focusable_cell (state->xword, &state->cursor);
    }

  state->clue = clue_id;
}


/* Public functions */

/**
 * grid_state_new:
 * @xword: An `IpuzCrossword`
 *
 * Creates a new GridState containing @xword. By default, the initial
 * cell will be the first across clue in the puzzle.
 *
 * Returns: (transfer full) The newly allocated `GridState`
 **/
GridState *
grid_state_new (IpuzCrossword    *xword,
                CrosswordsQuirks *quirks,
                GridStateMode     mode)
{
  g_return_val_if_fail (IPUZ_IS_CROSSWORD (xword), NULL);

  return grid_state_new_from_behavior (xword, quirks,
                                       grid_state_mode_to_behavior (mode));
}

GridState *
grid_state_new_from_behavior (IpuzCrossword      *xword,
                              CrosswordsQuirks   *quirks,
                              GridStateBehavior   behavior)
{
  GridState *state;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (xword), NULL);

  state = g_new0 (GridState, 1);

  state->xword = g_object_ref (xword);
  if (quirks)
    state->quirks = (CrosswordsQuirks *)g_object_ref (quirks);
  state->behavior = behavior;

  if (GRID_STATE_HAS_SELECTABLE (state))
    state->selected_cells = cell_array_new ();

  set_initial_cursor (state);

  return state;
}

/**
 * grid_state_dehydrate:
 * @state: A `GridState`
 *
 * Creates a *dehydrated* `GridState`.
 *
 * In general, a `GridState` has all the information to recreate the
 * state of a board by containing cursor information as well as the
 * puzzle. Occasionally, we need to store the state of board without
 * the puzzle. In those instances, we can create a dehydrated
 * `GridState` that just contains cursor information. It can be
 * reversed by calling grid_state_hydrate() with an appropriate puzzle
 *
 * No operations can or should be done on a dehydrated state. It
 * can be freed with grid_state_free()
 *
 * Returns: A new `GridState` without the xword set.
 **/
GridState *
grid_state_dehydrate (GridState *state)
{
  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  state = grid_state_clone (state);
  g_clear_object (&state->xword);
  g_clear_object (&state->quirks);

  return state;
}

/**
 * grid_state_hydrate:
 * @state: A `GridState`
 * @xword: An `IpuzCrossword`
 *
 * *Rehydrates* a dehydrated `GridState`. see grid_state_dehydrate()
 for more information.
 *
 * Returns: A new `GridState` with the xword set to @xword.
 **/
GridState *
grid_state_hydrate (GridState        *state,
                    IpuzCrossword    *xword,
                    CrosswordsQuirks *quirks)
{
  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (IPUZ_IS_CROSSWORD (xword), NULL);
  g_return_val_if_fail (GRID_STATE_DEHYDRATED (state), NULL);

  state = grid_state_clone (state);
  state->xword = g_object_ref (xword);
  if (quirks)
    state->quirks = g_object_ref (quirks);

  return state;
}


GridState *
grid_state_swap_xword (GridState    *state,
                       IpuzCrossword *xword)
{
  GridState *new_state;

  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (IPUZ_IS_CROSSWORD (xword), NULL);

  new_state = grid_state_clone (state);
  g_object_ref (xword);
  g_clear_object (&new_state->xword);
  new_state->xword = xword;

  /* FIXME: Maybe at some point in the future, we can check to make
   * sure the cursor is appropriate for the mode */
  return new_state;
}

/**
 * grid_state_free:
 * @state: A `GridState`
 *
 * Frees a `GridState` and associated resources.
 **/
void
grid_state_free (GridState *state)
{
  if (state == NULL)
    return;

  if (state->xword)
    g_object_unref (G_OBJECT (state->xword));
  if (state->selected_cells)
    cell_array_unref (state->selected_cells);
  if (state->quirks)
    g_object_unref (state->quirks);

  g_free (state);
}

/**
 * grid_state_replace:
 * @old_state: old state which will be freed.
 * @new_state: new state which will replace it.
 *
 * `GridState` is immutable, so its functions return a new
 * `GridState` instead of changing an existing one.  This function is
 * a convenience to avoid freeing a lot of old `GridState` by hand
 * every time one of those functions is called. You can use `state =
 * grid_state_replace (state, grid_state_foo (state, BAR)`, for
 * example.
 *
 * Returns: `new_state`.
 */
GridState *
grid_state_replace (GridState *old_state,
                    GridState *new_state)
{
  if (old_state)
    grid_state_free (old_state);

  return new_state;
}

/**
 * grid_state_change_mode:
 * @state: A `GridState`
 * @mode: The new mode for @state
 *
 * Updates @state to have the new mode.
 *
 * Returns: A new `GridState` with the new mode.
 **/
GridState *
grid_state_change_mode (GridState     *state,
                        GridStateMode  mode)
{
  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  return grid_state_change_behavior (state, grid_state_mode_to_behavior (mode));
}

/**
 * grid_state_change_behavior:
 * @state: A `GridState`
 * @behavior: The new behavior for @state
 *
 * Updates @state to have the new behavior.
 *
 * Returns: A new `GridState` with the new mode.
 **/
GridState *
grid_state_change_behavior (GridState         *state,
                            GridStateBehavior  behavior)
{
  GridState *new_state;

  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  new_state = grid_state_clone (state);

  /* No change. */
  if (behavior == state->behavior)
    return new_state;

  new_state->behavior = behavior;

  /* if the old mode was VIEW, we need to try to set the cursor. We
   * select the first available clue */
  if (! GRID_STATE_HAS_USE_CURSOR (state))
    set_initial_cursor (new_state);

  if (! GRID_STATE_HAS_USE_CURSOR (new_state))
      /* Unset the cursor. This indicates it has no real value */
    new_state->clue.direction = IPUZ_CLUE_DIRECTION_NONE;

  if (GRID_STATE_HAS_SELECTABLE (new_state))
    {
      if (new_state->selected_cells == NULL)
        new_state->selected_cells = cell_array_new ();
    }
  else
    {
      g_clear_pointer (&new_state->saved_selection, cell_array_unref);
      g_clear_pointer (&new_state->selected_cells, cell_array_unref);
    }

  return new_state;
}

static void
grid_state_clue_selected_mut (GridState *state,
                              IpuzClue   *clue)
{
  IpuzClueId clue_id;

  ipuz_clues_get_id_by_clue (IPUZ_CLUES (state->xword), clue, &clue_id);
  if (ipuz_clue_id_equal (&state->clue, &clue_id))
    return;

  if (ipuz_clue_get_first_coord (clue, &state->cursor))
    state->clue = clue_id;
  else
    state->clue.direction = IPUZ_CLUE_DIRECTION_NONE;
}

/**
 * grid_state_clue_selected:
 * @state: A `GridState`
 * @clue: A `IpuzClue`
 *
 * Updates @state to have the currently selected clue to be
 * @clue. The cursor will be set to be the first unguessed cell in
 * clue. Otherwise, it will put the cursor on the first cell.
 *
 * Returns: A new `GridState` with the new state.  Remember to free the old one.
 **/
GridState *
grid_state_clue_selected (GridState *state,
                          IpuzClue   *clue)
{
  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (clue != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  state = grid_state_clone (state);

  if (GRID_STATE_CURSOR_SET (state))
    grid_state_clue_selected_mut (state, clue);

  return state;
}

static void
grid_state_cell_selected_mut (GridState     *state,
                              IpuzCellCoord  coord)
{
  IpuzCell *cursor_cell;
  const IpuzClue *new_clue = NULL;

  cursor_cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &coord);
  g_return_if_fail (cursor_cell != NULL);

  /* If you're normal-only, nothing happens if you click on a NULL or
   * BLOCK cell */
  if (GRID_STATE_HAS_NORMAL_ONLY (state) &&
      ! IPUZ_CELL_IS_NORMAL (cursor_cell))
    return;

  if (GRID_STATE_HAS_SELECTABLE (state))
    {
      /* FIXME(selection): We have broader selection. We should
       * revisit this */
      /* This is only really used for the autofill dialog. Thus, we
       * only let you select a cell if it doesn't have any
       * solutions. If we ever allow broader selection for the
       * crossword, we should change this logic. */
      if (ipuz_cell_get_solution (cursor_cell) == NULL)
        cell_array_toggle (state->selected_cells, coord);
      return;
    }

  /* If you click on the focus cell, we change orientation... */
  if (state->cursor.row == coord.row &&
      state->cursor.column == coord.column)
    {
      new_clue = ipuz_cell_get_clue (cursor_cell,
                                     ipuz_clue_direction_switch (state->clue.direction));

      if (new_clue)
        {
          ipuz_clues_get_id_by_clue (IPUZ_CLUES (state->xword), new_clue, &state->clue);
        }

      return;
    }

  state->cursor = coord;

  if (state->clue.direction != IPUZ_CLUE_DIRECTION_NONE)
    {
      new_clue = ipuz_cell_get_clue (cursor_cell, state->clue.direction);
      if (new_clue == NULL) {
        new_clue = ipuz_cell_get_clue (cursor_cell,
                                       ipuz_clue_direction_switch (state->clue.direction));
      }
    }
  else
    {
      /* No current direction, so pick any that is available */
      for (guint i = 0; i < ipuz_clues_get_n_clue_sets (IPUZ_CLUES (state->xword)); i++)
        {
          IpuzClueDirection dir;
          dir = ipuz_clues_clue_set_get_dir (IPUZ_CLUES (state->xword), i);
          new_clue = ipuz_cell_get_clue (cursor_cell, dir);
          if (new_clue)
            break;
        }
    }

  ipuz_clues_get_id_by_clue (IPUZ_CLUES (state->xword), new_clue, &state->clue);
}

/**
 * grid_state_cell_selected:
 * @state: A `GridState`
 * @coord: Coordinates of the cell to be selected
 *
 * Updates @state to have a new focused cell. If the selected cell is
 * the current cell, then it will change the direction of the clue, if
 * that's an option.
 *
 * Returns: A new `GridState` with the new state.  Remember to free the old one.
 **/
GridState *
grid_state_cell_selected (GridState    *state,
                          IpuzCellCoord  coord)
{
  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  state = grid_state_clone (state);

  if (GRID_STATE_CURSOR_SET (state))
    grid_state_cell_selected_mut (state, coord);

  return state;
}

/* returns the next clue logically in a puzzle. We wrap around from
 * across<->down and back. */
static IpuzClue *
get_next_clue (GridState  *state,
               IpuzClueId  prev_clue,
               gint        incr)
{
  gint clue_index;
  guint n_clues;

  g_assert (incr == -1 || incr == 1);

  /* No direction - find the first clue at whatever direction is
   * available */
  /* FIXME(cluesets): make this work with clue sets */
  if (IPUZ_CLUE_ID_IS_UNSET (&prev_clue))
    {
      IpuzClueId clue_id = {
        .direction = IPUZ_CLUE_DIRECTION_ACROSS,
        .index = 0
      };
      IpuzClue *clue = ipuz_clues_get_clue_by_id (IPUZ_CLUES (state->xword), &clue_id);

      if (clue)
        return clue;

      clue_id.direction = IPUZ_CLUE_DIRECTION_DOWN;
      return ipuz_clues_get_clue_by_id (IPUZ_CLUES (state->xword), &clue_id);
    }

  clue_index = prev_clue.index;
  n_clues = ipuz_clues_get_n_clues (IPUZ_CLUES (state->xword), prev_clue.direction);

  if (clue_index == 0 && incr == -1)
    {
      IpuzClueDirection other_dir = ipuz_clue_direction_switch (prev_clue.direction);
      guint other_n_clues = ipuz_clues_get_n_clues (IPUZ_CLUES (state->xword), other_dir);
      IpuzClueId clue_id = {
        .direction = other_dir,
        .index = other_n_clues - 1,
      };

      if (other_n_clues > 0)
        return ipuz_clues_get_clue_by_id (IPUZ_CLUES (state->xword), &clue_id);
      else
        return NULL;
    }
  else if ((guint) (clue_index + incr) >= n_clues)
    {
      IpuzClueId clue_id = {
        .index = 0,
      };

      clue_id.direction = ipuz_clue_direction_switch (prev_clue.direction);
      return ipuz_clues_get_clue_by_id (IPUZ_CLUES (state->xword), &clue_id);
    }
  else
    {
      IpuzClueId clue_id = {
        .direction = prev_clue.direction,
        .index = clue_index + incr,
      };
      return ipuz_clues_get_clue_by_id (IPUZ_CLUES (state->xword), &clue_id);
    }
}

static IpuzCellCoord
acrostic_get_next_clue_cell (GridState *state,
		             gint       incr)
{
  IpuzCellCoord coord = state->cursor;
  IpuzClue *clue;
  guint n_coords;

  clue = ipuz_clues_get_clue_by_id (IPUZ_CLUES (state->xword), &state->clue);
  n_coords = ipuz_clue_get_n_coords (clue);

  for (guint i = 0; i < n_coords; i++)
    {
      IpuzCellCoord advance_coord;

      ipuz_clue_get_coord (clue, i, &advance_coord);

      if (ipuz_cell_coord_equal (&coord, &advance_coord))
        {
	  /* If we are at the end of the clue_grid while guessing
	   * or we are at the start of the clue_grid while doing a backspace
	   * then don't advance
	   */
          if ((incr == 1 && i == n_coords - 1) || (incr == -1 && i == 0))
	    return coord;

          ipuz_clue_get_coord (clue, i + incr, &advance_coord);

	  return advance_coord;
	}

    }

  return coord;
}

static IpuzCellCoord
acrostic_get_next_cell (GridState *state,
	                gint        incr)
{
  IpuzCellCoord coord = state->cursor;
  IpuzClue *clue;
  guint n_coords;

  clue = GET_QUOTE_CLUE (state->xword);
  if (clue == NULL)
    return coord;

  n_coords = ipuz_clue_get_n_coords (clue);

  for (guint i = 0; i < n_coords; i++)
    {
      IpuzCellCoord advance_coord;

      ipuz_clue_get_coord (clue, i, &advance_coord);

      if (ipuz_cell_coord_equal (&coord, &advance_coord))
        {
	  /* If we are at the end of the main_grid while guessing
	   * or we are at the start of the main_grid while doing a backspace
	   * then don't advance
	   */
          if ((incr == 1 && i == n_coords) || (incr == -1 && i == 0))
	    return coord;

          ipuz_clue_get_coord (clue, i + incr, &advance_coord);

	  return advance_coord;
	}
    }

  return coord;
}

static IpuzCellCoord
coord_for_forward_back (GridState       *state,
                        IpuzClueDirection direction,
                        gint              incr)
{
  IpuzCellCoord coord = state->cursor;

  g_assert (incr == -1 || incr == 1);

  if (direction == IPUZ_CLUE_DIRECTION_ACROSS)
    coord.column += incr;
  else if (direction == IPUZ_CLUE_DIRECTION_DOWN)
    coord.row += incr;
  else if (ACROSTIC_MAIN_GRID_HAS_FOCUS (state))
    coord = acrostic_get_next_cell (state, incr);
  else if (!ACROSTIC_MAIN_GRID_HAS_FOCUS (state))
    coord = acrostic_get_next_clue_cell (state, incr);

  return coord;
}

static void
grid_state_focus_forward_back (GridState *state,
                               gint       incr)
{
  if (state->clue.direction == IPUZ_CLUE_DIRECTION_NONE)
    {
      /* If there is no direction, there's nowhere to go. */
      return;
    }

  /* Don't move past the last cell in the current clue */

  IpuzCellCoord advanced_coord = coord_for_forward_back (state, state->clue.direction, incr);
  IpuzCell *cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &advanced_coord);

  if (IPUZ_CELL_IS_NORMAL (cell))
    {
      const IpuzClue *clue;

      if (ACROSTIC_MAIN_GRID_HAS_FOCUS (state))
	clue = GET_QUOTE_CLUE (state->xword);
      else
        clue = ipuz_cell_get_clue (cell, state->clue.direction);

      if (clue)
        {
          IpuzClueId clue_id;

          ipuz_clues_get_id_by_clue (IPUZ_CLUES (state->xword), clue, &clue_id);
          if (ipuz_clue_id_equal (&clue_id, &state->clue))
            {
              grid_state_cell_selected_mut (state, advanced_coord);
            }
	  else if (IPUZ_IS_ACROSTIC (state->xword) && clue_id.index == 0)
	    {
	      grid_state_cell_selected_mut (state, advanced_coord);
	    }
        }
    }
}

static void
grid_state_focus_forward_back_clue (GridState *state,
                                    gint        incr)
{
  IpuzClue *next_clue = get_next_clue (state, state->clue, incr);

  if (next_clue)
    {
      grid_state_clue_selected_mut (state, next_clue);
    }
}

static void
grid_state_focus_forward_back_emptycell (GridState       *state,
                                         gboolean  current_clue,
                                         gboolean     all_clues,
                                         gint              incr)
{
  IpuzClue *clue_ptr;
  IpuzClueId clue_id;
  IpuzClueId orig_clue_id;

  /* FIXME(editor): We should probably do something when
   * HAS_SHOW_GUESS isn't TRUE. I'm not sure what the right behavoir
   * is though. */
  if (!(GRID_STATE_HAS_USE_CURSOR (state) &&
        (GRID_STATE_HAS_SHOW_GUESS (state))))
    return;

  if (IPUZ_IS_ACROSTIC (state->xword) && ACROSTIC_MAIN_GRID_HAS_FOCUS (state))
    clue_ptr = GET_QUOTE_CLUE (state->xword);
  else
    clue_ptr = ipuz_clues_get_clue_by_id (IPUZ_CLUES (state->xword), &state->clue);
  clue_id = state->clue;
  if (clue_ptr == NULL)
    return;

  orig_clue_id = clue_id;

  if (current_clue)
    {
      if (advance_through_clue_until_open (state, clue_ptr, TRUE, FALSE))
        return;

      /* if we're not searching all clues, jump to earlier empty cell */
      if (!all_clues)
        {
          if (advance_through_clue_until_open (state, clue_ptr, FALSE, TRUE))
            return;
        }
    }

  if (!all_clues)
    {
      /* We didn't find anything open, so we just go to the next cell */
      guess_advance_adjacent (state);
      return;
    }

  while (TRUE)
    {
      clue_ptr = get_next_clue (state, clue_id, incr);
      ipuz_clues_get_id_by_clue (IPUZ_CLUES (state->xword), clue_ptr, &clue_id);

      if (clue_id.direction == orig_clue_id.direction &&
          clue_id.index == orig_clue_id.index)
        return;

      if (advance_through_clue_until_open (state, clue_ptr, FALSE, FALSE))
        return;
    }
}

/* Keep in sync with left/right below */
static void
grid_state_focus_up_down (GridState *state,
                          gint       incr)
{
  IpuzCell *cell;

  g_assert (incr == -1 || incr == 1);

  if (ACROSTIC_CLUE_GRID_HAS_FOCUS (state))
    {
      grid_state_focus_forward_back_clue (state, incr);
      return;
    }

  /* this will underflow when cursor_row == 0, but that should be okay.
   */
  IpuzCellCoord coord = {
    .row = state->cursor.row + incr,
    .column = state->cursor.column
  };

  cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &coord);
  while (cell)
    {
      if (IPUZ_CELL_IS_NORMAL (cell) ||
          ! GRID_STATE_HAS_NORMAL_ONLY (state))
        {
          grid_state_cell_selected_mut (state, coord);
          /* If we change directions on move, we toggle the cursor twice */
          if (crosswords_quirks_get_switch_on_move (state->quirks) &&
              state->clue.direction == IPUZ_CLUE_DIRECTION_ACROSS)
            grid_state_cell_selected_mut (state, coord);
          break;
        }

      coord.row += incr;
      cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &coord);
    }
}

/* Keep in sync with up/down above */
static void
grid_state_focus_left_right (GridState *state,
                             gint        incr)
{
  IpuzCell *cell;

  g_assert (incr == -1 || incr == 1);

  if (IPUZ_IS_ACROSTIC (state->xword))
    {
      grid_state_focus_forward_back (state, incr);
      return;
    }

  /* this will underflow when cursor_column == 0, but that should be okay.
   */
  IpuzCellCoord coord = {
    .row = state->cursor.row,
    .column = state->cursor.column + incr
  };

  cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &coord);
  while (cell)
    {
      if (IPUZ_CELL_IS_NORMAL (cell) ||
          ! GRID_STATE_HAS_NORMAL_ONLY (state))
        {
          grid_state_cell_selected_mut (state, coord);
          /* If we change directions on move, we toggle the cursor twice */
          if (crosswords_quirks_get_switch_on_move (state->quirks) &&
              state->clue.direction == IPUZ_CLUE_DIRECTION_DOWN)
            grid_state_cell_selected_mut (state, coord);
          break;
        }

      coord.column += incr;
      cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &coord);
    }
}

static void
grid_state_focus_toggle (GridState *state)
{
  grid_state_cell_selected_mut (state, state->cursor);
}

static void
grid_state_delete_selectable (GridState *state)
{
  for (guint i = 0; i < cell_array_len (state->selected_cells); i++)
    {
      IpuzCellCoord coord = cell_array_index (state->selected_cells, i);
      IpuzCell *cell;

      cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &coord);
      if (IPUZ_CELL_IS_NORMAL (cell))
        ipuz_cell_set_solution (cell, NULL);
    }
}

static void
grid_state_delete (GridState *state)
{
  IpuzCellCoord coord = {
    .row = state->cursor.row,
    .column = state->cursor.column
  };

  if (! GRID_STATE_HAS_EDIT_CELLS (state))
    return;

  /* FIXME(cleanup): I don't love this behavior. We are counting on
   * solve_guess/solution_guess to not advance the cursor when
   * done. Otherwise we go back, and then advance the cursor by
   * entering some text */
  if (GRID_STATE_HAS_SELECTABLE (state))
    grid_state_delete_selectable (state);
  else if (GRID_STATE_HAS_SHOW_GUESS (state))
    grid_state_solve_guess (state, NULL, coord);
  else
    grid_state_grid_guess (state, NULL, coord);
}

static void
grid_state_backspace (GridState *state)
{
  if (GRID_STATE_HAS_SELECTABLE (state))
    {
      grid_state_delete_selectable (state);
    }
  else
    {
      grid_state_delete (state);
      grid_state_focus_forward_back (state, -1);
    }
}


/**
 * grid_state_do_command:
 * @state: A `GridState`
 * @kind: Command kind.
 *
 * Updates the state based on a command @kind.  Not all commands are valid in all states;
 * invalid ones will be ignored and a new state equivalent to the old one will be
 * returned.
 *
 * Returns: A new `GridState` with the new state.  Remember to free the old one.
 **/
GridState *
grid_state_do_command (GridState   *state,
                       GridCmdKind  kind)
{
  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  state = grid_state_clone (state);

  if (! GRID_STATE_HAS_USE_CURSOR (state))
    return state;

  switch (kind)
    {
    case GRID_CMD_KIND_UP:
      grid_state_focus_up_down (state, -1);
      break;
    case GRID_CMD_KIND_RIGHT:
      grid_state_focus_left_right (state, 1);
      break;
    case GRID_CMD_KIND_DOWN:
      grid_state_focus_up_down (state, 1);
      break;
    case GRID_CMD_KIND_LEFT:
      grid_state_focus_left_right (state, -1);
      break;
    case GRID_CMD_KIND_FORWARD:
      grid_state_focus_forward_back (state, 1);
      break;
    case GRID_CMD_KIND_BACK:
      grid_state_focus_forward_back (state, -1);
      break;
    case GRID_CMD_KIND_FORWARD_EMPTYCELL:
      grid_state_focus_forward_back_emptycell (state, FALSE, TRUE, 1);
      break;
    case GRID_CMD_KIND_BACK_EMPTYCELL:
      grid_state_focus_forward_back_emptycell (state, FALSE, TRUE, -1);
      break;
    case GRID_CMD_KIND_FORWARD_CLUE:
      grid_state_focus_forward_back_clue (state, 1);
      break;
    case GRID_CMD_KIND_BACK_CLUE:
      grid_state_focus_forward_back_clue (state, -1);
      break;
    case GRID_CMD_KIND_SWITCH:
      grid_state_focus_toggle (state);
      break;
    case GRID_CMD_KIND_BACKSPACE:
      grid_state_backspace (state);
      break;
    case GRID_CMD_KIND_DELETE:
      grid_state_delete (state);
      break;
    default:
      break;
    }

  return state;
}

/* Guess a cell, but don't advance the cursor. That's handled elsewhere */
static void
grid_state_solve_guess (GridState     *state,
                        const gchar   *guess,
                        IpuzCellCoord  coord)
{
  IpuzCell *cell;
  IpuzGuesses *guesses;
  const gchar *cur_guess;

  guesses = ipuz_grid_get_guesses (IPUZ_GRID (state->xword));
  if (guesses == NULL)
    return;

  cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &coord);
  if (! IPUZ_CELL_IS_GUESSABLE (cell))
    return;

  cur_guess = ipuz_guesses_get_guess (guesses, &coord);

  /* Handle Dutch crosswords */
  if (crosswords_quirks_get_ij_digraph (state->quirks))
    {
      if (g_strcmp0 (cur_guess, "I") == 0 &&
          g_strcmp0 (guess, "J") == 0)
        {
          ipuz_guesses_set_guess (guesses, &coord, "IJ");
          return;
        }
    }

  if (g_strcmp0 (guess, cur_guess) != 0)
    {
      ipuz_guesses_set_guess (guesses, &coord, guess);
      return;
    }
}

static void
grid_state_grid_guess (GridState     *state,
                       const gchar   *guess,
                       IpuzCellCoord  coord)
{
  const gchar *cur_solution;
  IpuzCell *cell;
  gboolean update_grid = FALSE;
  IpuzSymmetry symmetry = IPUZ_SYMMETRY_NONE;

  cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &coord);
  cur_solution = ipuz_cell_get_solution (cell);
  if (state->quirks)
    symmetry = crosswords_quirks_get_symmetry (state->quirks);

  /* We can get NULL as a guess. Makes this a little messy */
  if (guess == NULL)
    {
      if (cur_solution == NULL || cur_solution[0] == '\0')
        return;
      ipuz_cell_set_solution (cell, NULL);
      return;
    }

  /* Handle initial val */
  if (IPUZ_CELL_IS_INITIAL_VAL (cell))
    {
      ipuz_cell_set_initial_val (cell, guess);
      /* This won't affect polarity, as the cell type didn't change */
      return;
    }

  /* Check to see if we're writing a block */
  if (strstr (GRID_STATE_BLOCK_CHARS, guess) != NULL)
    {
      /* If we're barred, we set a line instead of a block */
      if (IPUZ_IS_BARRED (state->xword))
        {
          IpuzStyleSides cell_bars = 0;

          if (state->clue.direction == IPUZ_CLUE_DIRECTION_ACROSS)
            cell_bars = ipuz_barred_calculate_side_toggle (IPUZ_BARRED (state->xword),
                                                           &coord, IPUZ_STYLE_SIDES_LEFT,
                                                           symmetry);
          else if (state->clue.direction == IPUZ_CLUE_DIRECTION_DOWN)
            cell_bars = ipuz_barred_calculate_side_toggle (IPUZ_BARRED (state->xword),
                                                           &coord, IPUZ_STYLE_SIDES_TOP,
                                                           symmetry);
          /* else... */
          /* If direction is none we're in the middle of a
           * box. Unclear which one to untoggle in that instance */

          update_grid = ipuz_barred_set_cell_bars (IPUZ_BARRED (state->xword), &coord, cell_bars);
        }
      else
        {
          if (IPUZ_CELL_IS_BLOCK (cell))
            ipuz_cell_set_cell_type (cell, IPUZ_CELL_NORMAL);
          else
            ipuz_cell_set_cell_type (cell, IPUZ_CELL_BLOCK);
          update_grid = TRUE;
        }
    }
  /* We have a normal character. Write that. */
  else if (g_strcmp0 (guess, cur_solution) != 0)
    {
      if (! IPUZ_CELL_IS_NORMAL (cell))
        {
          ipuz_cell_set_cell_type (cell, IPUZ_CELL_NORMAL);
          update_grid = TRUE;
        }

      ipuz_cell_set_solution (cell, guess);
    }

  /* We changed the grid. Update it to keep polarity, if needed */
  if (update_grid)
    {
      g_autoptr (GArray) coords = NULL;

      /* Change the cell type */
      coords = g_array_new (FALSE, FALSE, sizeof (IpuzCellCoord));
      g_array_append_val (coords, coord);

      ipuz_crossword_fix_all (state->xword,
                              "symmetry", symmetry,
                              "symmetry-coords", coords,
                              NULL);

      if (IPUZ_CELL_IS_BLOCK (cell))
        {
          state->clue.direction = IPUZ_CLUE_DIRECTION_NONE;
          state->clue.index = 0;
        }
      else
        {
          const IpuzClue *clue;
          IpuzClueDirection old_direction = state->clue.direction;

          if (old_direction == IPUZ_CLUE_DIRECTION_NONE)
            old_direction = IPUZ_CLUE_DIRECTION_ACROSS;

          clue = ipuz_cell_get_clue (cell, old_direction);
          if (clue == NULL)
            clue = ipuz_cell_get_clue (cell, ipuz_clue_direction_switch (old_direction));
          ipuz_clues_get_id_by_clue (IPUZ_CLUES (state->xword), clue, &state->clue);
        }
    }
}

GridStateBehavior
grid_state_mode_to_behavior (GridStateMode mode)
{
  switch (mode)
    {
    case GRID_STATE_SOLVE:
      return GRID_STATE_SOLVE_FLAGS;
    case GRID_STATE_BROWSE:
      return GRID_STATE_BROWSE_FLAGS;
    case GRID_STATE_EDIT:
      return GRID_STATE_EDIT_FLAGS;
    case GRID_STATE_EDIT_BROWSE:
      return GRID_STATE_EDIT_BROWSE_FLAGS;
    case GRID_STATE_SELECT:
      return GRID_STATE_SELECT_FLAGS;
    case GRID_STATE_VIEW:
      return GRID_STATE_VIEW_FLAGS;
    default:
      g_assert_not_reached ();
    }
}


static void
guess_advance_adjacent (GridState *state)
{
  g_return_if_fail (state != NULL);

  grid_state_focus_forward_back (state, 1);
}


/* This will iterate through a clue until it finds an open space. If
 * it finds one, it will set the cursor to that open space and return
 * TRUE. Otherwise, it will return FALSE */
static gboolean
advance_through_clue_until_open (GridState *state,
                                 IpuzClue  *clue,
                                 gboolean   start_at_cursor,
                                 gboolean   end_at_cursor)
{
  gboolean found_cursor = FALSE;
  IpuzGuesses *guesses;

  g_assert (clue != NULL);

  guesses = ipuz_grid_get_guesses (IPUZ_GRID (state->xword));
  for (guint i = 0; i < ipuz_clue_get_n_coords (clue); i++)
    {
      IpuzCellCoord coord;
      const gchar *guess;

      ipuz_clue_get_coord (clue, i, &coord);

      if ((start_at_cursor || end_at_cursor) &&
          coord.column == state->cursor.column &&
          coord.row == state->cursor.row)
        {
          if (end_at_cursor)
              return FALSE;

          if (start_at_cursor)
            {
              found_cursor = TRUE;
              continue;
            }
        }

      if (start_at_cursor && !found_cursor)
        continue;

      guess = ipuz_guesses_get_guess (guesses, &coord);
      if (guess == NULL)
        {
          /* Set the direction from the passed clue and invalidate cursor */
          IpuzClueId clue_id;

          ipuz_clues_get_id_by_clue (IPUZ_CLUES (state->xword), clue, &clue_id);
          state->clue.direction = clue_id.direction;
          state->cursor.row = coord.row + 1;

          grid_state_cell_selected_mut (state, coord);
          return TRUE;
        }
    }

  return FALSE;
}

static void
guess_advance (GridState *state)
{
  g_assert (GRID_STATE_HAS_USE_CURSOR (state));
  g_assert (GRID_STATE_HAS_SHOW_GUESS (state));

  switch (crosswords_quirks_get_guess_advance (state->quirks))
    {
    case QUIRKS_GUESS_ADVANCE_ADJACENT:
      guess_advance_adjacent (state);
      break;
    case QUIRKS_GUESS_ADVANCE_OPEN:
      grid_state_focus_forward_back_emptycell (state, TRUE, TRUE, 1);
      break;
    case QUIRKS_GUESS_ADVANCE_OPEN_IN_CLUE:
      grid_state_focus_forward_back_emptycell (state, TRUE, FALSE, 1);
      break;
    default:
      g_assert_not_reached ();
    }
}

static void
guess_advance_edit (GridState *state)
{
  IpuzClue *clue;

  g_assert (GRID_STATE_HAS_USE_CURSOR (state));
  g_assert (!GRID_STATE_HAS_SHOW_GUESS (state));

  if (state->clue.direction == IPUZ_CLUE_DIRECTION_DOWN)
    {
      if (state->cursor.row + 1 < ipuz_grid_get_height (IPUZ_GRID (state->xword)))
        state->cursor.row ++;
    }
  else if (state->clue.direction == IPUZ_CLUE_DIRECTION_ACROSS)
    {
      if (state->cursor.column + 1 < ipuz_grid_get_width (IPUZ_GRID (state->xword)))
        state->cursor.column ++;
    }

  clue = ipuz_clues_find_clue_by_coord (IPUZ_CLUES (state->xword),
                                        state->clue.direction,
                                        &state->cursor);
  if (clue == NULL)
    clue = ipuz_clues_find_clue_by_coord (IPUZ_CLUES (state->xword),
                                          ipuz_clue_direction_switch (state->clue.direction),
                                          &state->cursor);
  ipuz_clues_get_id_by_clue (IPUZ_CLUES (state->xword), clue, &state->clue);
}

/**
 * grid_state_guess:
 * @state: A `GridState`
 * @guess: (nullable) A new guess
 *
 * Adds a guess to the current focused cell. If @guess is NULL or "",
 * then the cell is cleared.
 *
 * Returns: A new `GridState` with the new state.  Remember to free the old one.
 **/
GridState *
grid_state_guess (GridState   *state,
                  const gchar *guess)
{
  IpuzCellCoord coord = {
    .row = state->cursor.row,
    .column = state->cursor.column
  };

  g_return_val_if_fail (state != NULL, FALSE);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  state = grid_state_clone (state);

  if (GRID_STATE_HAS_EDIT_CELLS (state))
    {
      if (GRID_STATE_HAS_SHOW_GUESS (state))
        {
          grid_state_solve_guess (state, guess, coord);
          guess_advance (state);
        }
      else
        {
          grid_state_grid_guess (state, guess, coord);

          /* We only advance if we're not a block */
          if (guess && strstr (GRID_STATE_BLOCK_CHARS, guess) == NULL)
            guess_advance_edit (state);
        }
    }

  return state;
}

GridState *
grid_state_guess_at_cell (GridState       *state,
                          const gchar      *guess,
                          IpuzCellCoord     coord)
{
  g_return_val_if_fail (state != NULL, FALSE);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  state = grid_state_clone (state);

  if (GRID_STATE_HAS_EDIT_CELLS (state))
    {
      if (GRID_STATE_HAS_SHOW_GUESS (state))
        {
          grid_state_solve_guess (state, guess, coord);
        }
      else
        {
          grid_state_grid_guess (state, guess, coord);
        }
    }

  return state;

}


/**
 * grid_state_guess_word:
 * @state: A `GridState`
 * @coord: A `IpuzCellCoord` in which to start the word
 * @direction: A `IpuzClueDirection`in which to fill in the word
 * @word: A sequence of UTF-8 characters to insert into the state
 *
 * Adds @word to the current @state one character at a time. It will
 * start at @coord and will continue in direction indicated by
 * @direction. It will stop adding the clue once it hits a block.
 *
 * If the word has a "?" in it, it will skip that letter.
 *
 * Returns: A new `GridState` with the new state.  Remember to free the old one.
 **/
GridState *
grid_state_guess_word (GridState         *state,
                       IpuzCellCoord      coord,
                       IpuzClueDirection  direction,
                       const gchar       *word)
{
  g_autofree gchar *normalized_word = NULL;
  IpuzClue *clue;
  const gchar *ptr;
  const gchar *end;

  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (word != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  state = grid_state_clone (state);
  state->cursor = coord;
  clue = ipuz_clues_find_clue_by_coord (IPUZ_CLUES (state->xword), direction, &coord);
  ipuz_clues_get_id_by_clue (IPUZ_CLUES (state->xword), clue, &state->clue);

  if (! GRID_STATE_HAS_EDIT_CELLS (state))
    return state;

  normalized_word = g_utf8_normalize (word, -1, G_NORMALIZE_NFC);

  ptr = normalized_word;
  while (ptr[0])
    {
      g_autofree gchar *cluster = NULL;
      IpuzCellCoord old_cursor;

      cluster = utf8_get_next_cluster (ptr, &end);
      ptr = end;

      /* cluster being NULL means we couldn't parse some text and
       * should stop processing */
      if (cluster == NULL)
        break;

      /* We skip any characters that would set a block */
      if (strstr (GRID_STATE_BLOCK_CHARS, cluster) != NULL)
        continue;

      old_cursor = state->cursor;
      if (strstr (GRID_STATE_SKIP_CHARS, cluster) != NULL)
        {
          grid_state_focus_forward_back (state, 1);
        }
      else if (GRID_STATE_HAS_SHOW_GUESS (state))
        {
          grid_state_solve_guess (state, cluster, state->cursor);
          grid_state_focus_forward_back (state, 1);
        }
      else
        {
          grid_state_grid_guess (state, cluster, state->cursor);
          grid_state_focus_forward_back (state, 1);
        }

      /* If the cursor didn't move, we hit a block or an edge */
      if (state->cursor.row == old_cursor.row &&
          state->cursor.column == old_cursor.column)
        break;
    }

  return state;
}

GridState *
grid_state_commit_pencil (GridState *state,
                          gboolean  *state_changed)
{
  IpuzGuesses *guesses;
  guint rows, columns;

  g_return_val_if_fail (state != NULL, NULL);

  state = grid_state_clone (state);
  if (state_changed)
    *state_changed = FALSE;

  guesses = ipuz_grid_get_guesses (IPUZ_GRID (state->xword));
  if (guesses == NULL)
    return state;

  rows = ipuz_grid_get_height (IPUZ_GRID (state->xword));
  columns = ipuz_grid_get_width (IPUZ_GRID (state->xword));
  for (guint row = 0; row < rows; row++)
    {
      for (guint column = 0; column < columns; column++)
        {
          const gchar *solution;
          IpuzCellCoord coord = {
            .row = row,
            .column = column
          };

          solution = ipuz_guesses_get_guess (guesses, &coord);
          if (solution && solution[0])
            {
              IpuzCell *cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &coord);
              const gchar *cell_solution = ipuz_cell_get_solution (cell);

              if (g_strcmp0 (cell_solution, solution))
                {
                  if (state_changed)
                    *state_changed = TRUE;
                  ipuz_cell_set_solution (cell, solution);
                }
            }
        }
    }

  /* Clear out the pencil marking guesses */
  ipuz_grid_set_guesses (IPUZ_GRID (state->xword), NULL);
  return state;
}

GridState *
grid_state_set_cell_type_selection (GridState    *state,
                                    IpuzCellType  cell_type)
{
  IpuzCell *cell;
  IpuzSymmetry symmetry = IPUZ_SYMMETRY_NONE;

  if (state->quirks)
    symmetry = crosswords_quirks_get_symmetry (state->quirks);

  for (guint i = 0; i < cell_array_len (state->selected_cells); i++)
    {
      IpuzCellCoord coord = cell_array_index (state->selected_cells, i);

      cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &coord);

      ipuz_cell_set_cell_type (cell, cell_type);
    }

  ipuz_crossword_fix_all (state->xword,
                          "symmetry", symmetry,
                          "symmetry-coords", state->selected_cells,
                          NULL);

  return state;
}

GridState *
grid_state_set_cell_type_coord (GridState        *state,
                                IpuzCellCoord     coord,
                                IpuzCellType      cell_type)
{
  IpuzCell *cell;
  g_autoptr (CellArray) coords = NULL;
  IpuzSymmetry symmetry = IPUZ_SYMMETRY_NONE;

  cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &coord);
  if (cell == NULL)
    return state;

  if (state->quirks)
    symmetry = crosswords_quirks_get_symmetry (state->quirks);

  /* We set the cell type regardless of what it is. We could be
   * resetting a block to be a block, but enforcing symmetry */
  ipuz_cell_set_cell_type (cell, cell_type);
  coords = cell_array_new ();
  cell_array_add (coords, coord);
  ipuz_crossword_fix_all (state->xword,
                          "symmetry", symmetry,
                          "symmetry-coords", coords,
                          NULL);

  /* Clean up the clue after changing the type */
  if (IPUZ_CELL_IS_BLOCK (cell))
    {
      state->clue.direction = IPUZ_CLUE_DIRECTION_NONE;
      state->clue.index = 0;
    }
  else
    {
      const IpuzClue *clue;
      IpuzClueDirection old_direction = state->clue.direction;

      if (old_direction == IPUZ_CLUE_DIRECTION_NONE)
        old_direction = IPUZ_CLUE_DIRECTION_ACROSS;

      clue = ipuz_cell_get_clue (cell, old_direction);
      if (clue == NULL)
        clue = ipuz_cell_get_clue (cell, ipuz_clue_direction_switch (old_direction));
      ipuz_clues_get_id_by_clue (IPUZ_CLUES (state->xword), clue, &state->clue);
    }
  return state;
}

GridState *
grid_state_set_cell_type (GridState        *state,
                          IpuzCellCoord     coord,
                          IpuzCellType      cell_type)
{
  state = grid_state_clone (state);

  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  /* We only allow you to set the cell type iff you can edit cells and
   * have show_guess available
   */
  if (! GRID_STATE_HAS_EDIT_CELLS (state) || GRID_STATE_HAS_SHOW_GUESS (state))
    return state;

  /* We ignore the coord if there's a selection. The coord is just the
   * anchor, and it might not be selected. */
  if (GRID_STATE_HAS_SELECTABLE (state))
    return grid_state_set_cell_type_selection (state, cell_type);
  else
    return grid_state_set_cell_type_coord (state, coord, cell_type);
}

static void
cell_set_use_initial_val (IpuzCell *cell,
                          gboolean  use_initial_val)
{
  const gchar *solution;
  const gchar *initial_val;

  solution = ipuz_cell_get_solution (cell);
  initial_val = ipuz_cell_get_initial_val (cell);

  if (use_initial_val)
    {
      if (solution && ! initial_val)
        ipuz_cell_set_initial_val (cell, solution);
      ipuz_cell_set_solution (cell, NULL);
    }
  else
    {
      if (initial_val && !solution)
        ipuz_cell_set_solution (cell, initial_val);
      ipuz_cell_set_initial_val (cell, NULL);
    }
}

GridState *
grid_state_set_initial_val (GridState     *state,
                            gboolean       use_initial_val)
{
  IpuzCell *cell;

  state = grid_state_clone (state);

  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  /* We only allow you to set the initial_val iff you can edit cells
   * and have show_guess available
   */
  if (! GRID_STATE_HAS_EDIT_CELLS (state) || GRID_STATE_HAS_SHOW_GUESS (state))
    return state;

  /* We ignore the coord if there's a selection. The coord is just the
   * anchor, and it might not be selected. */
   if (GRID_STATE_HAS_SELECTABLE (state))
     {
       for (guint i = 0; i < cell_array_len (state->selected_cells); i++)
         {
           IpuzCellCoord coord = cell_array_index (state->selected_cells, i);
           cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword),
                                      &coord);
           cell_set_use_initial_val (cell, use_initial_val);
         }
     }
   else
     {
       cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword),
                                  &state->cursor);
       cell_set_use_initial_val (cell, use_initial_val);
     }

   return state;
}
GridState *
grid_state_select_cell (GridState    *state,
                        IpuzCellCoord  coord)
{
  IpuzCell *cell;

  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  state = grid_state_clone (state);
  g_clear_pointer (&state->selected_cells, cell_array_unref);

  if (! GRID_STATE_HAS_SELECTABLE (state))
    return state;

  cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &coord);
  g_return_val_if_fail (cell != NULL, NULL);

  if (! IPUZ_CELL_IS_NORMAL (cell))
    return state;

  state->selected_cells = cell_array_new ();
  cell_array_add (state->selected_cells, coord);

  return state;
}

GridState *
grid_state_select_toggle (GridState    *state,
                          IpuzCellCoord  coord)
{
  IpuzCell *cell;

  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  state = grid_state_clone (state);

  if (! GRID_STATE_HAS_SELECTABLE (state))
    return state;

  cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &coord);
  g_return_val_if_fail (cell != NULL, NULL);

  if (! IPUZ_CELL_IS_NORMAL (cell))
    return state;

  if (state->selected_cells == NULL)
    state->selected_cells = cell_array_new ();

  cell_array_toggle (state->selected_cells, coord);

  return state;
}

static void
select_region (GridState    *state,
               IpuzCellCoord  start,
               IpuzCellCoord  end,
               gboolean       select_area)
{
  guint row_start, row_end;
  guint col_start, col_end;

  row_start = MIN (start.row, end.row);
  row_end = MAX (start.row, end.row);
  col_start = MIN (start.column, end.column);
  col_end = MAX (start.column, end.column);

  for (guint row = row_start; row <= row_end; row++)
    {
      for (guint col = col_start; col <= col_end; col++)
        {
          IpuzCellCoord coord = {
            .row = row,
            .column = col
          };

          if (select_area)
            {
              cell_array_add (state->selected_cells, coord);
            }
          else
            {
              cell_array_remove (state->selected_cells, coord);
            }
        }
    }
}

GridState *
grid_state_select_drag_start (GridState         *state,
                              IpuzCellCoord      anchor_coord,
                              IpuzCellCoord      new_coord,
                              GridSelectionMode  mode)
{
  IpuzCell *cell;

  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  state = grid_state_clone (state);

  state->anchor = anchor_coord;
  state->anchor_set = TRUE;

  //  if (! GRID_STATE_HAS_SELECTABLE (state))
  //    return state;

  g_clear_pointer (&state->saved_selection, cell_array_unref);

  if (state->selected_cells == NULL)
    {
      state->selected_cells = cell_array_new ();
      state->saved_selection = cell_array_new ();
    }
  else
    {
      if (mode == GRID_SELECTION_SELECT)
        {
          state->saved_selection = cell_array_new ();
          g_clear_pointer (&state->selected_cells, cell_array_unref);
          state->selected_cells = cell_array_new ();
        }
      else
        {
          state->saved_selection = cell_array_copy (state->selected_cells);
        }
    }

  cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &state->anchor);
  g_return_val_if_fail (cell != NULL, NULL);

  if (! IPUZ_CELL_IS_NORMAL (cell))
    return state;

  switch (mode)
    {
    case GRID_SELECTION_NONE:
      break;
    case GRID_SELECTION_EXTEND:
      cell_array_add (state->selected_cells, state->anchor);
      break;
    case GRID_SELECTION_TOGGLE:
      cell_array_toggle (state->selected_cells, state->anchor);
      break;
    case GRID_SELECTION_SELECT:
      select_region (state, state->anchor, new_coord, TRUE);
      break;
    default:
      g_assert_not_reached ();
    }

  return state;
}

GridState *
grid_state_select_drag_update (GridState         *state,
                               IpuzCellCoord      anchor_coord,
                               IpuzCellCoord      extention_coord,
                               GridSelectionMode  mode)
{
  gboolean select_area = TRUE;

  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  state = grid_state_clone (state);

  if (! GRID_STATE_HAS_SELECTABLE (state))
    return state;

  state->anchor = anchor_coord;
  state->anchor_set = TRUE;
  state->cursor = extention_coord;

  g_clear_pointer (&state->selected_cells, cell_array_unref);
  state->selected_cells = cell_array_copy (state->saved_selection);
  /* When toggle_selection is set, we don't blindly toggle each
   * cell. Instead, We use the anchor cell to determine what we want
   * to do. If the anchor cell is selected, then we deselect the whole
   * area. otherwise we select everything. */
  if (mode == GRID_SELECTION_TOGGLE &&
      cell_array_find (state->saved_selection, state->anchor, NULL))
    select_area = FALSE;

  select_region (state, state->anchor, extention_coord, select_area);

  return state;
}

GridState *
grid_state_select_drag_end (GridState     *state,
                            IpuzCellCoord  anchor_coord)
{
  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  state = grid_state_clone (state);

  g_clear_pointer (&state->saved_selection, cell_array_unref);
  state->anchor = anchor_coord;
  state->anchor_set = TRUE;
  return state;
}

GridState *
grid_state_select_all (GridState *state)
{
  guint rows, columns;

  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  state = grid_state_clone (state);

  if (! GRID_STATE_HAS_SELECTABLE (state))
    return state;

  g_clear_pointer (&state->saved_selection, cell_array_unref);
  cell_array_set_size (state->selected_cells, 0);

  rows = ipuz_grid_get_height (IPUZ_GRID (state->xword));
  columns = ipuz_grid_get_width (IPUZ_GRID (state->xword));
  for (guint row = 0; row < rows; row++)
    {
      for (guint column = 0; column < columns; column++)
        {
          IpuzCell *cell;
          IpuzCellCoord coord = {
            .row = row,
            .column = column
          };

          cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &coord);
          if (IPUZ_CELL_IS_NORMAL (cell))
            cell_array_add (state->selected_cells, coord);
        }
    }

  return state;
}

GridState *
grid_state_select_none (GridState *state)
{
  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  state = grid_state_clone (state);

  if (! GRID_STATE_HAS_SELECTABLE (state))
    return state;

  g_clear_pointer (&state->saved_selection, cell_array_unref);
  if (state->selected_cells)
    cell_array_set_size (state->selected_cells, 0);

  return state;
}

GridState *
grid_state_select_invert (GridState *state)
{
  guint rows, columns;
  CellArray *new_selected_cells;

  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  state = grid_state_clone (state);

  if (! GRID_STATE_HAS_SELECTABLE (state))
    return state;

  g_clear_pointer (&state->saved_selection, cell_array_unref);
  new_selected_cells = cell_array_new ();

  rows = ipuz_grid_get_height (IPUZ_GRID (state->xword));
  columns = ipuz_grid_get_width (IPUZ_GRID (state->xword));
  for (guint row = 0; row < rows; row++)
    {
      for (guint column = 0; column < columns; column++)
        {
          IpuzCell *cell;
          IpuzCellCoord coord = {
            .row = row,
            .column = column
          };

          cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &coord);
          if (! IPUZ_CELL_IS_NORMAL (cell))
            continue;
          if (! cell_array_find (state->selected_cells, coord, NULL))
            cell_array_add (new_selected_cells, coord);
        }
    }

  cell_array_unref (state->selected_cells);
  state->selected_cells = new_selected_cells;

  return state;
}

GridState *
grid_state_select_toggle_block (GridState *state)
{
  IpuzCellType cell_type = IPUZ_CELL_NORMAL;
  IpuzSymmetry symmetry;

  state = grid_state_clone (state);

  g_return_val_if_fail (state != NULL, state);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), state);

  symmetry = crosswords_quirks_get_symmetry (state->quirks);

  /* Go through the cells checking to see if there are any normal
   * cells.
   */
  for (guint i = 0; i < cell_array_len (state->selected_cells); i++)
    {
      IpuzCellCoord coord;
      IpuzCell *cell;

      coord = cell_array_index (state->selected_cells, i);
      cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &coord);

      if (IPUZ_CELL_IS_NORMAL (cell))
        {
          cell_type = IPUZ_CELL_BLOCK;
          break;
        }
    }

  /* Go back through the cells to set them to the correct cell_type */
  for (guint i = 0; i < cell_array_len (state->selected_cells); i++)
    {
      IpuzCellCoord coord;
      IpuzCell *cell;

      coord = cell_array_index (state->selected_cells, i);
      cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &coord);

      ipuz_cell_set_cell_type (cell, cell_type);
    }

  /* And fix it all up */
  ipuz_crossword_fix_all (state->xword,
                          "symmetry", symmetry,
                          "symmetry-coords", state->selected_cells,
                          NULL);

  return state;
}


GridState *
grid_state_set_guesses (GridState   *state,
                        IpuzGuesses *guesses)
{
  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  state = grid_state_clone (state);
  ipuz_grid_set_guesses (IPUZ_GRID (state->xword), guesses);

  return state;
}

GridState *
grid_state_set_reveal_mode (GridState      *state,
                            GridRevealMode  reveal_mode)
{
  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  state = grid_state_clone (state);
  state->reveal_mode = reveal_mode;
  return state;
}

GridState *
grid_state_set_bars (GridState     *state,
                     IpuzCellCoord   coord,
                     IpuzStyleSides  bars,
                     gboolean        update_cursor)
{
  IpuzSymmetry symmetry = IPUZ_SYMMETRY_NONE;
  g_autoptr (GArray) coords = NULL;

  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);
  g_return_val_if_fail (IPUZ_IS_BARRED (state->xword), NULL);

  state = grid_state_clone (state);

  if (state->quirks)
    symmetry = crosswords_quirks_get_symmetry (state->quirks);

  /* Change the cell type */
  coords = g_array_new (FALSE, FALSE, sizeof (IpuzCellCoord));
  g_array_append_val (coords, coord);

  ipuz_barred_set_cell_bars (IPUZ_BARRED (state->xword), &coord, bars);
  ipuz_crossword_fix_all (state->xword,
                          "symmetry", symmetry,
                          "symmetry-coords", coords,
                          NULL);

  if (update_cursor)
    {
      IpuzCell *cell;
      const IpuzClue *clue;

      cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &coord);
      clue = ipuz_cell_get_clue (cell, state->clue.direction);

      if (clue == NULL)
        clue = ipuz_cell_get_clue (cell, IPUZ_CLUE_DIRECTION_ACROSS);
      if (clue == NULL)
        clue = ipuz_cell_get_clue (cell, IPUZ_CLUE_DIRECTION_DOWN);

      state->cursor = coord;
      ipuz_clues_get_id_by_clue (IPUZ_CLUES (state->xword), clue, &state->clue);
    }

  return state;
}

/**
 * grid_state_ensure_lone_style:
 * @coord: The coordinate of the cell to set a style on.
 *
 * This function will make sure that the cell at @coord has an
 * `IpuzStyle`. If it's a global style, it will make a copy. If
 * there's no style at all on the cell, this will attach a blank
 * style. The end result is a cell with a style attached to it that
 * can be edited without affecting any other cell.
 *
 * Returns: A new `GridState` with the new state.  Remember to free the old one.
 **/
GridState *
grid_state_ensure_lone_style (GridState     *state,
                              IpuzCellCoord  coord)
{
  IpuzCell *cell;
  IpuzStyle *style;

  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  state = grid_state_clone (state);

  cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &coord);
  style = ipuz_cell_get_style (cell);

  if (style)
    {
      if (ipuz_style_get_style_name (style) == NULL)
        return state;
      style = ipuz_style_copy (style);
      ipuz_style_set_style_name (style, NULL);
    }
  else
    {
      style = ipuz_style_new ();
    }

  ipuz_cell_set_style (cell, style, NULL);
  ipuz_style_unref (style);

  return state;
}

/**
 * grid_state_sync_to_coord:
 * @coord: Coordinate to sync to
 *
 * Sets the cursor and clue to coord as appropriate. This can be used
 * after a selection or to sync the cursor between two grids. It does
 * a best effort job or finding a new cell.
 *
 * Returns: A new `GridState` with the new state.  Remember to free the old one.
 **/
GridState *
grid_state_sync_to_coord (GridState         *state,
                          IpuzCellCoord      coord,
                          IpuzClueDirection  preferred_direction)
{
  IpuzCell *cell;
  const IpuzClue *new_clue = NULL;

  g_return_val_if_fail (state != NULL, NULL);
  g_return_val_if_fail (! GRID_STATE_DEHYDRATED (state), NULL);

  state = grid_state_clone (state);

  cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &coord);
  g_return_val_if_fail (cell != NULL, NULL);

  /* If you're normal-only, nothing happens if you click on a NULL or
   * BLOCK cell */
  if (GRID_STATE_HAS_NORMAL_ONLY (state) &&
      ! IPUZ_CELL_IS_NORMAL (cell))
    return state;

  state->cursor = coord;

  if (GRID_STATE_HAS_SELECTABLE (state))
    return state;

  state->cursor = coord;

  /* Try and find a clue that has this cell. We start with the
   * preferred direction, then switch, then look through the rest of
   * the directions. */
  new_clue = ipuz_cell_get_clue (cell, preferred_direction);
  if (new_clue == NULL)
    new_clue = ipuz_cell_get_clue (cell,
                                   ipuz_clue_direction_switch (preferred_direction));

  if (new_clue == NULL)
    {
      /* No current direction, so pick any that is available */
      for (guint i = 0; i < ipuz_clues_get_n_clue_sets (IPUZ_CLUES (state->xword)); i++)
        {
          IpuzClueDirection dir;
          dir = ipuz_clues_clue_set_get_dir (IPUZ_CLUES (state->xword), i);
          new_clue = ipuz_cell_get_clue (cell, dir);
          if (new_clue)
            break;
        }
    }

  if (new_clue)
    ipuz_clues_get_id_by_clue (IPUZ_CLUES (state->xword), new_clue, &state->clue);
  else
    state->clue.direction = IPUZ_CLUE_DIRECTION_NONE;

  return state;
}

void
grid_state_assert_equal (const GridState *a,
                         const GridState *b)
{
  g_assert_cmpint (a->clue.direction, ==, b->clue.direction);
  g_assert_cmpint (a->clue.index,     ==, b->clue.index);
  g_assert_cmpint (a->cursor.row,     ==, b->cursor.row);
  g_assert_cmpint (a->cursor.column,  ==, b->cursor.column);
  g_assert_cmpint (a->reveal_mode,    ==, b->reveal_mode);
  g_assert_cmpint (a->behavior,       ==, b->behavior);
  g_assert (a->xword == b->xword);
  g_assert (a->quirks == b->quirks);
}

void
grid_state_print (const GridState *state,
                  gboolean          print_members)
{
  g_return_if_fail (state != NULL);

  g_print ("GridState (%p)\n", state);
  g_print ("\txword: (%p)\n", state->xword);
  g_print ("\tcursor: (%u, %u)\n", state->cursor.row, state->cursor.column);
  g_print ("\tclue: (%s, %u)\n",
           ipuz_clue_direction_to_string (state->clue.direction),
           state->clue.index);
  g_print ("\treveal_mode: ");
  switch (state->reveal_mode)
    {
    case GRID_REVEAL_NONE:
      g_print ("NONE\n");
      break;
    case GRID_REVEAL_ERRORS_BOARD:
      g_print ("ERRORS_BOARD\n");
      break;
    case GRID_REVEAL_ERRORS_CLUE:
      g_print ("ERRORS_CLUE\n");
      break;
    case GRID_REVEAL_ERRORS_CELL:
      g_print ("ERRORS_CELL\n");
      break;
    case GRID_REVEAL_ALL:
      g_print ("ALL\n");
      break;
    }
  g_print ("\tselected_cells: %p\n", state->selected_cells);
  cell_array_print (state->selected_cells);

  g_print ("\tsaved_selection: %p\n", state->saved_selection);
  cell_array_print (state->saved_selection);
  g_print ("\tquirks: %p\n", state->quirks);

  if (print_members && state->xword)
    ipuz_crossword_print (state->xword);
}

gboolean
grid_state_command_destructive (GridState   *state,
                                GridCmdKind  kind)
{
  IpuzCell *cell;

  if (! CMD_KIND_DESTRUCTIVE (kind))
    return FALSE;

  if (! GRID_STATE_HAS_USE_CURSOR (state))
    return FALSE;

  cell = ipuz_grid_get_cell (IPUZ_GRID (state->xword), &state->cursor);
  if (! IPUZ_CELL_IS_NORMAL (cell))
    return FALSE;

  if (GRID_STATE_HAS_SHOW_GUESS (state))
    {
      IpuzGuesses *guesses;

      guesses = ipuz_grid_get_guesses (IPUZ_GRID (state->xword));
      return ipuz_guesses_get_guess (guesses, &state->cursor) != NULL;
    }
  else
    {
      return ipuz_cell_get_solution (cell) != NULL;
    }
}
