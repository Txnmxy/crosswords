#pragma once

void layout_sets_main_text_for_solve_mode (void);
void layout_sets_main_text_for_browse_mode (void);
void layout_sets_main_text_for_edit_mode (void);
void layout_sets_main_text_for_edit_browse_mode (void);
void layout_sets_main_text_for_select_mode (void);
void layout_sets_main_text_for_view_mode (void);
void layout_sets_main_text_to_initial_val (void);

void layout_loads_barred (void);
