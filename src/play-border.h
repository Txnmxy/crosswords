/* play-border.h - Border items for PlayGrid.
 *
 * Copyright 2021 Federico Mena Quintero <federico@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>
#include "grid-layout.h"

G_BEGIN_DECLS

#define PLAY_TYPE_BORDER (play_border_get_type ())

G_DECLARE_FINAL_TYPE (PlayBorder, play_border, PLAY, BORDER, GtkWidget)

typedef enum
{
  PLAY_BORDER_KIND_INTERSECTION,
  PLAY_BORDER_KIND_HORIZONTAL,
  PLAY_BORDER_KIND_VERTICAL,
} PlayBorderKind;


GtkWidget *play_border_new           (PlayBorderKind         kind,
                                      LayoutConfig           config);
void       play_border_update_state  (PlayBorder            *border,
                                      gboolean               filled,
                                      LayoutItemBorderStyle  css_style,
                                      gboolean               bg_color_set,
                                      GdkRGBA                bg_color);
void       play_border_update_config (PlayBorder           *border,
                                      LayoutConfig          config);


G_END_DECLS





































































































































































































































































































































































