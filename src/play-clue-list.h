/* play-clue-list.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>
#include "grid-state.h"
#include "grid-layout.h"
#include "clue-grid.h"

G_BEGIN_DECLS


typedef enum
{
  PLAY_CLUE_LIST_CLUES,
  PLAY_CLUE_LIST_EDIT,
} PlayClueListMode;


#define PLAY_TYPE_CLUE_LIST (play_clue_list_get_type())
G_DECLARE_FINAL_TYPE (PlayClueList, play_clue_list, PLAY, CLUE_LIST, GtkWidget);


GtkWidget *play_clue_list_new                   (void);
void       play_clue_list_set_header_size_group (PlayClueList      *clue_list,
                                                 GtkSizeGroup      *size_group);
void       play_clue_list_set_clue_range        (PlayClueList      *clue_list,
                                                 IpuzClueDirection  direction,
                                                 gint               start,
                                                 gint               end);
void       play_clue_list_update                (PlayClueList      *clue_list,
                                                 GridState         *state,
                                                 ZoomLevel          zoom_level);
GtkWidget *play_clue_list_get_selected_grid     (PlayClueList      *clue_list);



G_END_DECLS
