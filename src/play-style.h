/* play-style.h
  *
  * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *
  * SPDX-License-Identifier: GPL-3.0-or-later
  */


#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>


G_BEGIN_DECLS

/* The first sixteen colors form a color palette for puzzles to refer
 * to by number. They aren't defined by the spec, so we give a good
 * assortment of colors to match.
 *
 * These change values between dark and light themes. */
typedef enum
{
  PLAY_COLOR_BLACK = 0,
  PLAY_COLOR_WHITE = 1,
  PLAY_COLOR_BLUE1 = 2,
  PLAY_COLOR_GREEN1 = 3,
  PLAY_COLOR_YELLOW1 = 4,
  PLAY_COLOR_ORANGE1 = 5,
  PLAY_COLOR_RED1 = 6,
  PLAY_COLOR_PURPLE1 = 7,
  PLAY_COLOR_BROWN1 = 8,
  PLAY_COLOR_BLUE3 = 9,
  PLAY_COLOR_GREEN3 = 10,
  PLAY_COLOR_YELLOW3 = 11,
  PLAY_COLOR_ORANGE3 = 12,
  PLAY_COLOR_RED3 = 13,
  PLAY_COLOR_PURPLE3 = 14,
  PLAY_COLOR_BROWN3 = 15,
  PLAY_COLOR_BLOCK,
  PLAY_COLOR_NORMAL,
  PLAY_COLOR_HIGHLIGHT,
  PLAY_COLOR_FOCUS,
  PLAY_N_COLORS,
} PlayColor;

#define PLAY_COLOR_PALLETTE_SIZE 16

#define DEFAULT_STYLE (play_style_get ())
#define PLAY_TYPE_STYLE (play_style_get_type())
G_DECLARE_FINAL_TYPE (PlayStyle, play_style, PLAY, STYLE, GObject);


PlayStyle     *play_style_get             (void);
const GdkRGBA *play_style_get_color       (PlayStyle *style,
                                           PlayColor  color);
float          play_style_color_luminance (GdkRGBA   *color);


void darken_color  (GdkRGBA *color);
void lighten_color (GdkRGBA *color);
#define get_color(color) play_style_get_color (DEFAULT_STYLE, PLAY_COLOR_##color)

G_END_DECLS
