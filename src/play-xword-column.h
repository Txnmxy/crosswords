/* play-xword-column.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS


#define PLAY_TYPE_XWORD_COLUMN (play_xword_column_get_type())
G_DECLARE_FINAL_TYPE (PlayXwordColumn, play_xword_column, PLAY, XWORD_COLUMN, GtkWidget);


void     play_xword_column_set_skip_next_anim    (PlayXwordColumn *xword_column,
                                                  gboolean         skip_next_anim);
gboolean play_xword_column_get_clue_visible      (PlayXwordColumn *xword_column);
gboolean play_xword_column_get_primary_visible   (PlayXwordColumn *xword_column);
gboolean play_xword_column_get_secondary_visible (PlayXwordColumn *xword_column);


G_END_DECLS
