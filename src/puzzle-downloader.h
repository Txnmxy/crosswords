/* puzzle-downloader.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <gtk/gtk.h>
#include "puzzle-set-config.h"


G_BEGIN_DECLS


typedef enum
{
  DOWNLOADER_STATE_DISABLED,
  DOWNLOADER_STATE_WAITING_FOR_NETWORK,
  DOWNLOADER_STATE_DOWNLOADING,
  DOWNLOADER_STATE_READY,
} DownloaderState;


typedef enum
{
  DOWNLOADER_FORMAT_UNKNOWN,
  DOWNLOADER_FORMAT_IPUZ,
  DOWNLOADER_FORMAT_PUZ,
  DOWNLOADER_FORMAT_JPZ,
  DOWNLOADER_FORMAT_XD
} DownloaderFormat;

#define PUZZLE_TYPE_DOWNLOADER (puzzle_downloader_get_type())
G_DECLARE_FINAL_TYPE (PuzzleDownloader, puzzle_downloader, PUZZLE, DOWNLOADER, GObject);


PuzzleDownloader *puzzle_downloader_new_filechooser      (void);
PuzzleDownloader *puzzle_downloader_new_from_config      (PuzzleSetConfig   *config);
void              puzzle_downloader_add_import_target    (PuzzleDownloader  *downloader,
                                                          const gchar       *uri);
gboolean          puzzle_downloader_get_requires_network (PuzzleDownloader  *downloader);

void              puzzle_downloader_run_async            (PuzzleDownloader  *downloader,
                                                          GtkWindow         *parent_window,
                                                          GCancellable      *cancellable,
                                                          GError           **error);


G_END_DECLS


