/* puzzle-set-list.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include "puzzle-set.h"
#include "puzzle-set-list.h"
#include "crosswords-misc.h"

static GListModel *global_puzzle_sets = NULL;
static GSettings *global_settings = NULL;



struct _PuzzleSetFilter
{
  GtkFilter parent_object;
  PuzzleSetFilterType type;
  GSettings *settings;
  gulong show_all_languages_handler;
  gulong shown_puzzle_sets_handler;
};


static void           puzzle_set_filter_init           (PuzzleSetFilter      *self);
static void           puzzle_set_filter_class_init     (PuzzleSetFilterClass *klass);
static void           puzzle_set_filter_dispose        (GObject               *object);
static gboolean       puzzle_set_filter_match          (GtkFilter             *filter,
                                                        gpointer               item);
static GtkFilterMatch puzzle_set_filter_get_strictness (GtkFilter             *filter);


G_DEFINE_TYPE (PuzzleSetFilter, puzzle_set_filter, GTK_TYPE_FILTER);


static void
puzzle_set_filter_init (PuzzleSetFilter *self)
{
  /* Pass */
}

static void
puzzle_set_filter_class_init (PuzzleSetFilterClass *klass)
{
  GObjectClass *object_class;
  GtkFilterClass *filter_class;

  object_class = G_OBJECT_CLASS (klass);
  filter_class = GTK_FILTER_CLASS (klass);

  object_class->dispose = puzzle_set_filter_dispose;
  filter_class->match = puzzle_set_filter_match;
  filter_class->get_strictness = puzzle_set_filter_get_strictness;

}

static void
puzzle_set_filter_dispose (GObject *object)
{
  PuzzleSetFilter *self;

  self = PUZZLE_SET_FILTER (object);

  g_clear_signal_handler (&self->show_all_languages_handler,
                          self->settings);
  g_clear_signal_handler (&self->shown_puzzle_sets_handler,
                          self->settings);
  g_clear_object (&self->settings);

  G_OBJECT_CLASS (puzzle_set_filter_parent_class)->dispose (object);
}


static void
show_all_languages_changed_cb (PuzzleSetFilter *self)
{
  gboolean show_all_languages;

  show_all_languages = g_settings_get_boolean (self->settings, "show-all-languages");

  gtk_filter_changed (GTK_FILTER (self),
                      show_all_languages?GTK_FILTER_CHANGE_LESS_STRICT:GTK_FILTER_CHANGE_MORE_STRICT);
}

/* Note: This is called twice when we change the shown-puzzle-sets
 * setting. Filtering twice is probably okay as it's stable.
 */
static void
shown_puzzle_sets_changed_cb (PuzzleSetFilter *self)
{
  gtk_filter_changed (GTK_FILTER (self), GTK_FILTER_CHANGE_DIFFERENT);
}

static gboolean
match_language (PuzzleSet *puzzle_set,
                GSettings *settings)
{
  const gchar *const *languages;
  const gchar *puzzle_set_language;
  gboolean show_all_languages;

  show_all_languages = g_settings_get_boolean (settings, "show-all-languages");
  if (show_all_languages)
    return TRUE;

  languages = g_get_language_names ();
  puzzle_set_language = puzzle_set_get_language (puzzle_set);

  for (int i = 0; languages[i]; i++)
    {
      if (g_strcmp0 (languages[i], puzzle_set_language) == 0)
        return TRUE;
    }

  return FALSE;
}

static gboolean
match_special (PuzzleSet *puzzle_set)
{
  const gchar *id;

  id  = puzzle_set_get_id (puzzle_set);

  if (strncmp (id, "devel-", strlen ("devel-")) == 0)
    return TRUE;

  if (strcmp (id, "uri") == 0)
    return TRUE;

  if (strcmp (id, "add-puzzle-sets") == 0)
    return TRUE;

  return FALSE;
}

static gboolean
match_user_shown (PuzzleSet *puzzle_set,
                  GSettings *settings)
{
  const gchar *id;
  g_autoptr (GVariant) show_list = NULL;

  id  = puzzle_set_get_id (puzzle_set);

  show_list = g_settings_get_value (settings, "shown-puzzle-sets");
  for (guint i = 0; i < g_variant_n_children (show_list); i++)
    {
      g_autoptr (GVariant) child = NULL;

      child = g_variant_get_child_value (show_list, i);
      if (g_strcmp0 (id, g_variant_get_string (child, NULL)) == 0)
        return TRUE;
    }

  return FALSE;
}

static gboolean
puzzle_set_filter_match (GtkFilter *filter,
                         gpointer   item)
{
  PuzzleSetFilter *self;
  PuzzleSet *puzzle_set;

  self = PUZZLE_SET_FILTER (filter);
  puzzle_set = PUZZLE_SET (item);

  /* We always skip puzzle_sets that declare themselves disabled */
  if (puzzle_set_get_disabled (puzzle_set))
    return FALSE;

  /* always skip devel if we're a non-devel build */
#ifndef DEVELOPMENT_BUILD
  if (strncmp (puzzle_set_get_id (puzzle_set), "devel-", strlen ("devel-")) == 0)
    return FALSE;
#endif

  if (match_special (puzzle_set))
    {
      if (self->type & PUZZLE_SET_FILTER_SPECIAL)
        return TRUE;
      return FALSE;
    }

  if (self->type & PUZZLE_SET_FILTER_LANGUAGE)
    {
      if (match_language (puzzle_set, self->settings))
        return TRUE;
    }

  if (self->type & PUZZLE_SET_FILTER_USER_SHOWN)
    {
      if (match_user_shown (puzzle_set, self->settings))
        return TRUE;
    }

  return FALSE;
}

static GtkFilterMatch
puzzle_set_filter_get_strictness (GtkFilter *filter)
{
  return GTK_FILTER_MATCH_SOME;
}

/* Public Functions */

GtkFilter *
puzzle_set_filter_new (PuzzleSetFilterType  type,
                       GSettings           *settings)
{
  PuzzleSetFilter *filter;

  filter = g_object_new (PUZZLE_TYPE_SET_FILTER, NULL);

  filter->type = type;
  filter->settings = g_object_ref (settings);
  filter->show_all_languages_handler =
    g_signal_connect_swapped (filter->settings,
                              "changed::show-all-languages",
                              G_CALLBACK (show_all_languages_changed_cb),
                              filter);

  filter->shown_puzzle_sets_handler =
    g_signal_connect_swapped (filter->settings,
                              "changed::shown-puzzle-sets",
                              G_CALLBACK (shown_puzzle_sets_changed_cb),
                              filter);

  return (GtkFilter *) filter;
}

/* The puzzle Set List */
   
static void
crosswords_load_puzzle_sets_from_dir (const gchar *dir_name,
                                      GListModel  *list)
{
  const char *name;
  GDir *data_dir;

  data_dir = g_dir_open (dir_name, 0, NULL);
  /* Ignore directories that don't exist. This is fine, because we're
   * walking through $XDG_DATA_DIRS */
  if (data_dir == NULL)
    return;

  while ((name = g_dir_read_name (data_dir)) != NULL)
    {
      g_autoptr (GError) err = NULL;
      g_autofree gchar *file_name = NULL;
      GResource *resource;
      PuzzleSet *puzzle_set;

      file_name = g_build_filename (dir_name, name, NULL);
      /* Recurse to subdirectories */
      /* FIXME(cleanup): Should we look for loops? */
      if (g_file_test (file_name, G_FILE_TEST_IS_DIR))
        {
          crosswords_load_puzzle_sets_from_dir (file_name, list);
          continue;
        }

      if (! g_str_has_suffix (name, ".gresource"))
        continue;

      resource = g_resource_load (file_name, &err);
      if (err)

        {
          g_warning ("Failed to load resource %s\nError %s", file_name, err->message);
          continue;
        }

      puzzle_set = puzzle_set_new (resource);
      g_resource_unref (resource);

      /* Can we create a meaningful puzzle_set from this resource? */
      if (puzzle_set == NULL)
        continue;

      g_list_store_append (G_LIST_STORE (list), puzzle_set);
    }
  g_dir_close (data_dir);
}

static gint
crosswords_list_store_sort (PuzzleSet *a,
                            PuzzleSet *b,
                            gpointer   user_data)
{
  const gchar *a_short_name;
  const gchar *b_short_name;
  const gchar *a_id;
  const gchar *b_id;

  a_short_name = puzzle_set_get_short_name (PUZZLE_SET (a));
  b_short_name = puzzle_set_get_short_name (PUZZLE_SET (b));
  a_id = puzzle_set_get_id (PUZZLE_SET (a));
  b_id = puzzle_set_get_id (PUZZLE_SET (b));

  /* Always make sure the "add-puzzle-sets" resource is sorted last */
  if (g_strcmp0 (a_id, "add-puzzle-sets") == 0)
    return 1;
  if (g_strcmp0 (b_id, "add-puzzle-sets") == 0)
    return -1;

  /* Always make sure the "uri" resource is sorted second last */
  if (g_strcmp0 (a_id, "uri") == 0)
    return 1;
  if (g_strcmp0 (b_id, "uri") == 0)
    return -1;

  return g_strcmp0 (a_short_name, b_short_name);
}


void
puzzle_set_list_init (void)
{
  const gchar *const *datadirs;
  const gchar *PUZZLE_SET_PATH = NULL;

  if (global_puzzle_sets != NULL)
    return;

  global_puzzle_sets = (GListModel *) g_list_store_new (PUZZLE_TYPE_SET);

  datadirs = g_get_system_data_dirs ();
  for (guint i = 0; datadirs[i] != NULL; i++)
    {
      g_autofree gchar *pathname = NULL;

      pathname = g_build_filename (datadirs[i], "crosswords/puzzle-sets", NULL);
      crosswords_load_puzzle_sets_from_dir (pathname, global_puzzle_sets);
    }

  datadirs = g_get_system_data_dirs ();
  for (guint i = 0; datadirs[i] != NULL; i++)
    {
      g_autofree gchar *pathname = NULL;

      pathname = g_build_filename (datadirs[i], "crosswords/extensions/puzzle-sets", NULL);
      crosswords_load_puzzle_sets_from_dir (pathname, global_puzzle_sets);
    }

  if (g_get_user_data_dir ())
    {
      g_autofree gchar *pathname = NULL;

      pathname = g_build_filename (g_get_user_data_dir (), "crosswords/puzzle-sets", NULL);
      crosswords_load_puzzle_sets_from_dir (pathname, global_puzzle_sets);
    }

  /* We also look in $PUZZLE_SET_PATH for puzzles. This is mostly for
   * running out of the builddir with a local install. It is set in
   * the 'run' script in this case.  */
  PUZZLE_SET_PATH = g_getenv ("PUZZLE_SET_PATH");
  if (PUZZLE_SET_PATH && PUZZLE_SET_PATH[0])
    {
      g_auto (GStrv) puzzle_set_paths = NULL;

      puzzle_set_paths = g_strsplit (PUZZLE_SET_PATH, ":", -1);
      for (guint i = 0; puzzle_set_paths[i]; i++)
        crosswords_load_puzzle_sets_from_dir (puzzle_set_paths[i], global_puzzle_sets);
    }

  if (g_list_model_get_n_items (global_puzzle_sets) == 0)
    {
      /* We should always have the "uri" puzzle-set at a minimum, and
       * we won't work without it. This implies a broken
       * installation. Erroring out. */
      g_warning (_("No puzzle sets were found. Use $PUZZLE_SET_PATH to set a directory to search."));
      g_warning (_("When running out of a local build, use the `run` script to set the environment correctly."));
      g_assert_not_reached ();
    }

  g_list_store_sort (G_LIST_STORE (global_puzzle_sets),
                     (GCompareDataFunc) crosswords_list_store_sort,
                     NULL);
}


static void
shown_puzzle_set_changed (void)
{
  GVariantBuilder builder;
  g_autoptr (GHashTable) show_hash = NULL;
  g_autoptr (GVariant) show_list = NULL;

  show_list = g_settings_get_value (global_settings, "shown-puzzle-sets");
  show_hash = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);
  g_variant_builder_init (&builder, G_VARIANT_TYPE_ARRAY);

  for (guint i = 0; i < g_variant_n_children (show_list); i++)
    {
      g_autoptr (GVariant) child = NULL;

      child = g_variant_get_child_value (show_list, i);
      g_hash_table_insert (show_hash, g_variant_dup_string (child, NULL), NULL);
    }

  for (guint i = 0; i < g_list_model_get_n_items (global_puzzle_sets); i++)
    {
      PuzzleSet *puzzle_set = g_list_model_get_item (global_puzzle_sets, i);
      gboolean shown;

      shown = g_hash_table_contains (show_hash, puzzle_set_get_id (puzzle_set));

      puzzle_set_set_shown (puzzle_set, shown);
    }
}

void
puzzle_set_settings_init (void)
{
  global_settings = g_settings_new ("org.gnome.Crosswords");

  shown_puzzle_set_changed ();
  g_signal_connect (global_settings,
                    "changed::shown-puzzle-sets",
                    G_CALLBACK (shown_puzzle_set_changed),
                    NULL);
}

GListModel *
puzzle_set_list_get (void)
{
  if (global_puzzle_sets == NULL)
    {
      /* this will assert on failure */
      puzzle_set_list_init ();
      puzzle_set_settings_init ();
    }

  return global_puzzle_sets;
}
