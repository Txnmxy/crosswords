/* puzzle-set-list.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS


/* These are the types of puzzles to filter out */
typedef enum
{
  PUZZLE_SET_FILTER_LANGUAGE = 1 << 0,   /* language settings exclude puzzles */
  PUZZLE_SET_FILTER_SPECIAL = 1 << 1,    /* devel- and uri */
  PUZZLE_SET_FILTER_USER_SHOWN = 1 << 2, /* puzzles that haven't been selected to include */
} PuzzleSetFilterType;


#define PUZZLE_TYPE_SET_FILTER (puzzle_set_filter_get_type())
G_DECLARE_FINAL_TYPE (PuzzleSetFilter, puzzle_set_filter, PUZZLE, SET_FILTER, GtkFilter);

/* PuzzleSetModel */
GtkFilter *puzzle_set_filter_new (PuzzleSetFilterType  type,
                                  GSettings           *settings);


/* public functions */
GListModel *puzzle_set_list_get (void);


G_END_DECLS
