/* puzzle-set.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>

#include "puzzle-set-config.h"


G_BEGIN_DECLS

typedef enum
{
  PUZZLE_SET_TYPE_RESOURCE, /* @ Standard puzzle set @ */
  PUZZLE_SET_TYPE_TEST,     /* @ Placeholder for testing puzzle sets @ */
} PuzzleSetType;


typedef enum
{
  PUZZLE_PHASE_MAIN,
  PUZZLE_PHASE_PICKER,
  PUZZLE_PHASE_GAME,
} PuzzlePhase;

#define PUZZLE_TYPE_SET (puzzle_set_get_type())
G_DECLARE_DERIVABLE_TYPE (PuzzleSet, puzzle_set, PUZZLE, SET, GObject);


struct _PuzzleSetClass
{
  GObjectClass parent_class;

  /* Signals */
  void        (* puzzles_start)   (PuzzleSet   *puzzle_set);
  void        (* change_phase)    (PuzzleSet   *puzzle_set,
                                   PuzzlePhase  phase);
  void        (* puzzles_done)    (PuzzleSet   *puzzle_set);
  void        (* reveal_canceled) (PuzzleSet   *puzzle_set);
};

PuzzleSet     *puzzle_set_new               (GResource   *resource);
PuzzleSetType  puzzle_set_get_puzzle_type   (PuzzleSet   *puzzle_set);
const gchar   *puzzle_set_get_id            (PuzzleSet   *puzzle_set);
const gchar   *puzzle_set_get_short_name    (PuzzleSet   *puzzle_set);
const gchar   *puzzle_set_get_long_name     (PuzzleSet   *puzzle_set);
const gchar   *puzzle_set_get_locale        (PuzzleSet   *puzzle_set);
const gchar   *puzzle_set_get_language      (PuzzleSet   *puzzle_set);
gboolean       puzzle_set_get_disabled      (PuzzleSet   *puzzle_set);
gboolean       puzzle_set_get_shown         (PuzzleSet   *puzzle_set);
void           puzzle_set_set_shown         (PuzzleSet   *puzzle_set,
                                             gboolean     shown);
gboolean       puzzle_set_get_auto_download (PuzzleSet   *puzzle_set);
ConfigSetTags  puzzle_set_get_tags          (PuzzleSet   *puzzle_set);


void           puzzle_set_puzzles_start     (PuzzleSet   *puzzle_set);
void           puzzle_set_change_phase      (PuzzleSet   *puzzle_set,
                                             PuzzlePhase  phase);
void           puzzle_set_puzzles_done      (PuzzleSet   *puzzle_set);
void           puzzle_set_reveal_canceled   (PuzzleSet   *puzzle_set);


GtkWidget     *puzzle_set_get_widget        (PuzzleSet   *puzzle_set,
                                             PuzzlePhase  phase);
IpuzPuzzle    *puzzle_set_get_puzzle        (PuzzleSet   *puzzle_set,
                                             PuzzlePhase  phase);
const gchar   *puzzle_set_get_title         (PuzzleSet   *puzzle_set,
                                             PuzzlePhase  phase);
void           puzzle_set_reset_puzzle      (PuzzleSet   *puzzle_set);
void           puzzle_set_reveal_toggled    (PuzzleSet   *puzzle_set,
                                             gboolean     reveal);
void           puzzle_set_show_hint         (PuzzleSet   *puzzle_set);
const gchar   *puzzle_set_get_uri           (PuzzleSet   *puzzle_set,
                                             PuzzlePhase  phase);
void           puzzle_set_import_uri        (PuzzleSet   *puzzle_set,
                                             const gchar *uri);
guint          puzzle_set_get_n_puzzles     (PuzzleSet   *puzzle_set);
guint          puzzle_set_get_n_won         (PuzzleSet   *puzzle_set);


G_END_DECLS
