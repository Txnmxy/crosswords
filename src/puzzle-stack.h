/* puzzle-stack.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <glib-object.h>
#include <libipuz/libipuz.h>


G_BEGIN_DECLS


typedef enum _PuzzleStackType
{
  PUZZLE_STACK_PLAY,
  PUZZLE_STACK_EDIT,
} PuzzleStackType;

typedef enum _PuzzleStackStage
{
  EDIT_STAGE_GRID,     /* Fill in the grid with letters */
  EDIT_STAGE_CLUES,    /* Add clues to the puzzle */
  EDIT_STAGE_STYLE,    /* Customize the puzzle through any style changes*/
  EDIT_STAGE_METADATA, /* Set metadata for the puzzle */
} PuzzleStackStage;

typedef enum _PuzzleStackChangeType
{
  STACK_CHANGE_PUZZLE,
  STACK_CHANGE_GRID,
  STACK_CHANGE_STATE,
  STACK_CHANGE_METADATA,
  STACK_CHANGE_GUESS,
} PuzzleStackChangeType;

typedef enum _PuzzleStackOperation
{
  PUZZLE_STACK_OPERATION_NEW_FRAME,
  PUZZLE_STACK_OPERATION_UNDO,
  PUZZLE_STACK_OPERATION_REDO,
} PuzzleStackOperation;


#define PUZZLE_TYPE_STACK (puzzle_stack_get_type())
G_DECLARE_FINAL_TYPE (PuzzleStack, puzzle_stack, PUZZLE, STACK, GObject);


PuzzleStack          *puzzle_stack_new                 (PuzzleStackType        type,
                                                        IpuzPuzzle            *initial_puzzle);
void                  puzzle_stack_push_change         (PuzzleStack           *puzzle_stack,
                                                        PuzzleStackChangeType  change_type,
                                                        IpuzPuzzle            *puzzle);
void                  puzzle_stack_set_data            (PuzzleStack           *puzzle_stack,
                                                        const gchar           *data_hint,
                                                        gpointer               data,
                                                        GDestroyNotify         clear_func);
void                  puzzle_stack_set_saved           (PuzzleStack           *puzzle_stack);
gpointer              puzzle_stack_get_data            (PuzzleStack           *puzzle_stack,
                                                        const gchar           *data_hint);
gpointer              puzzle_stack_peek_next_data      (PuzzleStack           *puzzle_stack,
                                                        const gchar           *data_hint);
IpuzPuzzle           *puzzle_stack_get_puzzle          (PuzzleStack           *puzzle_stack);
IpuzGuesses          *puzzle_stack_get_guesses         (PuzzleStack           *puzzle_stack);
PuzzleStackChangeType puzzle_stack_get_change_type     (PuzzleStack           *puzzle_stack);
gboolean              puzzle_stack_can_undo            (PuzzleStack           *puzzle_stack);
gboolean              puzzle_stack_can_redo            (PuzzleStack           *puzzle_stack);
gboolean              puzzle_stack_get_unsaved_changes (PuzzleStack           *puzzle_stack);
gint64                puzzle_stack_get_last_save_time  (PuzzleStack           *puzzle_stack);
void                  puzzle_stack_undo                (PuzzleStack           *puzzle_stack);
void                  puzzle_stack_redo                (PuzzleStack           *puzzle_stack);

/* For debugging */
void                  puzzle_stack_dump                (PuzzleStack           *puzzle_stack);
void                  puzzle_stack_validate            (const char            *curframe,
                                                        PuzzleStack           *puzzle_stack);


G_END_DECLS
