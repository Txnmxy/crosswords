/* puzzle-task.h
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "puzzle-task.h"

typedef struct
{
  GDestroyNotify result_clear;
} PuzzleTaskPrivate;


static void puzzle_task_init (PuzzleTask *self);
static void puzzle_task_class_init (PuzzleTaskClass *klass);


G_DEFINE_TYPE_WITH_CODE (PuzzleTask, puzzle_task, G_TYPE_OBJECT, G_ADD_PRIVATE (PuzzleTask));


static void
puzzle_task_init (PuzzleTask *self)
{
  /* Pass */
}

static void
puzzle_task_class_init (PuzzleTaskClass *klass)
{
  /* Pass */
}

GDestroyNotify
puzzle_task_get_result_clear (PuzzleTask *self)
{
  PuzzleTaskPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_TASK (self), NULL);

  priv = puzzle_task_get_instance_private (self);

  return priv->result_clear;
}

void
puzzle_task_set_result_clear (PuzzleTask     *self,
                              GDestroyNotify  result_clear)
{
  PuzzleTaskPrivate *priv;

  g_return_if_fail (PUZZLE_IS_TASK (self));

  priv = puzzle_task_get_instance_private (self);

  priv->result_clear = result_clear;
}

void
puzzle_task_set_run_info (PuzzleTask *self,
                          gint       *count,
                          GMutex     *results_mutex,
                          GArray     *results)
{
  PuzzleTaskClass *klass;

  g_return_if_fail (PUZZLE_IS_TASK (self));

  klass = PUZZLE_TASK_GET_CLASS (self);

  g_assert (klass->set_run_info);

  klass->set_run_info (self, count, results_mutex, results);
}

void
puzzle_task_run (GTask        *task,
                 GObject      *source_object,
                 gpointer      task_data,
                 GCancellable *cancellable)
{
  PuzzleTaskClass *klass;

  g_return_if_fail (task != NULL);
  g_return_if_fail (PUZZLE_IS_TASK (task_data));

  klass = PUZZLE_TASK_GET_CLASS (task_data);

  g_assert (klass->run);

  klass->run (task, source_object, task_data, cancellable);
}
