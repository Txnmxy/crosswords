/* puzzle-task.h
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

G_BEGIN_DECLS


#define PUZZLE_TYPE_TASK (puzzle_task_get_type())
G_DECLARE_DERIVABLE_TYPE (PuzzleTask, puzzle_task, PUZZLE, TASK, GObject);

typedef struct _PuzzleTaskClass PuzzleTaskClass;
struct _PuzzleTaskClass
{
  GObjectClass parent_class;

  void (*set_run_info) (PuzzleTask   *self,
                        gint         *count,
                        GMutex       *results_mutex,
                        GArray       *results);
  void (*run)          (GTask        *task,
                        GObject      *source_object,
                        gpointer      task_data,
                        GCancellable *cancellable);
};

GDestroyNotify puzzle_task_get_result_clear (PuzzleTask     *self);
void           puzzle_task_set_result_clear (PuzzleTask     *self,
                                             GDestroyNotify  result_clear);
void           puzzle_task_set_run_info     (PuzzleTask     *self,
                                             gint           *count,
                                             GMutex         *results_mutex,
                                             GArray         *results);
void           puzzle_task_run              (GTask          *task,
                                             GObject        *source_object,
                                             gpointer        task_data,
                                             GCancellable   *cancellable);


G_END_DECLS
