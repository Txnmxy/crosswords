/* svg.h - Generate SVG documents from crosswords
 *
 * Copyright 2021 Federico Mena Quintero <federico@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <librsvg/rsvg.h>
#include "grid-layout.h"

G_BEGIN_DECLS

typedef enum {
  COLORING_LIGHT,
  COLORING_DARK,
} Coloring;

RsvgHandle *svg_handle_from_string              (GString    *string,
                                                 Coloring    coloring);
GString    *svg_from_layout                     (GridLayout *layout);
GString    *svg_overlays_from_layout            (GridLayout *layout);
RsvgHandle *svg_handle_new_from_layout          (GridLayout *layout,
                                                 Coloring    coloring);
RsvgHandle *svg_handle_new_overlays_from_layout (GridLayout *layout,
                                                 Coloring    coloring);


G_END_DECLS
