/* task-runner.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "task-runner.h"
#include "word-list-resources.h"


enum {
  FINISHED,
  N_SIGNALS
};

static guint obj_signals[N_SIGNALS] = { 0 };


struct _TaskRunner
{
  GObject parent_object;
  TaskRunnerState state;

  gulong resource_changed_handler;

  /* These are the only structures modifiable by tasks. They persist
   * for the life of the task. */
  gint count; /* Only use with g_atomic_int_* */
  GMutex results_mutex; /* For accessing results */
  GArray *results;

  /* These are set for each run and cleared when completed */
  GCancellable *cancellable;
  gboolean clear_on_finish; /* We've been reset, and no need to keep results around */
  GTask *task;

};


static void task_runner_init       (TaskRunner      *self);
static void task_runner_class_init (TaskRunnerClass *klass);
static void task_runner_dispose    (GObject         *object);
static void task_runner_finalize   (GObject         *object);
static void resource_changed_cb    (TaskRunner      *self);


G_DEFINE_TYPE (TaskRunner, task_runner, G_TYPE_OBJECT);


static void
task_runner_init (TaskRunner *self)
{
  self->state = TASK_RUNNER_READY;
  self->resource_changed_handler =
    g_signal_connect_swapped (WORD_LIST_RESOURCES_DEFAULT,
                              "resource-changed",
                              G_CALLBACK (resource_changed_cb),
                              self);

  g_mutex_init (&self->results_mutex);
}

static void
task_runner_class_init (TaskRunnerClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = task_runner_dispose;
  object_class->finalize = task_runner_finalize;

  obj_signals [FINISHED] =
    g_signal_new ("finished",
                  TASK_TYPE_RUNNER,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);
}

static void
task_runner_dispose (GObject *object)
{
  TaskRunner *self;

  self = TASK_RUNNER (object);

  /* FIXME(threading): If we're running, we will not dispose
   * correctly */
  g_clear_object (&self->task);
  g_clear_object (&self->cancellable);
  g_clear_signal_handler (&self->resource_changed_handler, WORD_LIST_RESOURCES_DEFAULT);

  G_OBJECT_CLASS (task_runner_parent_class)->dispose (object);
}

static void
task_runner_finalize (GObject *object)
{
  TaskRunner *self;

  self = TASK_RUNNER (object);

  g_mutex_clear (&self->results_mutex);
  if (self->results)
    {
      g_array_free (self->results, TRUE);
      self->results = NULL;
    }

  G_OBJECT_CLASS (task_runner_parent_class)->finalize (object);
}

/* We call this if we are running and someone happens to change the
 * word-list resource under us. It's safe — the task will never hit a
 * mainloop in its thread so its word-list will never swap the data
 * under it. Nevertheless, we don't want to keep running on the old
 * data anyway.
*/
static void
resource_changed_cb (TaskRunner *self)
{
  if (self->state == TASK_RUNNER_RUNNING)
    task_runner_cancel (self);
}

TaskRunner *
task_runner_new (void)
{
  return g_object_new (TASK_TYPE_RUNNER, NULL);
}

static void
task_runner_task_complete (TaskRunner   *self,
                           GAsyncResult *res,
                           gpointer      user_data)
{
  if (self->clear_on_finish)
    {
      g_array_set_size (self->results, 0);
      self->clear_on_finish = FALSE;
      self->state = TASK_RUNNER_READY;
    }
  else
    {
      self->state = TASK_RUNNER_COMPLETE;
    }
  g_clear_object (&self->task);
  g_clear_object (&self->cancellable);

  g_signal_emit (self, obj_signals [FINISHED], 0);
}


void
task_runner_run (TaskRunner *self,
                 PuzzleTask *puzzle_task)
{
  g_return_if_fail (TASK_IS_RUNNER (self));
  g_return_if_fail (PUZZLE_IS_TASK (puzzle_task));
  g_return_if_fail (self->state != TASK_RUNNER_RUNNING);

  if (self->state == TASK_RUNNER_COMPLETE)
    task_runner_reset (self);

  self->state = TASK_RUNNER_RUNNING;
  self->count = 0;

  /* temporary assertions to make sure our assumptions are right */
  g_assert (self->results == NULL);
  g_assert (self->cancellable == NULL);
  g_assert (self->task == NULL);

  self->results = g_array_new (FALSE, FALSE, sizeof (gpointer));
  self->cancellable = g_cancellable_new ();


  g_array_set_clear_func (self->results,
                          puzzle_task_get_result_clear (puzzle_task));
  puzzle_task_set_run_info (puzzle_task,
                            &self->count,
                            &self->results_mutex,
                            self->results);

  self->task = g_task_new (self, self->cancellable,
                           (GAsyncReadyCallback) task_runner_task_complete,
                           NULL);
  g_task_set_task_data (self->task, g_object_ref (puzzle_task), g_object_unref);
  g_task_run_in_thread (self->task, (GTaskThreadFunc) puzzle_task_run);
}

void
task_runner_cancel (TaskRunner *self)
{
  g_return_if_fail (TASK_IS_RUNNER (self));
  g_return_if_fail (self->state == TASK_RUNNER_RUNNING);

  g_cancellable_cancel (self->cancellable);
  self->state = TASK_RUNNER_COMPLETE;
}

void
task_runner_reset (TaskRunner *self)
{
  g_return_if_fail (TASK_IS_RUNNER (self));

  /* Make sure we cancel it */
  if (task_runner_get_state (self) == TASK_RUNNER_RUNNING)
    {
      self->clear_on_finish = TRUE;
      task_runner_cancel (self);
    }
  else
    {
      g_clear_pointer (&self->results, g_array_unref);
      self->state = TASK_RUNNER_READY;
    }
}

TaskRunnerState
task_runner_get_state (TaskRunner *self)
{
  g_return_val_if_fail (TASK_IS_RUNNER (self), TASK_RUNNER_READY);

  return self->state;
}

gint
task_runner_get_count (TaskRunner *self)
{
  g_return_val_if_fail (TASK_IS_RUNNER (self), 0);

  return g_atomic_int_get (&(self->count));
}

guint
task_runner_get_n_results (TaskRunner *self)
{
  guint len;
  g_return_val_if_fail (TASK_IS_RUNNER (self), 0);

  g_mutex_lock (&self->results_mutex);
  if (self->results == NULL)
    len = 0;
  else
    len = self->results->len;
  g_mutex_unlock (&self->results_mutex);

  return len;
}

gpointer
task_runner_get_result (TaskRunner *self,
                        guint       index)
{
  gpointer result = NULL;

  g_return_val_if_fail (TASK_IS_RUNNER (self), NULL);

  g_mutex_lock (&self->results_mutex);
  if (index < self->results->len)
    result = g_array_index (self->results, gpointer, index);
  g_mutex_unlock (&self->results_mutex);

  return result;
}
