/* task-runner.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include "puzzle-task.h"

G_BEGIN_DECLS


#define TASK_TYPE_RUNNER (task_runner_get_type())
G_DECLARE_FINAL_TYPE (TaskRunner, task_runner, TASK, RUNNER, GObject);


typedef enum
{
  TASK_RUNNER_READY,    /* @ Ready to start @ */
  TASK_RUNNER_RUNNING,  /* @ Currently running @ */
  TASK_RUNNER_COMPLETE, /* @ Finished running @ */
} TaskRunnerState;


TaskRunner      *task_runner_new           (void);
TaskRunnerState  task_runner_get_state     (TaskRunner *self);
void             task_runner_run           (TaskRunner *self,
                                            PuzzleTask *puzzle_task);
void             task_runner_reset         (TaskRunner *self);
void             task_runner_cancel        (TaskRunner *self);
gint             task_runner_get_count     (TaskRunner *self);
guint            task_runner_get_n_results (TaskRunner *self);
gpointer         task_runner_get_result    (TaskRunner *self,
                                            guint       index);


G_END_DECLS
