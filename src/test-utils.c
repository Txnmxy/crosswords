/* test-utils.c
 *
 * Copyright 2021 Federico Mena
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include "test-utils.h"

GridState *
load_state (const char *filename, GridStateMode mode)
{
  char *path = g_test_build_filename (G_TEST_DIST, filename, NULL);
  GError *error = NULL;
  IpuzPuzzle *puzzle = ipuz_puzzle_new_from_file (path, &error);
  GridState *state;

  g_assert_no_error (error);
  g_assert (IPUZ_IS_CROSSWORD (puzzle));

  state = grid_state_new (IPUZ_CROSSWORD (puzzle), NULL, mode);
  g_assert (state != NULL);

  g_object_unref (puzzle);
  g_free (path);

  return state;
}

GridState *
load_state_with_quirks (const char *filename, GridStateMode mode)
{
  char *path = g_test_build_filename (G_TEST_DIST, filename, NULL);
  GError *error = NULL;
  IpuzPuzzle *puzzle = ipuz_puzzle_new_from_file (path, &error);
  GridState *state;

  g_assert_no_error (error);
  g_assert (IPUZ_IS_CROSSWORD (puzzle));

  state = grid_state_new (IPUZ_CROSSWORD (puzzle), NULL, mode);
  g_assert (state != NULL);

  state->quirks = crosswords_quirks_test_new (puzzle);
  /* Set some default values for the quirks to give us predictable results */
  crosswords_quirks_test_set_guess_advance (state->quirks, QUIRKS_GUESS_ADVANCE_ADJACENT);
  crosswords_quirks_test_set_switch_on_move (state->quirks, FALSE);
  g_object_unref (puzzle);
  g_free (path);

  return state;
}
