/* word-list-misc.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <locale.h>
#include "word-array-model.h"
#include "word-list-model.h"


struct _WordArrayModel
{
  GObject parent_object;

  WordList *word_list;
  WordArray *word_array;
  GArray *obj_array;
};


static void     word_array_model_init            (WordArrayModel      *self);
static void     word_array_model_class_init      (WordArrayModelClass *klass);
static void     word_array_model_list_model_init (GListModelInterface *iface);
static void     word_array_model_dispose         (GObject             *object);
static GType    word_array_model_get_item_type   (GListModel          *array);
static guint    word_array_model_get_n_items     (GListModel          *array);
static gpointer word_array_model_get_item        (GListModel          *array,
                                                  guint                position);


G_DEFINE_TYPE_WITH_CODE (WordArrayModel, word_array_model, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL,
                                                word_array_model_list_model_init));

static void
obj_array_clear (WordListModelRow **data)
{
  if (*data != NULL)
    {
      g_object_unref (G_OBJECT (*data));
      *data = NULL;
    }
}

static void
word_array_model_init (WordArrayModel *self)
{
  self->word_array = word_array_new ();
  self->obj_array = g_array_new (FALSE, TRUE, sizeof (WordListModelRow *));
  g_array_set_clear_func (self->obj_array, (GDestroyNotify ) obj_array_clear);
}

static void
word_array_model_class_init (WordArrayModelClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = word_array_model_dispose;
}

static void
word_array_model_list_model_init (GListModelInterface *iface)
{
  iface->get_item_type = word_array_model_get_item_type;
  iface->get_n_items = word_array_model_get_n_items;
  iface->get_item = word_array_model_get_item;
}

static void
word_array_model_dispose (GObject *object)
{
  WordArrayModel *self;

  self = WORD_ARRAY_MODEL (object);

  g_clear_object (&self->word_list);
  g_clear_pointer (&self->word_array, word_array_unref);
  g_clear_pointer (&self->obj_array, g_array_unref);

  G_OBJECT_CLASS (word_array_model_parent_class)->dispose (object);
}


static GType
word_array_model_get_item_type (GListModel *model)
{
  return WORD_TYPE_LIST_MODEL_ROW;
}

static guint
word_array_model_get_n_items (GListModel *model)
{
  WordArrayModel *array_model;

  array_model = WORD_ARRAY_MODEL (model);

  return array_model->obj_array->len;
}

static gpointer
word_array_model_get_item (GListModel *model,
                          guint       position)
{
  WordArrayModel *array_model;
  WordIndex word_index;
  WordListModelRow *row;
  const gchar *word;
  gint priority;

  array_model = WORD_ARRAY_MODEL (model);

  row  = g_array_index (array_model->obj_array, WordListModelRow *, position);
  word_index = word_array_index (array_model->word_array, position);

  word = word_list_get_indexed_word (array_model->word_list, word_index);
  priority = word_list_get_indexed_priority (array_model->word_list, word_index);

  if (row == NULL)
    {
      WordListModelRow **row_ptr = &(g_array_index (array_model->obj_array, WordListModelRow *, position));
      row = word_list_model_row_new (word, priority, NULL);
      *row_ptr = row;
    }
  else
    {
      row->word = word;
      row->priority = priority;
    }

  return g_object_ref (row);
}

WordArrayModel *
word_array_model_new (WordList *word_list)
{
  WordArrayModel *array_model;

  array_model = (WordArrayModel *) g_object_new (WORD_TYPE_ARRAY_MODEL, NULL);
  array_model->word_list = g_object_ref (word_list);

  return array_model;
}

/**
 * word_array_model_add:
 * @array_model: an `ArrayModel`
 * @word_index: a `WordIndex`
 *
 * Adds @word_index to the @array_model.
 *
 * Returns: %True, if @word_index was added
 **/
gboolean
word_array_model_add (WordArrayModel *array_model,
                      WordIndex       word_index)
{
  guint pos = 0;

  g_return_val_if_fail (WORD_IS_ARRAY_MODEL (array_model), FALSE);

  if (word_array_add (array_model->word_array, word_index))
    {
      /* since we reset the value every time you get an object, we don't
       * actually care what's in the obj_array. It just has to be the
       * same length. Weird, huh... */
      g_array_set_size (array_model->obj_array, array_model->obj_array->len +1);
      word_array_find (array_model->word_array, word_index, &pos);
      g_list_model_items_changed (G_LIST_MODEL (array_model),
                                  pos, 0, 1);
      return TRUE;
    }
  return FALSE;
}

/**
 * word_array_model_remove:
 * @array_model: an `ArrayModel`
 * @word_index: a `WordIndex`
 *
 * Removes @word_index from the @array_model.
 *
 * Returns: %True, if @word_index was removed
 **/
gboolean
word_array_model_remove (WordArrayModel *array_model,
                         WordIndex       word_index)
{
  guint pos = 0;

  g_return_val_if_fail (WORD_IS_ARRAY_MODEL (array_model), FALSE);
  g_return_val_if_fail (array_model->obj_array->len != 0, FALSE);

  word_array_find (array_model->word_array, word_index, &pos);

  if (word_array_remove (array_model->word_array, word_index))
    {
      /* since we reset the value every time you get an object, we don't
       * actually care what's in the obj_array. It just has to be the
       * same length. Weird, huh... */
      g_array_set_size (array_model->obj_array, array_model->obj_array->len -1);
      g_list_model_items_changed (G_LIST_MODEL (array_model),
                                  pos, 1, 0);
      return TRUE;
    }

  return FALSE;
}

gboolean
word_array_model_find (WordArrayModel *array_model,
                       WordIndex       word_index,
                       guint          *out)
{
  g_return_val_if_fail (WORD_IS_ARRAY_MODEL (array_model), FALSE);

  return word_array_find (array_model->word_array,
                          word_index, out);
}


/**
 * word_array_model_set_array:
 * @array_model: an `ArrayModel`
 * @array: a `WordArray`
 *
 * Explicitly sets the `WordArray` to back @array_model. Must be
 * sorted and unique.
 **/
void
word_array_model_set_array (WordArrayModel *array_model,
                            WordArray      *array)
{
  guint old_len;

  g_return_if_fail (WORD_IS_ARRAY_MODEL (array_model));

  old_len = array_model->word_array->len;

  if (array == NULL)
    {
      g_array_set_size (array_model->word_array, 0);
      g_array_set_size (array_model->obj_array, 0);
      g_list_model_items_changed (G_LIST_MODEL (array_model), 0, old_len, 0);

      return;
    }

  word_array_ref (array);
  g_clear_pointer (&array_model->word_array, word_array_unref);
  array_model->word_array = array;

  g_array_set_size (array_model->obj_array, array->len);

  if (old_len < array->len)
    g_list_model_items_changed (G_LIST_MODEL (array_model),
                                old_len, 0, array->len - old_len);
  else if (old_len > array->len)
    g_list_model_items_changed (G_LIST_MODEL (array_model),
                                array->len, old_len - array->len, 0);
  /* else do nothing */

  for (guint i = 0; i < array_model->obj_array->len; i++)
    {
       WordListModelRow *row;
       WordIndex word_index;
       const gchar *word;
       gint priority;
       const gchar *enumeration_src;

       word_index = word_array_index (array_model->word_array, i);
       word = word_list_get_indexed_word (array_model->word_list, word_index);
       priority = word_list_get_indexed_priority (array_model->word_list, word_index);
       enumeration_src = word_list_get_indexed_enumeration_src (array_model->word_list, word_index);

       row = g_array_index (array_model->obj_array, WordListModelRow *, i);
       word_list_model_row_changed (row, word, priority, enumeration_src);
    }
}

/**
 * word_array_model_get_array:
 * @array_model: an `ArrayModel`
 *
 * Returns the internal `WordArray` of @array_model. This should not
 * be modified in any way, but is useful to pass into a `WordSolver`.
 *
 * Returns: the internal `WordArray`
 **/
WordArray *
word_array_model_get_array (WordArrayModel *array_model)
{
  g_return_val_if_fail (WORD_IS_ARRAY_MODEL (array_model), NULL);

  return array_model->word_array;
}
