/* word-solver-misc.h
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "word-list.h"


G_BEGIN_DECLS


/* Wrapper around WordArray that implements GListModel. Suitable for
 * putting in a list. It uses a WordListModelRow as its primary type,
 * as they basically provide the same type of data through a different
 * means */
#define WORD_TYPE_ARRAY_MODEL (word_array_model_get_type())
G_DECLARE_FINAL_TYPE (WordArrayModel, word_array_model, WORD, ARRAY_MODEL, GObject);

WordArrayModel *word_array_model_new       (WordList       *word_list);
gboolean        word_array_model_add       (WordArrayModel *array_model,
                                            WordIndex       word_index);
gboolean        word_array_model_remove    (WordArrayModel *array_model,
                                            WordIndex       word_index);
gboolean        word_array_model_find      (WordArrayModel *array_model,
                                            WordIndex       word_index,
                                            guint          *out);
void            word_array_model_set_array (WordArrayModel *array_model,
                                            WordArray      *array);
WordArray      *word_array_model_get_array (WordArrayModel *array_model);


G_END_DECLS
