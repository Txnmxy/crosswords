/* word-list-def.c
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include <locale.h>
#include <json-glib/json-glib.h>
#include "word-list-def.h"
#include "word-list-misc.h"
#include "word-list-resources.h"


typedef struct
{
  guint le_hash;
  guint le_offset;
  gushort le_len;
  gushort unused;
} DefIndexElement;

typedef struct _WordListDefSense
{
  GArray *tags;
  GArray *glosses;
} WordListDefSense;

typedef struct _WordListDefEntry
{
  guint8 pos;
  const gchar *header;
  GArray *senses;
} WordListDefEntry;

struct _WordListDef
{
  grefcount ref_count;

  GVariant *variant;

  const gchar *word_str;
  GArray *entries;
  GArray *pos_list;
  GArray *tags_list;
};


static WordListDef *word_list_def_new_from_data (gconstpointer  data,
                                                 gssize         size,
                                                 GArray        *pos_list,
                                                 GArray        *tags_list);


static void
clear_string (gchar **arr_element)
{
  g_free (*arr_element);
  *arr_element = NULL;
}

static void
clear_sense (WordListDefSense *arr_element)
{
  g_clear_pointer (&arr_element->tags, g_array_unref);
  g_clear_pointer (&arr_element->glosses, g_array_unref);
}

static void
parse_tags (GArray       *tags_arr,
            GVariantIter *tags_iter)
{
  guint16 tag;
  guint i = 0;

  g_assert (g_variant_iter_n_children (tags_iter) > 0);
  g_array_set_size (tags_arr, g_variant_iter_n_children (tags_iter));

  while (g_variant_iter_loop (tags_iter, "q", &tag))
    {
      guint16 *tag_ptr;

      tag_ptr = &g_array_index (tags_arr, guint16, i);
      *tag_ptr = tag;
      i++;
    }
}

static void
parse_glosses (GArray   *glosses_arr,
               GVariant *glosses_var)
{
  GVariantIter iter;
  guint i = 0;
  gchar *gloss;

  g_assert (g_variant_n_children (glosses_var) > 0);
  g_array_set_size (glosses_arr, g_variant_n_children (glosses_var));

  g_variant_iter_init (&iter, glosses_var);
  while (g_variant_iter_loop (&iter, "&s", &gloss))
    {
      gchar **gloss_ptr;

      gloss_ptr = &g_array_index (glosses_arr, gchar *, i);
      *gloss_ptr = gloss;
      i++;
    }
}

static void
parse_senses (GArray   *senses_arr,
              GVariant *senses_var)
{
  GVariantIter iter;
  GVariant *tags_var;
  GVariant *glosses_var;
  guint i = 0;

  g_assert (g_variant_n_children (senses_var) > 0);
  g_array_set_size (senses_arr, g_variant_n_children (senses_var));

  g_variant_iter_init (&iter, senses_var);
  while (g_variant_iter_loop (&iter, "(@maq@as)", &tags_var, &glosses_var))
    {
      WordListDefSense *sense;
      GVariantIter *tags_iter = NULL;

      sense = &g_array_index (senses_arr, WordListDefSense, i);

      /* tags are a maybe variant, and may not exist. */
      g_variant_get (tags_var, "maq", &tags_iter);
      if (tags_iter)
        {
          sense->tags = g_array_new (FALSE, FALSE, sizeof (guint16));
          parse_tags (sense->tags, tags_iter);
          g_variant_iter_free (tags_iter);
        }
      else
        {
          sense->tags = NULL;
        }

      sense->glosses = g_array_new (FALSE, FALSE, sizeof (gchar *));
      g_array_set_clear_func (sense->glosses, (GDestroyNotify) clear_string);

      parse_glosses (sense->glosses, glosses_var);
      i++;
    }
}


static void
parse_entries (GArray   *entries_arr,
               GVariant *entries_var)
{
  GVariantIter iter;
  guint8 pos;
  const gchar *header;
  GVariant *senses_var;
  guint i = 0;

  g_assert (g_variant_n_children (entries_var) > 0);
  g_array_set_size (entries_arr, g_variant_n_children (entries_var));

  g_variant_iter_init (&iter, entries_var);
  while (g_variant_iter_loop (&iter, "(y&s@*)", &pos, &header, &senses_var))
    {
      WordListDefEntry *entry;

      entry = &g_array_index (entries_arr, WordListDefEntry, i);
      entry->pos = pos;
      entry->header = header;

      entry->senses = g_array_new (FALSE, FALSE, sizeof (WordListDefSense));
      g_array_set_clear_func (entry->senses, (GDestroyNotify) clear_sense);

      parse_senses (entry->senses, senses_var);
      i++;
    }
}

static void
parse_def (WordListDef *def)
{
  g_autoptr (GVariant) word_str_var = NULL;
  g_autoptr (GVariant) entries_var = NULL;

  word_str_var = g_variant_get_child_value (def->variant, 0);
  entries_var = g_variant_get_child_value (def->variant, 1);

  def->word_str = g_variant_get_string (word_str_var, NULL);
  g_assert (def->word_str != NULL);

  def->entries = g_array_new (FALSE, FALSE, sizeof (WordListDefEntry));
  parse_entries (def->entries, entries_var);
}

static WordListDef *
word_list_def_new_from_data (gconstpointer  data,
                             gssize         size,
                             GArray        *pos_list,
                             GArray        *tags_list)
{
  static GVariantType *def_type = NULL;
  WordListDef *def;

  /* used by every call to word_list_def_new_from_data(). Immutable */
  if (def_type == NULL)
    def_type = g_variant_type_new ("(sa(ysa(maqas)))");

  def = g_new0 (WordListDef, 1);
  g_ref_count_init (&def->ref_count);

  if (pos_list)
    def->pos_list = g_array_ref (pos_list);
  if (tags_list)
    def->tags_list = g_array_ref (tags_list);

  def->variant = g_variant_new_from_data (def_type,
                                          data, size,
                                          TRUE,
                                          NULL, NULL);
  /* NOTE: We assert expected values. If the dictionary is corrupt, we
   * don't try to continue */
  g_assert (def->variant);

  parse_def (def);

  return def;
}

/* Once we've found a hit against the hash, we need to walk the full
 * space with that hash. We may have multiple WordEntries for a given
 * hash, or we may have hash collisions. This will walk both
 * directions from the hash hit and check each entry. If it's valid,
 * it will add it to an array. */
static GArray *
build_search_array (GBytes      *index_bytes,
                    GBytes      *defs_bytes,
                    GArray      *pos_list,
                    GArray      *tags_list,
                    IpuzCharset *alphabet,
                    const gchar *filter,
                    guint        offset)
{
  guint target_hash;
  DefIndexElement *index_data;
  gconstpointer defs_data;
  gsize index_size = 0;
  gsize defs_size = 0;

  GArray *search_results = NULL;

  target_hash = g_str_hash (filter);
  index_data = (DefIndexElement *) g_bytes_get_data (index_bytes, &index_size);
  defs_data = g_bytes_get_data (defs_bytes, &defs_size);

  /*Start at the hash hit we found and go backwards */
  for (guint i = offset; i > 0; i--)
    {
      guint shifted_hash;
      DefIndexElement def_index;

      def_index = index_data [i];

      shifted_hash = GUINT_FROM_LE (def_index.le_hash);
      if (shifted_hash == target_hash)
        {
          WordListDef *def;
          guint offset = GUINT_FROM_LE (def_index.le_offset);
          gushort len = GUINT16_FROM_LE (def_index.le_len);
          g_autofree gchar *parsed_word = NULL;

          def = word_list_def_new_from_data (defs_data + offset, len, pos_list, tags_list);
          g_assert (def);

          parsed_word =
            scored_parse_word (word_list_def_get_word_str (def), alphabet);

          if (g_strcmp0 (parsed_word, filter) == 0)
            {
              if (search_results == NULL)
                {
                  search_results = g_array_new (FALSE, FALSE, sizeof (WordListDef *));
                }
              g_array_prepend_val (search_results, def);
            }
          else
            {
              word_list_def_unref (def);
            }
        }
    }

  /*Start at the hash hit we found and go backwards */
  for (guint i = offset + 1; i < index_size / sizeof (DefIndexElement); i++)
    {
      guint shifted_hash;
      DefIndexElement def_index;

      def_index = index_data [i];

      shifted_hash = GUINT_FROM_LE (def_index.le_hash);
      if (shifted_hash == target_hash)
        {
          WordListDef *def;
          guint offset = GUINT_FROM_LE (def_index.le_offset);
          gushort len = GUINT16_FROM_LE (def_index.le_len);
          g_autofree gchar *parsed_word = NULL;

          def = word_list_def_new_from_data (defs_data + offset, len, pos_list, tags_list);
          g_assert (def);

          parsed_word =
            scored_parse_word (word_list_def_get_word_str (def), alphabet);

          if (g_strcmp0 (parsed_word, filter) == 0)
            {
              if (search_results == NULL)
                {
                  search_results = g_array_new (FALSE, FALSE, sizeof (WordListDef *));
                }
              g_array_append_val (search_results, def);
            }
          else
            {
              word_list_def_unref (def);
            }
        }
    }

  return search_results;
}

static GArray *
lookup_search (GBytes      *index_bytes,
               GBytes      *defs_bytes,
               GArray      *pos_list,
               GArray      *tags_list,
               IpuzCharset *alphabet,
               const gchar *filter)
{
  guint target_hash;
  DefIndexElement *index_data;
  guint start, end, middle;
  gsize size = 0;

  target_hash = g_str_hash (filter);
  index_data = (DefIndexElement *) g_bytes_get_data (index_bytes, &size);
  start = 0;
  end = size / sizeof (DefIndexElement);

  while (start <= end)
    {
      guint shifted_hash;

      middle = start + (end - start)/2;
      shifted_hash = GUINT_FROM_LE (index_data [middle].le_hash);

      if (shifted_hash == target_hash)
        return build_search_array (index_bytes, defs_bytes, pos_list, tags_list, alphabet, filter, middle);
      else if (shifted_hash < target_hash)
        start = middle + 1;
      else
        end = middle - 1;
    }

  /* we didn't find the hash */
  return NULL;
}

GArray *
word_list_def_get_definitions (WordListResource *resource,
                               const gchar      *filter)
{
  WordListIndex *index;
  GBytes *index_bytes;
  GBytes *defs_bytes;
  GArray *pos_list;
  GArray *tags_list;

  if (filter == NULL || *filter == '\0')
    return NULL;

  index = word_list_resource_get_index (resource);
  g_assert (index);

  defs_bytes = word_list_resource_get_defs_bytes (resource);
  index_bytes = word_list_resource_get_defs_index_bytes (resource);
  pos_list = word_list_resource_get_defs_pos_list (resource);
  tags_list = word_list_resource_get_defs_tags_list (resource);

  /* not every resource will have definitions */
  if (defs_bytes == NULL)
    return NULL;

  return lookup_search (index_bytes,
                        defs_bytes,
                        pos_list,
                        tags_list,
                        index->charset,
                        filter);
}

WordListDef *
word_list_def_ref (WordListDef *def)
{
  g_return_val_if_fail (def != NULL, NULL);

  g_ref_count_inc (&def->ref_count);

  return def;
}

void
word_list_def_unref (WordListDef *def)
{
  if (def == NULL)
    return;

  if (!g_ref_count_dec (&def->ref_count))
    return;

  g_clear_pointer (&def->variant, g_variant_unref);
  g_clear_pointer (&def->entries, g_array_unref);
  g_clear_pointer (&def->pos_list, g_array_unref);
  g_clear_pointer (&def->tags_list, g_array_unref);

  g_free (def);
}

const gchar *
word_list_def_get_word_str (WordListDef *def)
{
  g_return_val_if_fail (def != NULL, NULL);

  return def->word_str;
}

guint
word_list_def_get_n_entries (WordListDef *def)
{
  g_return_val_if_fail (def != NULL, 0);

  return def->entries->len;
}

const gchar *
word_list_def_entry_get_pos (WordListDef *def,
                             guint        entry_index)
{
  WordListDefEntry *entry;

  g_return_val_if_fail (def != NULL, NULL);
  g_return_val_if_fail (def->pos_list != NULL, NULL);
  g_return_val_if_fail (entry_index < def->entries->len, NULL);

  entry = &(g_array_index (def->entries, WordListDefEntry, entry_index));

  g_return_val_if_fail (entry->pos < def->pos_list->len, NULL);

  return g_array_index (def->pos_list, gchar *, entry->pos);
}

const gchar *
word_list_def_entry_get_header (WordListDef *def,
                                guint        entry_index)
{
  WordListDefEntry *entry;

  g_return_val_if_fail (def != NULL, NULL);
  g_return_val_if_fail (entry_index < def->entries->len, NULL);

  entry = &(g_array_index (def->entries, WordListDefEntry, entry_index));

  return entry->header;
}

guint
word_list_def_entry_get_n_senses (WordListDef *def,
                                  guint        entry_index)
{
  WordListDefEntry *entry;

  g_return_val_if_fail (def != NULL, 0);
  g_return_val_if_fail (entry_index < def->entries->len, 0);

  entry = &(g_array_index (def->entries, WordListDefEntry, entry_index));

  return entry->senses->len;
}

guint
word_list_def_entry_sense_get_n_glosses (WordListDef *def,
                                         guint        entry_index,
                                         guint        sense_index)
{
  WordListDefEntry *entry;
  WordListDefSense *sense;

  g_return_val_if_fail (def != NULL, 0);
  g_return_val_if_fail (entry_index < def->entries->len, 0);

  entry = &(g_array_index (def->entries, WordListDefEntry, entry_index));
  g_return_val_if_fail (sense_index < entry->senses->len, 0);

  sense = &(g_array_index (entry->senses, WordListDefSense, sense_index));

  return sense->glosses->len;
}

const gchar *
word_list_def_entry_sense_get_gloss (WordListDef *def,
                                     guint        entry_index,
                                     guint        sense_index,
                                     guint        gloss_index)
{
  WordListDefEntry *entry;
  WordListDefSense *sense;

  g_return_val_if_fail (def != NULL, 0);
  g_return_val_if_fail (entry_index < def->entries->len, 0);

  entry = &(g_array_index (def->entries, WordListDefEntry, entry_index));
  g_return_val_if_fail (sense_index < entry->senses->len, 0);

  sense = &(g_array_index (entry->senses, WordListDefSense, sense_index));

  return g_array_index (sense->glosses, gchar *, gloss_index);
}

guint
word_list_def_entry_sense_get_n_tags (WordListDef      *def,
                                      guint             entry_index,
                                      guint             sense_index)
{
  WordListDefEntry *entry;
  WordListDefSense *sense;

  g_return_val_if_fail (def != NULL, 0);
  g_return_val_if_fail (entry_index < def->entries->len, 0);

  entry = &(g_array_index (def->entries, WordListDefEntry, entry_index));
  g_return_val_if_fail (sense_index < entry->senses->len, 0);

  sense = &(g_array_index (entry->senses, WordListDefSense, sense_index));

  if (sense->tags)
    return sense->tags->len;

  return 0;
}
const gchar *
word_list_def_entry_sense_get_tag (WordListDef      *def,
                                   guint             entry_index,
                                   guint             sense_index,
                                   guint             tag_index)
{
  WordListDefEntry *entry;
  WordListDefSense *sense;
  guint16 tag;


  g_return_val_if_fail (def != NULL, 0);
  g_return_val_if_fail (entry_index < def->entries->len, 0);

  entry = &(g_array_index (def->entries, WordListDefEntry, entry_index));
  g_return_val_if_fail (sense_index < entry->senses->len, 0);

  sense = &(g_array_index (entry->senses, WordListDefSense, sense_index));

  if (sense->tags == NULL)
    return NULL;
  g_return_val_if_fail (tag_index < sense->tags->len, NULL);

  tag = g_array_index (sense->tags, guint16, tag_index);
  g_return_val_if_fail ((guint) tag < def->tags_list->len, NULL);

  return g_array_index (def->tags_list, gchar *, (guint) tag);
}


void
word_list_def_print (WordListDef *def)
{
  g_return_if_fail (def != NULL);

  g_print ("\e[1m%s\e[0m\n", word_list_def_get_word_str (def));
  for (guint i = 0; i < word_list_def_get_n_entries (def); i++)
    {
      guint gloss_count = 1;
      g_print ("\t%s\n", word_list_def_entry_get_pos (def, i));
      g_print ("\t%s\n", word_list_def_entry_get_header (def, i));

      for (guint j = 0; j < word_list_def_entry_get_n_senses (def, i); j++)
        {
          for (guint k = 0; k < word_list_def_entry_sense_get_n_glosses (def, i, j); k++)
            {
              g_print ("\t\t%d. \e[3m%s\e[0m\n", gloss_count++,
                       word_list_def_entry_sense_get_gloss (def, i, j, k));
            }
        }
    }
}

#ifdef TESTING

#define TEST_DATA "../word-lists/test.gresource"

static void
load_def (void)
{
  g_autofree gchar *path = NULL;
  g_autoptr (WordListResource) resource = NULL;
  g_autoptr (GArray) defs = NULL;
  WordListDef *def;

  path = g_test_build_filename (G_TEST_BUILT, TEST_DATA, NULL);
  resource = word_list_resource_new_from_file (path);

  /* CAT */
  defs = word_list_def_get_definitions (resource, "CAT");
  g_assert_true (defs->len == 4);
  def = g_array_index (defs, WordListDef *, 0);
  g_assert_true (word_list_def_entry_get_n_senses (def, 0) == 17);
  g_assert_true (word_list_def_entry_sense_get_n_glosses (def, 0, 16) == 1);
}

int
main (int argc, char **argv)
{
  setlocale(LC_ALL, "en_US.UTF-8");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/defs/load_def", load_def);

  return g_test_run ();
}
#endif

