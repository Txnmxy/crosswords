/* word-list-def.h
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include "word-list-misc.h"
#include "word-list-resource.h"


G_BEGIN_DECLS


typedef struct _WordListDef WordListDef;

#define WORD_LIST_DEF(def) ((WordListDef *)def)


GArray      *word_list_def_get_definitions           (WordListResource *resource,
                                                      const gchar      *word);
WordListDef *word_list_def_ref                       (WordListDef      *def);
void         word_list_def_unref                     (WordListDef      *def);
const gchar *word_list_def_get_word_str              (WordListDef      *def);
guint        word_list_def_get_n_entries             (WordListDef      *def);
const gchar *word_list_def_entry_get_pos             (WordListDef      *def,
                                                      guint             entry_index);
const gchar *word_list_def_entry_get_header          (WordListDef      *def,
                                                      guint             entry_index);
guint        word_list_def_entry_get_n_senses        (WordListDef      *def,
                                                      guint             entry_index);
guint        word_list_def_entry_sense_get_n_glosses (WordListDef      *def,
                                                      guint             entry_index,
                                                      guint             sense_index);
const gchar *word_list_def_entry_sense_get_gloss     (WordListDef      *def,
                                                      guint             entry_index,
                                                      guint             sense_index,
                                                      guint             gloss_index);
guint        word_list_def_entry_sense_get_n_tags    (WordListDef      *def,
                                                      guint             entry_index,
                                                      guint             sense_index);
const gchar *word_list_def_entry_sense_get_tag       (WordListDef      *def,
                                                      guint             entry_index,
                                                      guint             sense_index,
                                                      guint             tag_index);

/* debugging function */
void         word_list_def_print                     (WordListDef      *def);


G_DEFINE_AUTOPTR_CLEANUP_FUNC(WordListDef, word_list_def_unref)


G_END_DECLS
