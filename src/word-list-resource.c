/* word-list-resource.c
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include <locale.h>
#include "word-list-resource.h"


enum
{
  PROP_0,
  PROP_ID,
  PROP_RESOURCE,
  N_PROPS,
};

static GParamSpec *obj_props[N_PROPS] = { NULL, };


struct _WordListResource
{
  GObject parent_object;

  gchar *id;
  GResource *resource;

  WordListIndex *index;
  GBytes *index_bytes;
  GBytes *frequency_bytes;
  GBytes *anagram_bytes;
  GBytes *defs_bytes;
  GBytes *defs_index_bytes;
  GArray *defs_pos_list;
  GArray *defs_tags_list;
  gboolean error_loading;
};


static void word_list_resource_init          (WordListResource      *self);
static void word_list_resource_class_init    (WordListResourceClass *klass);
static void word_list_resource_set_property  (GObject               *object,
                                              guint                  prop_id,
                                              const GValue          *value,
                                              GParamSpec            *pspec);
static void word_list_resource_get_property  (GObject               *object,
                                              guint                  prop_id,
                                              GValue                *value,
                                              GParamSpec            *pspec);
static void word_list_resource_dispose       (GObject               *object);
static void load_resource                    (WordListResource      *self);


G_DEFINE_FINAL_TYPE (WordListResource, word_list_resource, G_TYPE_OBJECT);


static void
word_list_resource_init (WordListResource *self)
{
  /* we set this flag if we couldn't successfully load a word list
   * resource. */
  self->error_loading = FALSE;
}

static void
word_list_resource_class_init (WordListResourceClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = word_list_resource_set_property;
  object_class->get_property = word_list_resource_get_property;
  object_class->dispose = word_list_resource_dispose;


  obj_props[PROP_ID] =
    g_param_spec_string ("id",
                         NULL, NULL,
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
  obj_props[PROP_RESOURCE] =
    g_param_spec_boxed ("resource",
                        NULL, NULL,
                        G_TYPE_RESOURCE,
                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

}

static void
word_list_resource_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  WordListResource *self;

  self = WORD_LIST_RESOURCE (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_assert (self->id == NULL);

      self->id = g_value_dup_string (value);

      if (self->resource && self->id)
        load_resource (self);
      break;
    case PROP_RESOURCE:
      g_assert (self->resource == NULL);

      self->resource = g_value_dup_boxed (value);
      if (self->resource && self->id)
        load_resource (self);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
word_list_resource_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  WordListResource *self;

  self = WORD_LIST_RESOURCE (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, self->id);
      break;
    case PROP_RESOURCE:
      g_value_set_boxed (value, self->resource);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
word_list_resource_dispose (GObject *object)
{
  WordListResource *self;

  self = WORD_LIST_RESOURCE (object);

  g_clear_pointer (&self->id, g_free);
  g_clear_pointer (&self->resource, g_resource_unref);
  g_clear_pointer (&self->index, word_list_index_free);
  g_clear_pointer (&self->index_bytes, g_bytes_unref);
  g_clear_pointer (&self->frequency_bytes, g_bytes_unref);
  g_clear_pointer (&self->anagram_bytes, g_bytes_unref);
  g_clear_pointer (&self->defs_bytes, g_bytes_unref);
  g_clear_pointer (&self->defs_index_bytes, g_bytes_unref);

  G_OBJECT_CLASS (word_list_resource_parent_class)->dispose (object);
}


static void
load_index (WordListResource *self)
{
  g_autoptr (GInputStream) stream = NULL;
  g_autofree gchar *path = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (JsonParser) parser = NULL;

  path = g_strdup_printf ("/org/gnome/Crosswords/word-list/%s.json",
                          self->id);

  stream = g_resource_open_stream (self->resource, path,
                                   G_RESOURCE_LOOKUP_FLAGS_NONE, NULL);

  if (stream == NULL)
    {
      g_warning ("Word list resource %s: Missing index at %s", self->id, path);
      goto error;
    }

  parser = json_parser_new_immutable ();
  if (! json_parser_load_from_stream (parser, stream, NULL, &error))
    {
      g_warning ("Word list %s: Error parsing index", self->id);
      goto error;
    }

  self->index = word_list_index_new_from_json (json_parser_get_root (parser));
  if (self->index == NULL)
    {
      g_warning ("Word list %s: Error parsing index", self->id);
      goto error;
    }

  return;

  /* Drop the reference to the resource if it can't be loaded */
 error:
  g_clear_pointer (&self->resource, g_resource_unref);
  self->error_loading = TRUE;
}

static GBytes *
load_bytes (WordListResource *self,
            const gchar      *format_str,
            gboolean          required)
{
  g_autoptr (GError) error = NULL;
  gchar *path;
  GBytes *bytes;

  path = g_strdup_printf (format_str, self->id);
  bytes = g_resource_lookup_data (self->resource,
                                  path, 0,
                                  &error);

  if (required && bytes == NULL)
    {
      g_warning ("resource %s doesn't have %s: %s", self->id, path, error->message);
      self->error_loading = TRUE;
    }
  return bytes;
}

static void
clear_string (gchar **arr_element)
{
  g_free (*arr_element);
  *arr_element = NULL;
}


static GArray *
build_defs_enum_list (GBytes      *enum_index_bytes,
                      const gchar *enum_str)
{
  gconstpointer enum_index_data;
  g_autoptr (JsonParser) parser = NULL;
  g_autoptr (GError) error = NULL;
  JsonNode *root;
  JsonObject *root_obj;
  JsonNode *arr_node;
  JsonArray *arr;
  GArray *pos_list;
  gsize size;

  enum_index_data = g_bytes_get_data (enum_index_bytes, &size);

  parser = json_parser_new ();
  json_parser_load_from_data (parser, enum_index_data, size, &error);
  if (error)
    {
      g_warning ("Failed to parse enum index from word list: %s\n", error->message);
      return NULL;
    }


  root = json_parser_get_root (parser);
  root_obj = json_node_get_object (root);

  arr_node = json_object_get_member (root_obj, enum_str);
  arr = json_node_get_array (arr_node);

  g_return_val_if_fail (arr != NULL, NULL);

  pos_list = g_array_new (FALSE, FALSE, sizeof (gchar *));
  g_array_set_clear_func (pos_list, (GDestroyNotify) clear_string);
  for (guint i = 0; i < json_array_get_length (arr); i++)
    {
      JsonNode *element = json_array_get_element (arr, i);
      gchar *pos = json_node_dup_string (element);
      g_array_append_val (pos_list, pos);
    }

  return pos_list;
}

static void
load_resource (WordListResource *self)
{
  g_autoptr (GBytes) enum_index_bytes = NULL;

  load_index (self);
  self->index_bytes = load_bytes (self, "/org/gnome/Crosswords/word-list/%s.dict", TRUE);
  self->defs_bytes = load_bytes (self, "/org/gnome/Crosswords/word-list/%s-gvariant-defs.data", FALSE);
  self->defs_index_bytes = load_bytes (self, "/org/gnome/Crosswords/word-list/%s-gvariant-index.data", FALSE);

  enum_index_bytes = load_bytes (self, "/org/gnome/Crosswords/word-list/%s-enum-index.json", FALSE);
  if (enum_index_bytes)
    {
      self->defs_pos_list = build_defs_enum_list (enum_index_bytes, "pos");
      self->defs_tags_list = build_defs_enum_list (enum_index_bytes, "tags");
    }

  /* FIXME(resource): Make sure we have a well=formed
     resource. Currently, we only accept resources that come with the
     app. We need to do a lot more checking before we accept 3P
     resources.*/
}

WordListResource *
word_list_resource_new_from_file (const gchar *file_name)
{
  WordListResource *retval = NULL;
  g_autoptr (GResource) resource = NULL; 
  g_autoptr (GError) err = NULL;
  g_autofree gchar *id = NULL;
  gsize len;

  if (! g_str_has_suffix (file_name, ".gresource"))
    return NULL;

  /* Get the id of the resource */
  id = g_path_get_basename (file_name);
  len = strlen (id);
  if (len <= strlen (".gresource"))
    return NULL;

  id[len - strlen (".gresource")] = '\0';

  resource = g_resource_load (file_name, &err);
  if (err)
    {
      g_warning ("Failed to load resource %s\nError %s", file_name, err->message);
      return NULL;
    }

  
  retval = g_object_new (WORD_TYPE_LIST_RESOURCE,
                         "id", id,
                         "resource", resource,
                         NULL);

  if (retval->error_loading)
    {
      g_warning ("failed to load %s", file_name);
      g_object_unref (retval);
      return NULL;
    }

  return retval;
}

GResource *
word_list_resource_get_resource (WordListResource *self)
{
  g_return_val_if_fail (WORD_IS_LIST_RESOURCE (self), NULL);

  return self->resource;
}

WordListIndex *
word_list_resource_get_index (WordListResource *self)
{
  g_return_val_if_fail (WORD_IS_LIST_RESOURCE (self), NULL);

  return self->index;
}

GBytes *
word_list_resource_get_index_bytes (WordListResource *self)
{
  g_return_val_if_fail (WORD_IS_LIST_RESOURCE (self), NULL);

  return self->index_bytes;
}

GBytes *
word_list_resource_get_frequency_bytes (WordListResource *self)
{
  g_return_val_if_fail (WORD_IS_LIST_RESOURCE (self), NULL);

  return self->frequency_bytes;
}

GBytes *
word_list_resource_get_anagram_bytes (WordListResource *self)
{
  g_return_val_if_fail (WORD_IS_LIST_RESOURCE (self), NULL);

  return self->anagram_bytes;
}

GBytes *
word_list_resource_get_defs_bytes (WordListResource *self)
{
  g_return_val_if_fail (WORD_IS_LIST_RESOURCE (self), NULL);

  return self->defs_bytes;
}

GBytes *
word_list_resource_get_defs_index_bytes (WordListResource *self)
{
  g_return_val_if_fail (WORD_IS_LIST_RESOURCE (self), NULL);

  return self->defs_index_bytes;
}

GArray *
word_list_resource_get_defs_pos_list (WordListResource *self)
{
  g_return_val_if_fail (WORD_IS_LIST_RESOURCE (self), NULL);

  return self->defs_pos_list;
}

GArray *
word_list_resource_get_defs_tags_list (WordListResource *self)
{
  g_return_val_if_fail (WORD_IS_LIST_RESOURCE (self), NULL);

  return self->defs_tags_list;
}

#ifdef TESTING

#define TEST_DATA "../word-lists/test.gresource"

static void
test_resource (void)
{
  g_autofree gchar *path = NULL;
  g_autoptr (WordListResource) resource = NULL;
  WordListIndex *index;

  path = g_test_build_filename (G_TEST_BUILT, TEST_DATA, NULL);

  resource = word_list_resource_new_from_file (path);

  /* FIXME(test): Write more thorough tests here */
  g_assert_true (resource != NULL);
  index = word_list_resource_get_index (resource);

  g_assert_cmpstr (index->display_name, ==, "Test Words");
}

int
main (int argc, char **argv)
{
  setlocale(LC_ALL, "en_US.UTF-8");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/defs/test_resource", test_resource);

  return g_test_run ();
}

#endif
