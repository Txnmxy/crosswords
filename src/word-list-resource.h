/* word-list-resource.h
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include "word-list-index.h"

G_BEGIN_DECLS


#define WORD_TYPE_LIST_RESOURCE (word_list_resource_get_type())
G_DECLARE_FINAL_TYPE (WordListResource, word_list_resource, WORD, LIST_RESOURCE, GObject);


WordListResource *word_list_resource_new_from_file        (const gchar      *file_name);
GResource        *word_list_resource_get_resource         (WordListResource *self);
WordListIndex    *word_list_resource_get_index            (WordListResource *self);
GBytes           *word_list_resource_get_index_bytes      (WordListResource *self);
GBytes           *word_list_resource_get_frequency_bytes  (WordListResource *self);
GBytes           *word_list_resource_get_anagram_bytes    (WordListResource *self);
GBytes           *word_list_resource_get_defs_bytes       (WordListResource *self);
GBytes           *word_list_resource_get_defs_index_bytes (WordListResource *self);
GArray           *word_list_resource_get_defs_pos_list    (WordListResource *self);
GArray           *word_list_resource_get_defs_tags_list   (WordListResource *self);

G_END_DECLS
