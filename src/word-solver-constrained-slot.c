/* word-solver-constrained-slot.c
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
/*
 * Complexity sink
 */

/* ConstrainedSlots contain an array of ConstrainedSlot. This is
 * essentially the list of clues that we are interested in solving for
 * in the solver. It is entirely stateless: We keep some extra
 * information around to help work with them but it's transient,
 * incompletely calculated, and not for external use.
 *
 * This is internal code for the word-solver betterfill
 * algorithm. It's highly specialized, and not to be used
 * elsewhere. It also uses betterfill terminology instead of the
 * libipuz terminology used elsewhere.
 *
 * While it looks and is implemented as a list, the order of the list
 * does not matter. Instead, we use it to simply to calculate the best
 * possible slot to tackle with a given overlay. The reason is that
 * the list is not stable: putting in a guess may change the best one
 * to pick from iteration to iteration.
 *
 * We use the following scoring technique (from betterfill).
 *
 * 1. The more known letters the better, i.e. "EX.UIS.TE" seems easier than "T....D"
 * 2. The less unknown letters the better, i.e. "EX.T" seems easier than "EXT....."
 * 3. The less candidates the better, i.e. "...ZZ" s easier than "E...T"
 * 4. The more crossings the better, i.e. choose to fail fast

 * Note: The score is stored as a guint32. The breakdown is:
 * 1-bit   unused
 * 5-bits  length of known letters
 * 5-bits  MASK - length of unknown letters
 * 16-bits MASK - number of candidates
 * 5-bits  number of crossings
 *
 * This can be compared with other scores to pick the most promising
 * slot to use next. We don't fully compute the score. Since sorting
 * is never a goal, if a score is going to be smaller we don't bother
 * calculating the remaining bits.
 *
 * NOTE: We also store a word-list per slot. This is a little
 * overkill, but saves us from having to reset the word-list after
 * every iteration. Or in other words, upfront memory is cheap, and it
 * saves us a word_list_match() per iteration. (It also makes the API a
 * little cleaner)
 */
/* This file also generates a list of possible words that can go in a
 * given slot. It's a copy of the words from the word-list that fit
 * the slot, but it's ordered by a word score instead. The word-score
 * is a little complex: For each crossing slot, we look at the letter
 * in the word and see what percentage of words have that latter at
 * the crossing index. A word's score is the sum of all these
 * percentages.
 *
 * This is confusing so here's a diagram:
 *
 *   ?
 * CA??
 *   ??
 *   ??
 *   ??
 *
 * Consider a simple grid above. In this case the main slot would be
 * the word CA??, and the crossing slots would have the filters F???K
 * and ???S. The index of the first crossing slot would be 2, and the
 * crossing index would be one. The index of the second crossing slot
 * would be 3, and the crossing index would be 0.
 *
 * If we wanted to score the word CARS, we would see what the
 * frequency of the letter 'R' is the second letter of 5 letter words,
 * and add it to the frequency of 'S' in the first letter of 4 letter
 * words.
 *
 * This is just a heuristic: it doesn't take into effect any letters
 * in the crossing slot. however, it constrains the words in a way
 * that we choose better words when iterating.
 */


#include "word-solver-constrained-slot.h"


/* 5-bit and 16-bit masks */
#define SCORE_LETTER_MASK ((1<<5)-1)
#define SCORE_CANDIDATES_MASK ((1<<16)-1)


static gchar *slot_to_filter (IpuzCellCoordArray *slot,
                              IpuzGuesses        *overlay);


static GHashTable *
create_unique_slots (IpuzCrossword *xword,
                     CellArray     *selected_cells)
{
  GHashTable *unique_slots;

  unique_slots = g_hash_table_new (g_int64_hash, g_int64_equal);

  for (guint i = 0; i < cell_array_len (selected_cells); i++)
    {
      IpuzCellCoord coord;
      IpuzCell *cell;
      const IpuzClue *clue;
      IpuzCellCoordArray *slot = NULL;

      coord = cell_array_index (selected_cells, i);
      cell = ipuz_grid_get_cell (IPUZ_GRID (xword), &coord);

      if (!IPUZ_CELL_IS_NORMAL (cell))
        continue;

      if (ipuz_cell_get_solution (cell) ||
          ipuz_cell_get_initial_val (cell))
        continue;

      /* Across */
      clue = ipuz_cell_get_clue (cell, IPUZ_CLUE_DIRECTION_ACROSS);
      if (clue)
        {
          slot = ipuz_clue_get_coords (clue);
          g_hash_table_insert (unique_slots, &slot, slot);
        }

      /* Down */
      clue = ipuz_cell_get_clue (cell, IPUZ_CLUE_DIRECTION_DOWN);
      if (clue)
        {
          slot = ipuz_clue_get_coords (clue);
          g_hash_table_insert (unique_slots, &slot, slot);
        }
    }

  return unique_slots;
}

static void
clear_constrained_slot (ConstrainedSlot *con_slot)
{
  g_clear_pointer (&con_slot->crossings, g_array_unref);
  g_clear_object (&con_slot->word_list);
}

GArray *
constrained_slots_create (WordListResource *resource,
                          IpuzCrossword    *xword,
                          CellArray        *selected_cells)
{
  g_autoptr (GHashTable) unique_slots = NULL;
  GArray *constrained_slots;
  GHashTableIter iter;
  gpointer key, value;

  /* We use the hash_table to keep track of slots that touch
   * selected_cells */
  unique_slots = create_unique_slots (xword, selected_cells);

  constrained_slots = g_array_new (FALSE, FALSE, sizeof (ConstrainedSlot));
  g_array_set_clear_func (constrained_slots, (GDestroyNotify) clear_constrained_slot);

  g_hash_table_iter_init (&iter, unique_slots);
  while (g_hash_table_iter_next (&iter, &key, &value))
    {
      IpuzClueDirection dir = IPUZ_CLUE_DIRECTION_ACROSS;
      ConstrainedSlot con_slot;
      IpuzCellCoord coord;
      guint i = 0;

      con_slot.slot = value;
      con_slot.crossings = g_array_new (FALSE, FALSE, sizeof (CrossingSlot));
      con_slot.word_list = word_list_new ();
      word_list_set_resource (con_slot.word_list, resource);

      while (ipuz_cell_coord_array_index (con_slot.slot, i, &coord))
        {
          CrossingSlot cross_slot;
          IpuzCell *cell;
          const IpuzClue *clue;

          /* Quick short circuit from storing the direction of the
           * clue with the coord_array. We check the across clue for
           * the first cell and see if the slot is the same as the
           * slot we're looking at. If so, we catch all the crossing
           * clues in the opposite direction.
           */
          if (i == 0)
            {
              cell = ipuz_grid_get_cell (IPUZ_GRID (xword), &coord);
              clue = ipuz_cell_get_clue (cell, dir);
              if (clue && ipuz_clue_get_coords (clue) == con_slot.slot)
                dir = ipuz_clue_direction_switch (dir);
            }

          cell = ipuz_grid_get_cell (IPUZ_GRID (xword), &coord);

          /* Ignore crossings with a cell already filled out. We can't
           * affect those. */
          if (ipuz_cell_get_solution (cell) || ipuz_cell_get_initial_val (cell))
            goto done;

          clue = ipuz_cell_get_clue (cell, dir);
          if (clue)
            {

              cross_slot.slot = ipuz_clue_get_coords (clue);
              if (cross_slot.slot)
                {
                  gint crossing_index;

                  crossing_index = ipuz_cell_coord_array_coord_index (cross_slot.slot, &coord);
                  if (crossing_index >= 0)
                    {
                      cross_slot.index = i;
                      cross_slot.crossing_index = (guint) crossing_index;
                      g_array_append_val (con_slot.crossings, cross_slot);
                    }
                  else
                    {
                      g_assert_not_reached ();
                    }
                }
            }
        done:
          i++;
        }
      g_array_append_val (constrained_slots, con_slot);
    }

  return constrained_slots;
}

/* calculates the top bits of the con_slot. This is fast */
static void
calc_score_letters (ConstrainedSlot *con_slot,
                    IpuzGuesses     *guesses)
{
  IpuzCellCoord coord;
  guint n_known = 0;
  guint n_unknown = 0;
  guint i = 0;
  guint32 score = 0;

  while (ipuz_cell_coord_array_index (con_slot->slot, i++, &coord))
    {
      if (ipuz_guesses_get_guess (guesses, &coord))
        n_known++;
      else
        n_unknown++;
    }

  /* Skip any slots that have all their cells filled in. We force
   * their score to be 0. */
  if (n_unknown == 0)
    {
      con_slot->score = 0;
      return;
    }

  score = (n_known & SCORE_LETTER_MASK);
  score = score << 5;
  score |= SCORE_LETTER_MASK - (n_unknown & SCORE_LETTER_MASK);
  score = score << 21;

  con_slot->score = score;
}

static void
calc_score_words_and_crossings (ConstrainedSlot *con_slot,
                                IpuzGuesses     *guesses)
{
  g_autofree gchar *filter = NULL;

  filter = constrained_slot_get_filter (con_slot, guesses);
  word_list_set_filter (con_slot->word_list, filter, WORD_LIST_MATCH);

  con_slot->score |=
    (SCORE_CANDIDATES_MASK - ((word_list_get_n_items (con_slot->word_list) & SCORE_CANDIDATES_MASK)));
  con_slot->score = con_slot->score << 5;
  con_slot->score |= (con_slot->crossings->len & SCORE_LETTER_MASK);
}

/* This will find the best possible candidate for using next when
 * solving. It uses the following heuristic for deciding which is
 * best. If there's a straight-up tie, an arbitrary one is picked.
 *
 * See above for how the score is calculated and stored.
 */
ConstrainedSlot *
constrained_slots_get_best (ConstrainedSlots *con_slots,
                            IpuzGuesses      *guesses)
{
  ConstrainedSlot *max_slot = NULL;
  gboolean need_second_pass = FALSE;
  guint letter_max_score = 0;
  guint max_score = 0;

  g_assert (con_slots);
  g_assert (constrained_slots_len (con_slots) > 0);

  /* we do a two-pass method to find the best slot to use. First, we
   * calculate the letter portion of the score and see if we get a
   * winner from that. That's fast. If we have a tie, then we do a
   * second pass and check the word-list and crossings. */
  for (guint i = 0; i < constrained_slots_len (con_slots); i++)
    {
      ConstrainedSlot *con_slot = &(constrained_slots_index (con_slots, i));

      calc_score_letters (con_slot, guesses);
      if (con_slot->score == 0)
        continue;
      else if (con_slot->score > letter_max_score)
        {
          letter_max_score = con_slot->score;
          max_slot = con_slot;
          need_second_pass = FALSE;
        }
      else if (con_slot->score == letter_max_score)
        {
          max_slot = NULL;
          need_second_pass = TRUE;
        }
    }

  if (need_second_pass)
    {
      for (guint i = 0; i < constrained_slots_len (con_slots); i++)
        {
          ConstrainedSlot *con_slot = &(constrained_slots_index (con_slots, i));

          /* Only calculate the words and crossings score if we have a
           * possible candidate for the highest score. */
          if (con_slot->score == letter_max_score)
            calc_score_words_and_crossings (con_slot, guesses);

          if (con_slot->score > max_score)
            {
              max_score = con_slot->score;
              max_slot = con_slot;
            }
        }
    }

  return max_slot;
}

gboolean
constrained_slots_validate (ConstrainedSlots *con_slots,
                            IpuzGuesses      *guesses)
{
  for (guint i = 0; i < constrained_slots_len (con_slots); i++)
    {
      ConstrainedSlot con_slot;
      g_autofree gchar *filter = NULL;

      con_slot = constrained_slots_index (con_slots, i);
      filter = slot_to_filter (con_slot.slot, guesses);
      word_list_set_filter (con_slot.word_list, filter, WORD_LIST_MATCH);
      if (word_list_get_n_items (con_slot.word_list) == 0)
        return FALSE;
    }
  return TRUE;
}

void
constrained_slots_unref (ConstrainedSlots *con_slots)
{
  g_array_unref ((GArray *) con_slots);
}

static gchar *
slot_to_filter (IpuzCellCoordArray *slot,
                IpuzGuesses        *overlay)
{
  IpuzCellCoord coord;
  guint i = 0;
  GString *filter;

  filter = g_string_new (NULL);

  while (ipuz_cell_coord_array_index (slot, i++, &coord))
    {
      const gchar *guess;

      guess = ipuz_guesses_get_guess (overlay, &coord);
      if (guess)
        g_string_append (filter, guess);
      else
        g_string_append (filter, "?");
    }

  return g_string_free_and_steal (filter);
}

gchar *
constrained_slot_get_filter (ConstrainedSlot *con_slot,
                             IpuzGuesses     *overlay)
{
  return slot_to_filter (con_slot->slot, overlay);
}

gboolean
constrained_slot_check_crosses (ConstrainedSlot  *con_slot,
                                IpuzGuesses      *overlay)
{
  guint i = 0;

  for (i = 0; i < con_slot->crossings->len; i++)
    {
      CrossingSlot cross;
      g_autofree gchar *filter = NULL;

      cross = g_array_index (con_slot->crossings, CrossingSlot, i);
      filter = slot_to_filter (cross.slot, overlay);
      word_list_set_filter (con_slot->word_list, filter, WORD_LIST_MATCH);

      if (word_list_get_n_items (con_slot->word_list) == 0)
        return FALSE;
    }

  return TRUE;
}

/* Calculates the score of a word as per the algorithm described at
 * the top of the file.
 *
 * This function is super complex. There's an inner loop that iterates
 * through four things simultaneously:
 *
 * It iterates through word, and filter at the same time. It also
 * keeps track of the index in the slot that the letters are
 * at. Simultaneously, it keeps the next available crossing slot to
 * see if the current index needs calculating.
 */
gfloat
calculate_word_score (ConstrainedSlot *con_slot,
                      const gchar     *word,
                      const gchar     *filter)
{
  gfloat score = 0.0;
  const gchar *w_ptr, *f_ptr;
  guint index = 0;
  guint crossing_index = 0;
  guint filter_len;


  if (con_slot->crossings == NULL)
    return score;


  w_ptr = word; f_ptr = filter;
  filter_len = strlen (filter);

  /* these should never fail */
  g_assert (word != NULL);
  g_assert (filter != NULL);
  g_assert (strlen (word) == filter_len);

  while (w_ptr[0] != '\0' && f_ptr[0] != '\0')
    {
      CrossingSlot crossing_slot;

      if (crossing_index > con_slot->crossings->len)
        break;
      crossing_slot = g_array_index (con_slot->crossings, CrossingSlot, crossing_index);

      /* iff we have an empty cell at index*/
      if (f_ptr[0] == '?' &&
          crossing_slot.index == index)
        {
          gunichar c;
          guint crossing_slot_len;

          c = g_utf8_get_char (w_ptr);
          crossing_slot_len = ipuz_cell_coord_array_len (crossing_slot.slot);
          score += word_list_get_char_frequency (con_slot->word_list,
                                                 crossing_slot_len,
                                                 crossing_slot.crossing_index,
                                                 c);
        }

      /* increment everything in the loop */
      /* First, if crossing_slot.index == index, then the current index
       * has a cross and we looked at it. We want to consider the next
       * cross */
      if (crossing_slot.index == index)
        crossing_index++;
      index++;
      w_ptr = g_utf8_next_char (w_ptr);
      f_ptr = g_utf8_next_char (f_ptr);
    }

  return score;
}

static gint
mapping_array_sort_fn (gconstpointer a,
                       gconstpointer b)
{
  const ConstrainedSlotMap *map_a = a;
  const ConstrainedSlotMap *map_b = b;

  if (map_a->score > map_b->score)
    return -1;
  else if (map_b->score > map_a->score)
    return 1;
  return 0;
}

GArray *
constrained_slot_get_mapping (ConstrainedSlot *con_slot,
                              IpuzGuesses     *guesses)
{
  g_autofree gchar *filter = NULL;
  GArray *mapping_array;

  filter = constrained_slot_get_filter (con_slot, guesses);
  mapping_array = g_array_new (FALSE, FALSE, sizeof (ConstrainedSlotMap));

  word_list_set_filter (con_slot->word_list, filter, WORD_LIST_MATCH);

  g_array_set_size (mapping_array, word_list_get_n_items (con_slot->word_list));

  for (guint i = 0; i < mapping_array->len; i++)
    {
      ConstrainedSlotMap *map;

      map = &(g_array_index (mapping_array, ConstrainedSlotMap, i));

      map->word = word_list_get_word (con_slot->word_list, i);
      map->score = calculate_word_score (con_slot, map->word, filter);
    }

  g_array_sort (mapping_array, mapping_array_sort_fn);

  return mapping_array;
}
