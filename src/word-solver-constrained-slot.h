/* word-solver-constrained-slot.h
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <libipuz/libipuz.h>
#include "cell-array.h"
#include "word-list.h"

G_BEGIN_DECLS


typedef struct
{
  const gchar *word;
  gdouble score;
} ConstrainedSlotMap;

typedef struct
{
  IpuzCellCoordArray *slot;  /* No ref held */
  guint index; /* character index of the slot that the crossing slot is locaed in */
  guint crossing_index; /* character index of the crossing slot that it crosses the slot */
} CrossingSlot;

typedef struct
{
  IpuzCellCoordArray *slot;  /* No ref held */
  guint32 score;
  GArray *crossings;         /* Array of CrossingSlots */
  WordList *word_list;
} ConstrainedSlot;

/* GArray of type ConstrainedSlot */
typedef GArray ConstrainedSlots;

/* ConstraintedSlots */
ConstrainedSlots *constrained_slots_create       (WordListResource *resource,
                                                  IpuzCrossword    *xword,
                                                  CellArray        *selected_cells);
ConstrainedSlot  *constrained_slots_get_best     (ConstrainedSlots *con_slots,
                                                  IpuzGuesses      *guesses);
gboolean          constrained_slots_validate     (ConstrainedSlots *con_slots,
                                                  IpuzGuesses      *guesses);
void              constrained_slots_unref        (ConstrainedSlots *con_slots);

/* ConstraintedSlot (without the s) */
gchar            *constrained_slot_get_filter    (ConstrainedSlot  *con_slot,
                                                  IpuzGuesses      *guesses);
gboolean          constrained_slot_check_crosses (ConstrainedSlot  *con_slot,
                                                  IpuzGuesses      *guesses);
GArray           *constrained_slot_get_mapping   (ConstrainedSlot  *con_slot,
                                                  IpuzGuesses      *guesses);


#define constrained_slots_len(cs)        (((GArray*) cs)->len)
#define constrained_slots_index(cs,i)    (g_array_index((GArray*)cs,ConstrainedSlot,i))
#define constrained_slots_ref(cs)        (g_array_ref((GArray*)cs))


G_DEFINE_AUTOPTR_CLEANUP_FUNC(ConstrainedSlots, constrained_slots_unref);


G_END_DECLS
