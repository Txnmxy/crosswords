/* word-solver-task.c
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/*
 * Code based on the betterfill algorithm developed by Sebastian
 * Migniot.
 *
 * See https://github.com/smigniot/MotsCroises/ for more information.
 */
/*
 * A note on terminology. A slot here refers to an IpuzCellCoordArray
 * associated with a clue. We don't call this a slot anywhere else in
 * this code base, but in the interest of keeping in sync with the
 * algorithm we use that terminology
 */


#include "word-solver-task.h"
#include "word-solver-constrained-slot.h"
#include "word-list.h"


struct _WordSolverTask
{
  PuzzleTask parent_object;

  WordListResource *resource;

  /* Owned by the WordSolver. */
  gint *count; /* (atomic) */
  GMutex *results_mutex;
  GArray *results;

  /* Options for the run */
  IpuzCrossword *xword;
  gint max_results;
  gint min_priority;
  GCancellable *cancellable;
  gboolean clear_on_finish;

  /* Data for the run  */
  ConstrainedSlots *con_slots;
  IpuzGuesses *overlay;
  GHashTable *skip_table;
};


static void word_solver_task_init         (WordSolverTask      *self);
static void word_solver_task_class_init   (WordSolverTaskClass *klass);
static void word_solver_task_dispose      (GObject             *object);
static void word_solver_task_set_run_info (PuzzleTask          *self,
                                           gint                *count,
                                           GMutex              *results_mutex,
                                           GArray              *results);
static void word_solver_task_run          (GTask               *task,
                                           GObject             *source_object,
                                           gpointer             task_data,
                                           GCancellable        *cancellable);
static void word_solver_task_run_helper   (WordSolverTask      *self);


G_DEFINE_TYPE (WordSolverTask, word_solver_task, PUZZLE_TYPE_TASK);


static void
result_clear (IpuzGuesses **result)
{
  g_clear_pointer (result, ipuz_guesses_unref);
}

static void
word_solver_task_init (WordSolverTask *self)
{
  self->max_results = -1;
  self->min_priority = 50;

  puzzle_task_set_result_clear (PUZZLE_TASK (self), (GDestroyNotify) result_clear);
}

static void
word_solver_task_class_init (WordSolverTaskClass *klass)
{
  GObjectClass *object_class;
  PuzzleTaskClass *task_class;

  object_class = G_OBJECT_CLASS (klass);
  task_class = PUZZLE_TASK_CLASS (klass);

  object_class->dispose = word_solver_task_dispose;
  task_class->set_run_info = word_solver_task_set_run_info;
  task_class->run = word_solver_task_run;
}

static void
word_solver_task_dispose (GObject *object)
{
  WordSolverTask *self;

  self = WORD_SOLVER_TASK (object);

  g_clear_object (&self->xword);
  g_clear_object (&self->resource);
  g_clear_object (&self->cancellable);
  g_clear_pointer (&self->overlay, ipuz_guesses_unref);
  g_clear_pointer (&self->con_slots, constrained_slots_unref);

  G_OBJECT_CLASS (word_solver_task_parent_class)->dispose (object);
}


static void
word_solver_task_set_run_info (PuzzleTask          *self,
                               gint                *count,
                               GMutex              *results_mutex,
                               GArray              *results)
{
  WORD_SOLVER_TASK (self)->count = count;
  WORD_SOLVER_TASK (self)->results_mutex = results_mutex;
  WORD_SOLVER_TASK (self)->results = results;
}

/* This is run in a separate thread */
static void
word_solver_task_run (GTask        *task,
                      GObject      *source_object,
                      gpointer      task_data,
                      GCancellable *cancellable)
{
  WordSolverTask *self = WORD_SOLVER_TASK (task_data);

  g_return_if_fail (WORD_IS_SOLVER_TASK (self));

  self->cancellable = g_object_ref (cancellable);

  word_solver_task_run_helper (self);

  g_task_return_pointer (task, NULL, NULL);
}

static IpuzGuesses *
create_initial_overlay (IpuzCrossword *xword)
{
  IpuzGuesses *overlay;
  guint row, column;

  overlay = ipuz_grid_create_guesses (IPUZ_GRID (xword));

  for (row = 0; row < ipuz_guesses_get_height (overlay); row++)
    {
      for (column = 0; column < ipuz_guesses_get_width (overlay); column++)
        {
          IpuzCellCoord coord = { .row = row, .column = column };
          IpuzCell *cell = ipuz_grid_get_cell (IPUZ_GRID (xword), &coord);
          const gchar *solution;

          if (IPUZ_CELL_IS_GUESSABLE (cell))
            {
              solution = ipuz_cell_get_solution (cell);
              if (solution)
                ipuz_guesses_set_guess (overlay,
                                        &coord,
                                        solution);
            }
          else if (IPUZ_CELL_IS_INITIAL_VAL (cell))
            {
              solution = ipuz_cell_get_initial_val (cell);
              if (solution)
                ipuz_guesses_set_guess (overlay,
                                        &coord,
                                        solution);
            }
        }
    }

  return overlay;
}

static void
write_guess_to_overlay (IpuzCellCoordArray *slot,
                        const gchar        *guess,
                        IpuzGuesses        *overlay)
{
  IpuzCellCoord coord;
  guint i = 0;
  const gchar *ptr = guess;
  gchar buf[7];

  g_assert (guess != NULL);

  while (ipuz_cell_coord_array_index (slot, i++, &coord))
    {
      g_assert (*ptr);
      if (ptr[0] == '?')
        {
          ipuz_guesses_set_guess (overlay, &coord, NULL);
        }
      else
        {
          g_utf8_strncpy (buf, ptr, 1);
          ipuz_guesses_set_guess (overlay, &coord, buf);
        }

      ptr = g_utf8_next_char (ptr);
    }
}

/* FIXME(refactor): move this to PuzzleTask */
static void
add_solution (WordSolverTask *self)
{
  gint len; /* int, not guint */

  g_atomic_int_inc (self->count);

  g_mutex_lock (self->results_mutex);
  if (!g_cancellable_is_cancelled (self->cancellable))
    {
      IpuzGuesses *new_overlay;
      new_overlay = ipuz_guesses_copy (self->overlay);
      g_array_append_val (self->results, new_overlay);
    }
  len = (gint) self->results->len;
  if (self->max_results >= 0 &&
      self->max_results <= len)
    g_cancellable_cancel (self->cancellable);
  g_mutex_unlock (self->results_mutex);
}

static void
word_solver_task_run_helper (WordSolverTask *self)
{
  g_autofree gchar *best_slot_filter = NULL;
  ConstrainedSlot *best_slot;

  g_autoptr (GArray) mapping_array = NULL;

  if (g_cancellable_is_cancelled (self->cancellable))
    return;

  /* Passed in a list of open slots. Search for the highest scoring
   * slot in the list.
   * Get a copy of the word at that point.
   * Find a list of possible words to fit that spot
   * Sort by frequency
   * Go through the words and try them
   * make sure the acrosses have a word that fits
   * Restore the old word to guesses
   */
  best_slot = constrained_slots_get_best (self->con_slots, self->overlay);

  /* If we can't find any slots left to use, we found grid! */
  if (best_slot == NULL)
    {
      add_solution (self);
      return;
    }

  /* We keep the filter around so we can restore the overlay to how we
   * found it when we're done */
  best_slot_filter = constrained_slot_get_filter (best_slot, self->overlay);

  mapping_array = constrained_slot_get_mapping (best_slot, self->overlay);

  /* Run the inner loop */
  for (guint i = 0; i < mapping_array->len; i++)
    {
      ConstrainedSlotMap map;
      gint priority;

      if (g_cancellable_is_cancelled (self->cancellable))
        return;

      map = g_array_index (mapping_array, ConstrainedSlotMap, i);
      priority = word_list_get_priority (best_slot->word_list, i);

      /* Skip words that have too low a priority or we've already
       * included */
      if (priority < self->min_priority)
        continue;
      if (g_hash_table_contains (self->skip_table, map.word))
        continue;

      g_hash_table_insert (self->skip_table,
                           g_strdup (map.word),
                           GINT_TO_POINTER (TRUE));
      write_guess_to_overlay (best_slot->slot, map.word, self->overlay);

      /* Check the crossing slots to see if we have a valid word. If
       * so, recurse. */
      if (constrained_slot_check_crosses (best_slot, self->overlay))
        word_solver_task_run_helper (self);

      /* restore the state */
      g_hash_table_remove (self->skip_table, map.word);
    }

  /* Undo the guess in the overlay */
  write_guess_to_overlay (best_slot->slot, best_slot_filter, self->overlay);

}

static void
skip_table_foreach (IpuzClues         *clues,
                    IpuzClueDirection  direction,
                    IpuzClue          *clue,
                    IpuzClueId        *clue_id,
                    gpointer           user_data)
{
  GHashTable *skip_table = (GHashTable *)user_data;
  GString *word;

  word = g_string_new (NULL);

  for (guint i = 0; i < ipuz_clue_get_n_coords (clue); i++)
    {
      IpuzCellCoord coord;
      IpuzCell *cell;
      const gchar *solution;

      ipuz_clue_get_coord (clue, i, &coord);
      cell = ipuz_grid_get_cell (IPUZ_GRID (clues), &coord);
      solution = ipuz_cell_get_solution (cell);
      if (solution == NULL || solution[0] == '\0')
        {
          g_string_free (word, TRUE);
          return;
        }
      g_string_append (word, solution);
    }

  g_hash_table_insert (skip_table,
                       g_string_free_and_steal (word),
                       GINT_TO_POINTER (TRUE));
}

static GHashTable *
create_skip_table (WordListResource *resource,
                   WordSolverTask   *self,
                   WordArray        *skip_list)
{
  g_autoptr (WordList) word_list = NULL;
  GHashTable *skip_table;

  /* Just for looking up the words in the WordArray */
  word_list = word_list_new ();
  word_list_set_resource (word_list, resource);

  skip_table =
    g_hash_table_new_full (g_str_hash, g_str_equal,
                           g_free, NULL);


  /* First, populate the skip table with completed words in the puzzle */
  ipuz_clues_foreach_clue (IPUZ_CLUES (self->xword),
                           skip_table_foreach,
                           skip_table);

  /* Now include the skip table */
  for (guint i = 0; i < skip_list->len; i++)
    {
      WordIndex index;
      const gchar *key;

      index = word_array_index (skip_list, i);
      key = word_list_get_indexed_word (word_list, index);

      g_hash_table_insert (skip_table,
                           g_strdup (key),
                           GINT_TO_POINTER (TRUE));
    }

  return skip_table;
}

WordSolverTask *
word_solver_task_new (IpuzCrossword    *xword,
                      WordListResource *resource,
                      CellArray        *selected_cells,
                      WordArray        *skip_list,
                      gint              max_results,
                      gint              min_priority)
{
  WordSolverTask *self;

  /* FIXME(directions): warn if called on a crossword without
   * down/across semantics
   */
  self = g_object_new (WORD_TYPE_SOLVER_TASK, NULL);
  self->max_results = max_results;
  self->min_priority = min_priority;
  self->resource = g_object_ref (resource);

  /* We count on the xword not changing while we're running. If that
   * fact changes this will have to be duped. */
  self->xword = g_object_ref (xword);
  self->overlay = create_initial_overlay (self->xword);
  self->con_slots = constrained_slots_create (resource, xword, selected_cells);
  self->skip_table = create_skip_table (resource, self, skip_list);

  return self;
}
