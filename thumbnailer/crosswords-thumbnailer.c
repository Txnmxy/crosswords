/* crosswords-thumbnailer.c
 *
 * Copyright 2023 Tanmay Patil <tanmaynpatil105@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "crosswords-config.h"
#include "crosswords-misc.h"

#include <locale.h>

#include <libipuz/libipuz.h>
#include <cairo-svg.h>
#include <glib/gi18n-lib.h>

#define THUMBNAIL_SIZE 128

static gint size = THUMBNAIL_SIZE;
static gboolean export_svg = FALSE;
static const gchar **file_arguments;

static const GOptionEntry goption_options[] = {
  { "size", 's', 0, G_OPTION_ARG_INT, &size, NULL, "SIZE" },
  { "svg",  'v', 0, G_OPTION_ARG_NONE, &export_svg, NULL, NULL},
  { G_OPTION_REMAINING, 0, 0, G_OPTION_ARG_FILENAME_ARRAY, &file_arguments, NULL, "<input> <ouput>" },
  { NULL }
};

static void
print_usage (GOptionContext *context)
{
  g_autofree gchar* help = NULL;

  help = g_option_context_get_help (context, TRUE, NULL);
  g_print ("%s", help);
}

static void
crosswords_thumbnail_svg_get (IpuzPuzzle  *puzzle,
                              const gchar *output,
                              GError      *error)
{
  cairo_surface_t *surf;
  cairo_t *cr;
  GridLayout *layout;
  GridState *state;
  RsvgHandle *handle;

  surf = cairo_svg_surface_create (output, size, size);
  cr = cairo_create (surf);

  state = grid_state_new (IPUZ_CROSSWORD (puzzle), NULL, GRID_STATE_VIEW);

  LayoutConfig config = {
    .border_size = 1,
    .base_size = 3,
  };
  layout = grid_layout_new (state, config);

  handle = svg_handle_new_from_layout (layout, coloring_from_adwaita ());
  RsvgRectangle viewport = { 0.0, 0.0, size, size};

  if (rsvg_handle_render_document (handle, cr, &viewport, &error))
    {
       cairo_surface_flush(surf);
       cairo_surface_finish(surf);
    }

  cairo_destroy (cr);
  cairo_surface_destroy (surf);
  g_object_unref (handle);
  grid_layout_free (layout);
  grid_state_free (state);
}

static gboolean
crosswords_thumbnail_pngenc_get (IpuzPuzzle  *puzzle,
                                 const gchar *output,
                                 GError      *error)
{
  cairo_surface_t *surf;
  cairo_t *cr;
  GridLayout *layout;
  GridState *state;
  RsvgHandle *handle;
  gboolean res = FALSE;

  surf = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, size, size);
  cr = cairo_create (surf);

  state = grid_state_new (IPUZ_CROSSWORD (puzzle), NULL, GRID_STATE_VIEW);

  LayoutConfig config = {
    .border_size = 1,
    .base_size = 3,
  };
  layout = grid_layout_new (state, config);

  handle = svg_handle_new_from_layout (layout, coloring_from_adwaita ());
  RsvgRectangle viewport = { 0.0, 0.0, size, size};

  if (rsvg_handle_render_document (handle, cr, &viewport, &error))
    {
      if (cairo_surface_write_to_png (surf, output) == CAIRO_STATUS_SUCCESS)
        res = TRUE;
    }

  cairo_destroy (cr);
  cairo_surface_destroy (surf);
  g_object_unref (handle);
  grid_layout_free (layout);
  grid_state_free (state);

  return res;
}

int
main (int   argc,
      char *argv[])
{
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  g_autoptr (GError)  error = NULL;
  g_autoptr (GOptionContext) context = NULL;
  const gchar *input;
  const gchar *output;

  setlocale (LC_ALL, "");

  context = g_option_context_new ("- GNOME Crosswords Thumbnailer");
  g_option_context_add_main_entries (context, goption_options, NULL);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      print_usage (context);
      return -1;
    }

  input = file_arguments ? file_arguments[0] : NULL;
  output = input ? file_arguments[1] : NULL;

  if (!input || !output)
    {
      print_usage (context);
      return -1;
    }

  puzzle = ipuz_puzzle_new_from_file (input, &error);
  if (error != NULL)
    {
      g_printerr ("Error loading puzzle: %s\n", error->message);
      return -1;
    }

  if (export_svg == TRUE)
    {
      crosswords_thumbnail_svg_get (puzzle, output, error);
      if (error != NULL)
        g_printerr ("Error creating thumbnail: %s\n", error->message);
      return -1;
    }
  else if (!crosswords_thumbnail_pngenc_get (puzzle, output, error))
    {
      if (error != NULL)
        g_printerr ("Error creating thumbnail: %s\n", error->message);
      return -1;
    }

  return 0;
}
