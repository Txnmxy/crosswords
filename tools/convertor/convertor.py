#!/usr/bin/python
import regex
from bs4 import BeautifulSoup
from abc import ABC, abstractmethod
import warnings
warnings.filterwarnings("ignore", message='.*looks more like a filename.*', module='bs4')

class Convertor(ABC):
    EMPTY_CHAR=0
    BLOCK_CHAR='#'
    DIRECTIONS=('Across', 'Down', 'Diagonal', 'Diagonal Up', 'Diagonal Down Left', 'Diagonal Up Left', 'Zones')

    def __init__(self):
        self.has_enumerations = False

    @abstractmethod
    def convert_to_ipuz(self):
        pass

    @abstractmethod
    def convert_from_ipuz(self):
        pass

    def extract_enumeration(self, clue_text):
        enumeration_text = None
        # FIXME: Make this match ipuz-enumeration.c. This regex misses
        # enumerations like (7-)
        s = regex.search (r'\(\d+([ ,\'-.]\d+)*\)*$', clue_text)
        if s:
            # remove the enumeration from the clue text
            clue_text = clue_text[0:s.start()].strip ()
            enumeration_text = s.captures()[0][1:-1].replace (',', ' ')
        if enumeration_text:
            self.has_enumerations = True
        return (clue_text, enumeration_text)

    def clean_html(self, text):
        #Handle some tricky unicode entities
        text = text.replace ('\u0085', '...')
        text = text.replace ('\u0091', '`')
        text = text.replace ('\u0092', '\'')
        text = text.replace ('\u0093', '"')
        text = text.replace ('\u0094', '"')
        text = text.replace ('\u0096', '–')
        text = text.replace ('\n', '<br>')
        return str(BeautifulSoup(text, "html.parser"))

    def build_grid(self, width, height):
        grid = []
        for i in range (0, height):
            row = [ None ] * width
            grid.append (row)
        return grid

    def ipuz_assert(self, assertion, message="Badly formed jpz file"):
        if not assertion:
            print (message)
            exit(1)


class TestConvertor(Convertor):
    def convert_to_ipuz(self):
        pass
    def convert_from_ipuz(self):
        pass
    def test_clean (self, p, r):
        result = self.clean_html(p)
        if not result == r:
            print ('expected \"%s\" and got \"%s\"' % (r, result))
            raise AssertionError

if __name__ == "__main__":
    c = TestConvertor ()
    try:
        c.test_clean("cats and dogs", "cats and dogs")
        c.test_clean("cats & dogs", "cats &amp; dogs")
        c.test_clean("cats < dogs", "cats &lt; dogs")
        c.test_clean("cats &amp; dogs", "cats &amp; dogs")
        c.test_clean("<b>cats</b> &amp; <i>dogs</i>", "<b>cats</b> &amp; <i>dogs</i>")
        c.test_clean("cats \u0093and\u0094 dogs", "cats \"and\" dogs")
    except AssertionError:
        exit (1)
    exit (0)

