import re
from sys import stderr
from html import escape
from convertor import Convertor
import xdfile

class XDConvertor(Convertor):
    EXTENSIONS=['.xd']

    def __init__(self, src):
        super().__init__()
        self.puzzle = xdfile.xdfile(src.decode('utf-8'), pubid='unknown')

    def convert_to_ipuz(self):
        self.ipuz_dict = {}
        self.ipuz_dict['version'] = 'http://ipuz.org/v2'
        self.ipuz_dict['kind'] = ['http://ipuz.org/crossword#1']
        self.ipuz_dict['dimensions'] = {
            'width': self.puzzle.width(),
            'height': self.puzzle.height(),
        }
        self.ipuz_dict['block'] = self.BLOCK_CHAR
        self.ipuz_dict['empty'] = self.EMPTY_CHAR

        special_type = self.puzzle.get_header('Special')
        if special_type not in ['', 'circle', 'shaded']:
            print(f'Warning: Puzzle has unsupported special type ("{special_type}"). Some information will be lost.', file=stderr)

        for key in 'Title', 'Author', 'Editor', 'Copyright':
            header = self.puzzle.get_header(key)
            if len(header) > 0:
                self.ipuz_dict[key.lower()] = escape(header)

        # xdfile collapses runs of multiple newlines in the notes to a single
        # newline
        notes = self.puzzle.notes.strip()
        if notes != '':
            self.ipuz_dict['notes'] = escape(notes).replace('\n', '<br/>')

        date_parts = self.puzzle.get_header('Date').split('-')
        if len(date_parts) == 3:
            # xd dates are "YYYY-MM-DD", ipuz dates are "MM/DD/YYYY"
            date = f'{date_parts[1]}/{date_parts[2]}/{date_parts[0]}'
            self.ipuz_dict['date'] = date

        clues = { 'Across': [], 'Down': [] }
        clue_keys = { 'A': 'Across', 'D': 'Down' }
        for clue in self.puzzle.clues:
            if not clue[0][0]:
                # empty clue -- xdfile does not filter out the empty lines
                # between clue direction groups
                continue
            direction = clue_keys[clue[0][0]]
            number = clue[0][1]
            # xd clues support {/italic/}, {*bold*}, {_underscore_}, and
            # {-strikethrough-} markup.
            text = escape(clue[1])
            text = re.sub(r'{/([^{}/]*)/}', r'<i>\1</i>', text)
            text = re.sub(r'{\*([^{}\*]*)\*}', r'<b>\1</b>', text)
            text = re.sub(r'{_([^{}_]*)_}', r'<u>\1</u>', text)
            text = re.sub(r'{-([^{}-]*)-}', r'<s>\1</s>', text)
            clues[direction].append([number, text])
        self.ipuz_dict['clues'] = clues

        rebus = self.puzzle.rebus()
        puzzle = []
        solution = []
        clue_grid = self.puzzle.numberedPuzzle()
        for y, line in enumerate(self.puzzle.grid):
            puzzle_row = []
            solution_row = []
            for x, character in enumerate(line):
                if character == '_':
                    value = None
                    cell_type = None
                elif character == '#':
                    value = self.BLOCK_CHAR
                    cell_type = self.BLOCK_CHAR
                else:
                    # A crossword puzzle with "FOO" in a single cell will have
                    # a "Rebus: 1=FOO" header, and use "1" as the cell's value
                    # to denote that it is a rebus.
                    if character in rebus:
                        character = rebus[character]
                    value = character
                    # Value of clue_grid[y][x] is None for a regular cell,
                    # and a clue number if that cell is the start of a clue.
                    cell_type = self.EMPTY_CHAR
                    if clue_grid[y][x] is not None:
                        cell_type = clue_grid[y][x]
                    # In xd, lowercase letters in the grid mark that cell as
                    # "special". Puzzles can be either "circle" or "shaded"
                    # (not both).
                    if value.islower():
                        value = value.upper()
                        if special_type == 'circle':
                            cell_type = {
                                'cell': cell_type,
                                'style': { 'shapebg': 'circle' },
                            }
                        else: # "shaded"
                            cell_type = {
                                'cell': cell_type,
                                'style': { 'highlight': True },
                            }
                puzzle_row.append(cell_type)
                solution_row.append(value)
            puzzle.append(puzzle_row)
            solution.append(solution_row)
        self.ipuz_dict['puzzle'] = puzzle
        self.ipuz_dict['solution'] = solution

        return self.ipuz_dict

    def convert_from_ipuz(self):
        pass
