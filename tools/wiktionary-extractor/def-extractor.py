#!/usr/bin/python
import os
import sys
import json
import argparse
import time
from operator import attrgetter, itemgetter

import gi
gi.require_version('GLib', '2.0')
from gi.repository import GLib

from wordentry import Word
from wordentry import Entry
from wordentry import Sense
from wordentry import get_tags_table, get_pos_table
import enumtable
import unicodedata

# Language settings. To be internationalized later
LANG_CODE='en'
ALPHABET="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

# For parsing words
MIN_WORD_LENGTH = 1
MAX_WORD_LENGTH = 21
WORD_THRESHOLD  = 50


# The working directory
#
# NOTE: we read and write here because the end result is checked into
# git, and not part of a common build process
WORD_LISTS_DIR = "../../word-lists/"

# Raw sources of data. These are downloaded
RAW_DEFINITIONS = WORD_LISTS_DIR + "raw-wiktextract-data.jsonl"
WORDNIK_LIST = WORD_LISTS_DIR + "wordlist-20210729.txt"
BRODA_LIST = WORD_LISTS_DIR + "peter-broda-wordlist__full-text__scored.txt"
TEST_LIST = WORD_LISTS_DIR + "test-words.txt"

# Output files when conbined with the `wordlist_source` variable
FILTERED_RAW_DEFINITIONS = WORD_LISTS_DIR + "%s-filtered-wiktextract-data.jsonl"
ENUM_INDEX = WORD_LISTS_DIR + "%s-enum-index.json"
GVARIANT_DEFINITIONS = WORD_LISTS_DIR + "%s-gvariant-defs.data"
GVARIANT_INDEX = WORD_LISTS_DIR + "%s-gvariant-index.data"

SOURCE_HASH = {}
WORD_HASH = {}

# Importers. Load a word list into SOURCE_HASH

def strip_accents(text):
    return ''.join(c for c in unicodedata.normalize('NFD', text)
                   if unicodedata.category(c) != 'Mn')

# NOTE: This is a reimplementation of the parser in
# src/gen-word-list-importer.c. Keep it in sync with
# scored_parse_word() in that file
def scored_parse_word (word):
    word = strip_accents (word)
    retval = ''
    for c in word:
        c = c.upper()
        if c.isdigit():
            return None
        elif c == '&':
            retval += "AND"
        elif c in ALPHABET:
            retval += c
    if len (retval) > MAX_WORD_LENGTH or len (retval) < MIN_WORD_LENGTH:
        return None
    return retval

def load_wordnik():
    with open(WORDNIK_LIST) as file:
        for line in file:
            word = scored_parse_word(line)
            if not word:
                continue
            SOURCE_HASH [word] = 1

def load_broda():
    with open(BRODA_LIST) as file:
        for line in file:
            (word, priority) = line.split ('::')
            if int (priority) < WORD_THRESHOLD:
                continue
            word = scored_parse_word (word)
            if not word:
                continue
            SOURCE_HASH [word] = 1

def load_test():
    with open(TEST_LIST) as file:
        for line in file:
            (word, priority) = line.split ('::')
            if int (priority) < WORD_THRESHOLD:
                continue
            word = scored_parse_word (word)
            if not word:
                continue
            SOURCE_HASH [word] = 1

# Write a list of all the json lines filtered by the current loaded
# word list
def write_filtered_raw_list (word_list_source, max_count=None):
    filtered_raw_definitions = FILTERED_RAW_DEFINITIONS % word_list_source
    count = 0
    print (f"Filtering {RAW_DEFINITIONS}")
    with open (filtered_raw_definitions, "w") as filtered_file:
        with open(RAW_DEFINITIONS) as raw_file:
            for line in raw_file:
                j = json.loads (line)
                if 'word' in j:
                    word = j['word']
                    word = scored_parse_word (word)
                    if not word:
                        continue
                else:
                    continue

                try:
                    # filter out all words of a different language
                    lang_code = j['lang_code']
                    if not lang_code == LANG_CODE:
                        continue
                except:
                    continue
                if word in SOURCE_HASH:
                    filtered_file.write (line)
                    count += 1
                if max_count and count > max_count:
                    break
    print (f"Wrote {filtered_raw_definitions}")

def find_word_in_hash(word_str):
    try:
        word_list = WORD_HASH[scored_parse_word (word_str)]
    except KeyError:
        return None
    for word in word_list:
        if word.word_str == word_str:
            return word
    return None

def add_word_to_hash(word):
    try:
        word_list = WORD_HASH[scored_parse_word (word.word_str)]
    except KeyError:
        WORD_HASH[scored_parse_word (word.word_str)] = [word, ]
    else:
        word_list.append (word)

def create_entry_list (word_list_source, max_count=None):
    filtered_raw_definitions = FILTERED_RAW_DEFINITIONS % word_list_source
    count=0
    print (f"Building the list of definitions for {word_list_source}")
    with open (filtered_raw_definitions) as filtered_file:
        for line in filtered_file:
            new_word = False

            j = json.loads (line)
            word_str = j['word']
            word = find_word_in_hash(word_str)
            if not word:
                word = Word (word_str)
                new_word = True

            word.parse_entry (j)

            if word.validate ():
                if new_word:
                    add_word_to_hash (word)

# FIXME: Skip printing this out for now. We need to investigate it further though
#            else:
#                print (f"Skipping {word_str}", file=sys.stderr)

            if max_count and count >= max_count:
                break;
            count += 1


# Keep in sync with glib's version of g_str_hash (djb2)
# This hasn't changed in 20 years though, so should remain stable
def g_str_hash(string: bytes):
  hash = 5381
  for c in string:
    hash = ((hash << 5) + hash) + ord(c)
    hash &= 0xFFFFFFFF
  return hash

def write_gvariant_file (word_list_source, max_count=None):
    gvariant_definitions = GVARIANT_DEFINITIONS % word_list_source
    gvariant_index = GVARIANT_INDEX % word_list_source
    max_len=0
    offset = 0

    with open (gvariant_definitions, "wb") as gvariant_definitions_file:
        with open (gvariant_index, "wb") as gvariant_index_file:
            count = 0
            print ("Creating list of hashes")
            word_hash_list = [(g_str_hash (key), WORD_HASH[key]) for key in WORD_HASH]
            word_hash_list = sorted (word_hash_list, key=itemgetter(0))

            print ("Writing GVariants")
            for (word_hash, word_list) in word_hash_list:
                # when we have multiple words in the same hash, sort
                # them by the word_str before writing
                word_list = sorted (word_list, key=attrgetter('word_str'))
                max_len = max (max_len, len(word_list))

                for word in word_list:
                    # Get the variant for the word entry
                    word_v = word.to_variant()
                    word_data = word_v.get_data_as_bytes ()
                    gvariant_definitions_file.write (word_data.get_data())

                    # Create a variant of the index
                    # "uuq" == hash, offset, len
                    index_v = GLib.Variant ("(uuq)", (word_hash, offset, word_data.get_size()))
                    index_data = index_v.get_data_as_bytes()

                    gvariant_index_file.write(index_data.get_data())
                    if max_count and count > max_count:
                        return

                    data_size = word_data.get_size()
                    # Assert that no entry has a size larger than a uint16
                    assert data_size < 0xFFFF
                    offset += data_size
                    count += 1
    print (f"Wrote: {gvariant_definitions}")
    print (f"Wrote: {gvariant_index}")
    print (f"Max list len: {max_len}\n")

def generate_filtered_list (word_list_source, max_count=None):
    # Load the word lists to build an internal hash of words
    if not os.path.isfile(RAW_DEFINITIONS):
        print (f"Missing {RAW_DEFINITIONS}.\nDownload it from https://kaikki.org/dictionary/rawdata.html")
        sys.exit(1)
    if word_list_source == 'broda':
        load_broda ()
    elif word_list_source == 'wordnik':
        load_wordnik ()
    elif word_list_source == 'test':
        load_test ()

    write_filtered_raw_list (word_list_source, max_count=max_count)

def generate_enums(word_list_source):
    enum_index = ENUM_INDEX % word_list_source
    filtered_raw_definitions = FILTERED_RAW_DEFINITIONS % word_list_source

    if not os.path.isfile(filtered_raw_definitions):
        print (f"Missing file {filtered_raw_definitions}.\nRun `def-extractor.py {word_list_source} filtered-list` first")
        sys.exit(1)

    # as a side effect of doing this, we populate the tags and pos
    # hashes in wordentry. We otherwise ignore the results
    print ("Creating enum table")
    create_entry_list (word_list_source)

    pos_table = get_pos_table ()
    tags_table = get_tags_table ()

    pos_list = sorted (pos_table.keys ())
    tags_list = sorted (tags_table.keys())
    pos_tags_hash = {'pos':pos_list, 'tags':tags_list}
    with open (enum_index, "w") as enum_file:
        json.dump (pos_tags_hash, enum_file)
    print (f"Wrote {enum_index}")

def generate_gvariant_list(word_list_source, max_count=None):
    filtered_raw_definitions = FILTERED_RAW_DEFINITIONS % word_list_source
    enum_index = ENUM_INDEX %word_list_source

    if not os.path.isfile(filtered_raw_definitions):
        print (f"Missing file {filtered_raw_definitions}.\nCall def-extractor.py with filtered-list first")
        sys.exit(1)

    enumtable.load_table (enum_index)
    create_entry_list (word_list_source, max_count)
    write_gvariant_file (word_list_source, max_count)

def extract_word(word_list_source, target_word):
    target_word = scored_parse_word (target_word)
    filtered_raw_definitions = FILTERED_RAW_DEFINITIONS % word_list_source
    if not os.path.isfile(filtered_raw_definitions):
        print (f"Missing file {filtered_raw_definitions}.\nCall def-extractor.py with filtered-list first")
        sys.exit(1)
    with open(filtered_raw_definitions) as source_file:
        with open(target_word + '.jsonp', "w") as target_json_file:
            with open(target_word + '.gvariant', "wb") as target_gvariant_file:
                for line in source_file:
                    j = json.loads (line)
                    if 'word' in j:
                        word_src = j['word']
                        if scored_parse_word (word_src) == target_word:
                            target_json_file.write (json.dumps(j, indent=4, sort_keys=True))

                            word = Word (word_src)
                            word_v = word.to_variant()
                            word_data = word_v.get_data_as_bytes ()
                            target_gvariant_file.write (word_data.get_data())


    print (f"Wrote {target_word}.jsonp and {target_word}.gvariant")


if __name__ == '__main__':
    t = time.process_time()
    parser = argparse.ArgumentParser(description='Tool for extracting definitions from the wiktionary dumps')
    parser.add_argument('-c', '--count', type=int,
                        help='limit run to n iterations for testing')
    parser.add_argument('-w', '--word', type=str,
                        help='extract a word from the main wiktionary dump')
    parser.add_argument('wordlist', choices=['broda', 'wordnik', 'test'])
    parser.add_argument('command', choices=['filtered-list', 'enums', 'gvariant-list', 'extract', 'test'])

    args = parser.parse_args()

    word_list_source = args.wordlist

    if args.command == 'filtered-list':
        generate_filtered_list (word_list_source, max_count=args.count)
    elif args.command == 'enums':
        generate_enums(word_list_source)
    elif args.command == 'gvariant-list':
        generate_gvariant_list (word_list_source, max_count=args.count)
    elif args.command == 'extract':
        extract_word (word_list_source, args.word)
    else: #test
        pass

    print ("Elapsed time: %f secs" % (time.process_time()-t))

    ## Uncomment the following commands to play with the values

    ## Load word lists
    # load_wordnik ()
    # load_broda ()

    ## Cut down from the full 20gb raw list to one filtered by language
    ## and a word list. That will make our output much more manageable
    ## to deal with.
    # generate_filtered_list(word_list_source, max_count=args.count)

    # Go through the filtered raw list and create our list of entries.
    # create_entry_list (word_list_source, max_count=args.count)
    # write_gvariant_file ()

    # Generate new enums
    # dump_tags ()
    # dump_pos ()

