import json

ENUM_TABLE = {}

def load_table (enum_index):
    global ENUM_TABLE
    with open (enum_index) as enum_index_file:
        ENUM_TABLE = json.load (enum_index_file)

def get_pos_index(pos):
    try:
        return ENUM_TABLE['pos'].index (pos)
    except:
        return 0

def get_tag_index(tag):
    try:
        return ENUM_TABLE['tags'].index (tag)
    except:
        return 0
