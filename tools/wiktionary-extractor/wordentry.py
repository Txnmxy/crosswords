import gi
gi.require_version('GLib', '2.0')
from gi.repository import GLib
import json
from enumtable import get_pos_index, get_tag_index
# These tables are for generating the enums during the enums
# pass. They aren't read from anywhere else, and not used when
# construct the GVariant files
POS_ENUM_TABLE = {}
TAGS_ENUM_TABLE = {}


class Sense:
    def __init__ (self):
        self.glosses = []
        self.raw_glosses = []
        self.tags = []

    def parse_sense (self, s):
        self.tags = []
        if 'tags' in s:
            for tag in s['tags']:
                # Add the tag to the enum table This is used for
                # generating the enums table, and is ignored when
                # writing the gvariant
                TAGS_ENUM_TABLE[tag] = 1
                self.tags.append (get_tag_index (tag))
        if 'glosses' in s:
            for gloss in s['glosses']:
                self.glosses.append (str (gloss))
        if 'raw_glosses' in s:
            for gloss in s['raw_glosses']:
                self.raw_glosses.append (str (gloss))
        # GVariant makes a distinction between the empty list and a
        # 'nothing' value. We want to force this to be nothing if
        # there's no value.
        if not self.tags:
            self.tags = None

    def __str__(self):
        s = ''
        if self.raw_glosses:
            for gloss in self.raw_glosses:
                s += f"\t\t{gloss}\n"
        elif self.glosses:
            for gloss in self.glosses:
                s += f"\t\t{gloss}\n"
        return s

    def validate(self):
        if not self.glosses and not self.raw_glosses:
            return False
        return True

    def to_variant (self):
        if self.raw_glosses:
            return GLib.Variant ("(maqas)",
                                 (self.tags, self.raw_glosses))
        else:
            return GLib.Variant ("(maqas)",
                                 (self.tags, self.glosses))

class Entry:
    def __init__(self):
        self.pos = None
        self.senses = []
        self.header_expansion = None

    def parse_entry (self, j):
        if 'pos' in j:
            self.pos = get_pos_index (j['pos'])
            # Add the pos to the enum table
            POS_ENUM_TABLE[j['pos']] = 1
        else:
            return

        if 'head_templates' in j:
            self.header_expansion = j['head_templates'][0]['expansion']
        else:
            self.header_expansion = '[MISSING HEADER]'

        # skip words without senses
        if not 'senses' in j:
            return

        for s in j['senses']:
            sense = Sense ()
            sense.parse_sense (s)
            if sense.validate():
                self.senses.append (sense)

    def to_variant (self):
        senses = []
        for sense in self.senses:
            senses.append (sense.to_variant())
        return GLib.Variant ("(ysa(maqas))",
                             (self.pos.to_bytes(),
                              self.header_expansion,
                              senses))

    def validate (self):
        if not self.pos or not self.senses:
            return False
        return True

    def __str__(self):
        s = ''
        pos_str = '[No POS]'
        header_expansion = '[Empty Header]'
        if self.pos:
            pos_str = POS(self.pos).name
        if self.header_expansion:
            header_expansion = self.header_expansion

        s = f"\t{pos_str}\n"
        s += f"\t{header_expansion}\n"
        for sense in self.senses:
            s += str (sense)
        return s


class Word:
    def __init__(self, word_str):
        self.word_str = word_str
        self.entries = []

    def parse_entry(self, j):
        # sanity check we're doing the right word
        word_str = j['word']
        if self.word_str != word_str:
            print (self.word_str + " doesn't match " + word_str)
        assert self.word_str == word_str

        entry = Entry()
        entry.parse_entry (j)
        if entry.validate ():
            self.entries.append (entry)

    def __str__(self):
        s = self.word_str + '\n'
        for e in self.entries:
            s += str (e)
        return s

    def validate(self):
        return self.entries

    def to_variant (self):
        entries = []
        for entry in self.entries:
            entries.append (entry.to_variant())
        v = GLib.Variant ("(sa(ysa(maqas)))",
                          (self.word_str, entries))
        # For debugging
        # s = v.print_(True)
        # print (f"Variant: {s}")
        return v

# These functions will create the table of enums we need.

def get_tags_table():
    return TAGS_ENUM_TABLE

def get_pos_table():
    return POS_ENUM_TABLE
