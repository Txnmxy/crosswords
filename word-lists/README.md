# Word Lists

These are the source files for the word list gresource bundles. They
are large, and may need to move out of git in the future. They are
precomputed in order to save *significant* compilation time, at the
cost of bandwidth and storage.

Information on how to create these files can be found
[here](../docs/word-entry.md).

## License

* The broda word list has no explicit license, but has the following
  terms on its website:

> You are free to use this list in any way you'd like. This includes
> commercial uses, though I'd appreciate it if you didn't just turn
> around and try to sell it (but I mean, I'll still offer it for free
> to anyone so that wouldn't be a smart business venture anyway).

* The test word list is just one out of every ten words from the broda
  word list, plus a few more test words added. It is under the same
  terms as the broda list.

* The wordnik list is available under the terms of the MIT license,
  and its license file is included unmodified in this directory in
  `LICENSE.wordnik`

